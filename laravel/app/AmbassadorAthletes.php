<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmbassadorAthletes extends Model {
    // use Authenticatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ambassador_athletes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
      protected $fillable = ['ambassador_id', 'subscriptionStatus', 'subscriptionDate','athlete_id'];
}
