<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Ambassadors extends Model {
    // use Authenticatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ambassadors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'subscriptionRate', 'subscriptionComission', 'productComission'];

    public static function search($search) {

        $sql = 'select u.firstName, u.middleName, u.lastName, u.image, u.status, u.email,u.id, amb_s.speciality_id from users as u 
left join ambassadors_specialities as amb_s on amb_s.ambassador_id = u.id
where (u.firstName LIKE ' . '"%' . $search . '%"' . ' or u.middleName LIKE ' . '"%' . $search . '%"' . ' or u.lastName LIKE ' . '"%' . $search . '%"' . ' or u.email LIKE "' . $search . '%") and u.status = 1 and u.role_id = 3';

        $result = DB::select($sql);
        return $result;
    }

}
