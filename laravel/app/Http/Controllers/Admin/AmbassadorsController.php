<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Validator;
use App\User;
use App\Ambassadors;
// use App\Orders;
// use App\Products;
use Illuminate\Http\Request;

class AmbassadorsController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $data['model'] = User::where('role_id', 3)->paginate(9);

        return view('admin.ambassadors', $data);
    }

    public function store(Request $request) {
        // d($_POST,1);
        $validator = Validator::make($request->all(), [
                    'subscriptionRate' => 'required',
                    'subscriptionComission' => 'required',
                    'productComission' => 'required'
        ]);

        if ($validator->fails()) {
            $request->session()->flash('alert-danger', 'Ambassador Info not added, check your details!');
            return back()
                            ->withErrors($validator)
                            ->withInput();
        } else {

            $ambassador = new Ambassadors;
            $ambassador->subscriptionRate = $request->subscriptionRate;
            $ambassador->productComission = $request->productComission;
            $ambassador->subscriptionComission = $request->subscriptionComission;
            $ambassador->user_id = $request->user_id;
            $ambassador->save();
            $request->session()->flash('alert-success', 'Successfully Added!');
            return redirect('admin/');
        }
    }

    public function update(Request $request, $id) {
        //d($_POST,1);
        $validator = Validator::make($request->all(), [
                    'subscriptionRate' => 'required',
                    'subscriptionComission' => 'required',
                    'productComission' => 'required'
        ]);

        if ($validator->fails()) {
            $request->session()->flash('alert-danger', 'Ambassador Info not added, check your details!');
            return back()
                            ->withErrors($validator)
                            ->withInput();
        } else {
            //d($_POST,1);
            // echo $id;
            $ambassador = Ambassadors::find($id);
            $ambassador->subscriptionRate = $request->subscriptionRate;
            $ambassador->productComission = $request->productComission;
            $ambassador->subscriptionComission = $request->subscriptionComission;
            $ambassador->save();

            $request->session()->flash('alert-success', 'Successfully Updated!');
            return back()->withInput();
        }
    }

}
