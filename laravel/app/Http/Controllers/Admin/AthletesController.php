<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use App\AmbassadorAthletes;
use App\AmbassadorsSubscriptions;
use Illuminate\Http\Request;
use App\Transactions;
use App\Functions\Functions;

class AthletesController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $data['model'] = User::where('role_id', 2)->paginate(9);

        return view('admin.athletes', $data);
    }

    public function athletes($id) {
        $data['ambassador'] = User::where('role_id', 3)->where('id', $id)->first();
        $data['model'] = User::unassignedAthlete();
        $data['ambassador_id'] = $id;

        return view('admin.assign_athletes', $data);
    }

    public function assign(Request $request) {
        $subscriptionRate = Config('params.subscription_fee');
        $input = $request->all();
        if (AmbassadorAthletes::where('athlete_id', $input['athlete_id'])->where(['status' => 1, 'deleted' => 0, 'subscriptionStatus' => '!=1'])->exists()) {
            $request->session()->flash('alert-danger', 'Already Assigned');
            return redirect()->back();
        } else {
            unset($input['_token']);
            AmbassadorAthletes::create([
                'ambassador_id' => $input['ambassador_id'],
                'athlete_id' => $input['athlete_id'],
                'subscriptionStatus' => 1,
            ]);
            $transactions = Transactions::where('type_id', $input['athlete_id'])->where('status', 1)->where('deleted', 0)->first();
            $subscribed = new AmbassadorsSubscriptions;
            $subscribed->ambassador_id = $input['ambassador_id'];
            $subscribed->athlete_id = $input['athlete_id'];
            $subscribed->subscriptionRate = $transactions->amount;
            $subscribed->subscriptionComission = 0.00;
            $subscribed->subscriptionDate = date('Y-m-d H:i:s');
            $subscribed->save();

            $getAthlete = User::where('id', $input['athlete_id'])->where('status', 1)->where('deleted', 0)->first();
            $getAmbassador = User::where('id', $input['ambassador_id'])->where('status', 1)->where('deleted', 0)->first();
            $data['athleteName'] = $getAthlete->firstName.' '.$getAthlete->lastName;
            $data['ambassadorName'] = $getAmbassador->firstName.' '.$getAmbassador->lastName;
            
            //Email sent to Coach that tells the Coach they have a new Athlete assigned
            $subjectAmbassador = view('emails.assign_athlete_mailto_coach.subject');
            $bodyAmbassador = view('emails.assign_athlete_mailto_coach.body', compact('data'));
            Functions::sendEmail($getAmbassador->email, $subjectAmbassador, $bodyAmbassador);
            
            //Email sent to Athlete that tells the Athlete assigning of Coach
            $subjectAthlete = view('emails.assign_athlete_mailto_athlete.subject');
            $bodyAthlete = view('emails.assign_athlete_mailto_athlete.body', compact('data'));
            Functions::sendEmail($getAthlete->email, $subjectAthlete, $bodyAthlete);

            $request->session()->flash('alert-success', 'SuccessFully Assigned!');
            return redirect()->back();
        }
    }

}
