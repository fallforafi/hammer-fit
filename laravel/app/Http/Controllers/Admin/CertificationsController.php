<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;
use Validator, Input, Redirect;
use App\User;
use Auth;
use Session;
use App\Certifications;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Functions\Functions;
use Intervention\Image\Facades\Image as Image;


class CertificationsController extends AdminController  {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    use AuthenticatesAndRegistersUsers;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
          $certifications = Certifications::paginate(10);
      return view('admin.certifications.index', compact('certifications'));
    }
    
    public function view(Request $request, $id) {
        $certifications = Certifications::where('id', '=', $id)->first();
        return view('admin.certifications.view', compact('certifications'));
    }
    

    
    public function delete($id, Request $request) {
        $row = Certifications::where('id', '=', $id)->delete();
        $request->session()->flash('alert-success', 'Successfully Deleted!');
        return back();
    }

}
