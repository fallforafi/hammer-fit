<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Charges;
use Validator,
    Input,
    Redirect;

class ChargesController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data = array();
        $data['model'] = Charges::get();

        return view('admin.charges.index', $data);
    }

    public function create() {

        return view('admin.charges.create');
    }

    public function insert(Request $request) {

        $rules = [
            'name' => 'required',
            'type' => 'required|unique:charges',
            'rate' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $model = new Charges;
        $model->name = $request->name;
        $model->type = $request->type;
        $model->rate = $request->rate;
        $model->save();

        $request->session()->flash('success', 'Successfully Added!');

        return redirect('admin/charges');
    }

    public function edit($id) {
        $charges = Charges::findOrFail($id);
        return view('admin.charges.edit', compact('charges'));
    }

    public function update($id, Request $request) {
        $input = $request->all();

        unset($input['_token']);

        Charges::where('id', '=', $id)->update($input);

        $request->session()->flash('success', 'Successfully Updated!');
        return redirect('admin/charges');
    }

}
