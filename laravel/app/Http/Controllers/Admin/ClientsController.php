<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Auth;
use DB;
use App\Results;
use App\Shows;
use App\MealPlans;
use App\AthleteMp;
use App\WorkoutPlans;
use App\AthleteWp;
use App\Certifications;
use App\User;
use App\Countries;
use App\RequestedCoaches;
use App\States;
use App\Messages;
use Illuminate\Http\Request;
use App\Orders;
use File;

class ClientsController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if (Auth::user()->role->role == 'admin') {

            $model = User::where('users.role_id', '!=', 1)
                    ->join('roles as r', 'r.id', '=', 'users.role_id')
                    ->leftJoin('ambassador_athletes as aa', 'aa.athlete_id', '=', 'users.id')
                    ->leftJoin('users as v', 'v.id', '=', 'aa.ambassador_id')
                    ->select('users.id', 'users.status', 'users.firstName', 'users.lastName', 'users.email', 'users.role_id', 'users.created_at', 'r.role', 'v.firstName as ambassadorFirstname', 'v.lastName as ambassadorLastname', 'aa.subscriptionStatus')
                    ->where('users.deleted', 0)
                    ->orderBy('users.id', 'desc')
                    ->paginate(50);
            //d($model,1);
            return view('admin.clients', compact('model'));
        } else {
            return redirect('home');
        }
    }

    public function userDetail(Request $request) {
        $userId = $request->id;
        $countries = Countries::get();
        $states = States::get();
        //d($userId,1);
        if ($userId > 0) {
            if (Auth::user()->role->role == 'admin') {
                $data = User::join('address', 'address.user_id', '=', 'users.id')
                        ->leftJoin('countries as c', 'c.id', '=', 'address.country')
                        ->leftJoin('states as s', 's.id', '=', 'address.state')
                        ->select('users.*', 'address.phone', 'address.city', 'c.name as countryName', 's.name as stateName', 'address.address', 'address.zip', 'address.created_at', 'address.addressType', 'address.address2', 'address.mobile', 'address.status as addStatus')
                        ->where('users.id', '=', $userId)
                        ->where('address.user_id', '=', $userId)
                        ->whereNull('address.order_id')
                        ->first();

                $orders = Orders::where('orders.user_id', '=', $userId)
                        ->orderBy('orders.id', 'desc')
                        ->get();
                $subscriptions = DB::table('ambassador_athletes as aa')
                        ->join('users as u', 'u.id', '=', 'aa.ambassador_id')
                        ->join('users as v', 'v.id', '=', 'aa.athlete_id')
                        ->select('aa.*', 'u.firstName as ambassadorFirstname', 'u.lastName as ambassadorLastname', 'v.firstName as athleteFirstname', 'v.lastName as athleteLastname', 'u.role_id as amb_role', 'v.role_id as ath_role')
                        ->where('u.status', 1)
                        ->where('v.status', 1)
                        ->where('ambassador_id', '=', $userId)
                        ->orWhere('athlete_id', '=', $userId)
                        ->orderBy('id', 'desc')
                        ->get();
                $results = Results::where('user_id', '=', $userId)
                        ->orderBy('date', 'desc')
                        ->get();
                //     d($results, 1);
                $shows = Shows::where('user_id', '=', $userId)
                        ->orderBy('date', 'desc')
                        ->first();
                $athleteWp = WorkoutPlans::join('athlete_wp', 'athlete_wp.wp_id', '=', 'workout_plans.id')
                        ->select('workout_plans.*', 'athlete_wp.athlete_id as a_wid')
                        ->where([
                            'athlete_wp.athlete_id' => $userId,
                            'workout_plans.deleted' => 0,
                        ])
                        ->orderBy('athlete_wp.id', 'desc')
                        ->get();
                $athleteMp = MealPlans::join('athlete_mp', 'athlete_mp.mp_id', '=', 'meal_plans.id')
                        ->select('meal_plans.*', 'athlete_mp.athlete_id as a_mid')
                        ->where([
                            'meal_plans.deleted' => 0,
                            'athlete_mp.athlete_id' => $userId,
                        ])
                        ->orderBy('athlete_mp.id', 'desc')
                        ->get();
//                $mealPlans = MealPlans::where([
//                            'user_id' => $userId,
//                            'deleted' => 0,
//                        ])->get();
//                $workoutPlans = WorkoutPlans::where([
//                            'user_id' => $userId,
//                            'deleted' => 0,
//                        ])->get();
                $certifications = Certifications::where('user_id', '=', $userId)->orderBy('year', 'desc')->get();
                $messages = Messages::get();
                if (RequestedCoaches::where('athlete_id', $userId)->exists()) {
                    $requested = RequestedCoaches::where('athlete_id', $userId)
                            ->orWhere('coach_id', $userId)
                            ->first();
                    $coach = User::where('id', $requested->coach_id)->first();
                } else {
                    $coach = Null;
                }
                return view('admin.client', [
                    'data' => $data,
                    'orders' => $orders,
                    'subscriptions' => $subscriptions,
                    'results' => $results,
                    'shows' => $shows,
                    'athleteMp' => $athleteMp,
                    'athleteWp' => $athleteWp,
//                    'mealPlans' => $mealPlans,
//                    'workoutPlans' => $workoutPlans,
                    'certifications' => $certifications,
                    'messages' => $messages,
                    'countries' => $countries,
                    'states' => $states,
                    'coach' => $coach
                ]);
            } else {
                return redirect('home');
            }
        } else {
            return redirect('home');
        }
    }

    public function delete($id, Request $request) {
        User::where('id', '=', $id)->delete();
        $request->session()->flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function makeCollage(Request $request) {
        $data = array();
        //d($request->all(),1);
        //d(count($request->image),1);
        if (count($request->image) > 2) {

            $request->session()->flash('danger', 'Invalid selection!');
            return redirect()->back();
        }
        if (!empty($request->image)) {
            $keys = array_keys($request->image);
            if (isset($keys[0])) {
                $values[0] = array_values($request->image);
                $data['first'] = $values[0][0];
            }
            if (isset($keys[1])) {
                $values[1] = array_values($request->image);
                $data['second'] = $values[1][1];
            }
            return view('admin.collage.index', $data);
        }

        $request->session()->flash('danger', 'Invalid selection to compare images.');
        return redirect()->back();
        //d($data,1);
    }

    public function deleteResult($id, Request $request) {
        $results = Results::where('id', '=', $id)->first();

        if (isset($results->frontImage) && $results->frontImage != "") {
            $destinationPath = public_path() . '/uploads/users/results/front_images/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $destinationPathOrig = $destinationPath . 'original/';

            $file1 = $destinationPath . $results->frontImage;
            $file2 = $destinationPathThumb . $results->frontImage;
            $file3 = $destinationPathOrig . $results->frontImage;
            File::delete($file1, $file2, $file3);
        }
        if (isset($results->sideImage) && $results->sideImage != "") {
            $destinationPath = public_path() . '/uploads/users/results/side_images/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $destinationPathOrig = $destinationPath . 'original/';

            $file1 = $destinationPath . $results->sideImage;
            $file2 = $destinationPathThumb . $results->sideImage;
            $file3 = $destinationPathOrig . $results->sideImage;
            File::delete($file1, $file2, $file3);
        }
        if (isset($results->backImage) && $results->backImage != "") {
            $destinationPath = public_path() . '/uploads/users/results/back_images/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $destinationPathOrig = $destinationPath . 'original/';

            $file1 = $destinationPath . $results->backImage;
            $file2 = $destinationPathThumb . $results->backImage;
            $file3 = $destinationPathOrig . $results->backImage;
            File::delete($file1, $file2, $file3);
        }

        Results::where('id', '=', $id)->delete();
        $request->session()->flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function deleteMealPlan($id, Request $request) {
        $mp = MealPlans::where('id', '=', $id)->first();

        $path = public_path() . '/uploads/users/meal-plans/';
        $file1 = $path . $mp->file;
        File::delete($file1);

        MealPlans::where('id', '=', $id)->delete();
        AthleteMp::where('mp_id', '=', $id)->delete();

        $request->session()->flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function deleteWorkoutPlan($id, Request $request) {
        $wp = WorkoutPlans::where('id', '=', $id)->first();

        $path = public_path() . '/uploads/users/workout-plans/';
        $file1 = $path . $wp->file;
        File::delete($file1);

        MealPlans::where('id', '=', $id)->delete();
        AthleteWp::where('wp_id', '=', $id)->delete();

        $request->session()->flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function available(Request $request, $id) {
        DB::table('users')->where('id', $id)->update([
            'availability' => 1,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('alert-success', 'Successfully Updated!');
        return back();
    }

    public function unavailable(Request $request, $id) {
        DB::table('users')->where('id', $id)->update([
            'availability' => 0,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('alert-success', 'Successfully Updated!');
        return back();
    }

}
