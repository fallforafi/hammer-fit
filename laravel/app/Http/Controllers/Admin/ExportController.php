<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Auth;
use DB;
use Excel;
use App\User;

class ExportController extends AdminController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        parent::__construct();
    }

    public function export($type) {
        if (Auth::user()->role->role == 'admin') {
//            d($type,1);
//            $result = User::where('users.role_id', '!=', 1)
//                    ->join('roles as r', 'r.id', '=', 'users.role_id')
//                    ->leftJoin('ambassador_athletes as aa', 'aa.athlete_id', '=', 'users.id')
//                    ->leftJoin('users as v', 'v.id', '=', 'aa.ambassador_id')
//                    ->select('users.id', 'users.status', 'users.firstName', 'users.lastName', 'users.email', 'users.role_id', 'users.created_at', 'r.role', 'v.firstName as ambassadorFirstname', 'v.lastName as ambassadorLastname', 'aa.subscriptionStatus')
//                    ->where('users.deleted', 0)
//                    ->orderBy('users.id', 'desc')
//                    ->toSql();
            $sql = "select `users`.`firstName` as First_Name, `users`.`lastName` as Last_Name, `r`.`role` as Joined_As, CONCAT(`v`.`firstName`,' ',`v`.`lastName`) AS Assigned_Coach,`users`.`status` as Status from `users` inner join `roles` as `r` on `r`.`id` = `users`.`role_id` left join `ambassador_athletes` as `aa` on `aa`.`athlete_id` = `users`.`id` left join `users` as `v` on `v`.`id` = `aa`.`ambassador_id` where `users`.`role_id` != 1 and `users`.`deleted` = 0 order by `users`.`id` DESC";
            $result = DB::select($sql);
            //d(count($result),1);
            //d($model,1);
            $data = array_map(function($object) {
                return (array) $object;
            }, $result);
            return Excel::create('users_data', function($excel) use ($data) {
                        $excel->sheet('mySheet', function($sheet) use ($data) {
                            $sheet->fromArray($data);
                        });
                    })->download($type);
        } else {
            return redirect('home');
        }
    }

//    public function downloadExcel($type) {
//        $sql = "SELECT u.firstName FirstName,u.`lastName` LastName,u.`email`Email,u.`phone`Phone,u.`location`Location,
//       GROUP_CONCAT(DISTINCT l.title) Languages, 
//       GROUP_CONCAT(DISTINCT s.title) Skills,
//       COUNT(DISTINCT t1.id) TasksPosted,
//       COUNT(DISTINCT t2.id) TasksCompleted,
//       AVG(r.`rating`) AverageRating
//FROM   users u 
//       LEFT JOIN users_languages ul 
//               ON  ul.user_id = u.id
//       LEFT  JOIN languages l 
//               ON ul.language_id = l.id
//       LEFT JOIN users_skills us 
//               ON us.user_id = u.`id`
//       LEFT  JOIN skills s 
//               ON s.id = us.`skill_id`
//       LEFT JOIN tasks t1 ON t1.user_id = u.id
//       LEFT JOIN (SELECT * FROM tasks WHERE taskStatus='completed')t2 ON t2.user_id = u.id
//       LEFT JOIN reviews r ON r.rate_to = u.id
//GROUP  BY u.`id`";
//        $result = DB::select($sql);
//        $data = array_map(function($object) {
//            return (array) $object;
//        }, $result);
//        return Excel::create('users_data', function($excel) use ($data) {
//                    $excel->sheet('mySheet', function($sheet) use ($data) {
//                        $sheet->fromArray($data);
//                    });
//                })->download($type);
//    }
    //    public function importExcel() {
//        if (Input::hasFile('import_file')) {
//            $path = Input::file('import_file')->getRealPath();
//            $data = Excel::load($path, function($reader) {
//                        
//                    })->get();
//            if (!empty($data) && $data->count()) {
//                foreach ($data as $key => $value) {
//                    $insert[] = ['title' => $value->title, 'description' => $value->description];
//                }
//                if (!empty($insert)) {
//                    DB::table('users')->insert($insert);
//                    dd('Insert Record successfully.');
//                }
//            }
//        }
//        return back();
//    }
}
