<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use DB;
use App\User;
use App\Orders;
use App\Transactions;
use Carbon\Carbon;
//use App\AmbassadorsSubscriptions;
use Illuminate\Http\Request;

class HomeController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data = array();

        $data['totalUsers'] = User::where('role_id', '!=', 1)->get();

        $data['totalOrders'] = Orders::where('deleted', 0)->get();

        $data['totalSubscriptions'] = DB::table('ambassadors_subscriptions')
                ->join('users as u', 'u.id', '=', 'ambassadors_subscriptions.ambassador_id')
                ->join('users as v', 'v.id', '=', 'ambassadors_subscriptions.athlete_id')
                ->join('transactions as tx', 'tx.type_id', '=', 'v.id')
                ->select('ambassadors_subscriptions.*', 'u.firstName as ambassador_firstName', 'u.lastName as ambassador_lastName', 'v.firstName as athlete_firstName', 'v.lastName as athlete_lastName', 'u.role_id as amb_role', 'v.role_id as ath_role', 'tx.amount')
                ->where('u.status', 1)
                ->where('v.status', 1)
                ->where('ambassadors_subscriptions.subscriptionStatus', 1)
                ->orderBy('ambassadors_subscriptions.id', 'desc')
                ->get();

        $data['totalTransactions'] = Transactions::where('status', 1)->where('deleted', 0)->get();

        $currentDay = Carbon::today();
        $now = Carbon::now();
        $currentMonth = $now->month;
        $currentWeekStart = $now->startOfWeek() . '';
        $currentWeekEnd = $now->endOfWeek();

        //Users
        $data['usersMonthly'] = User::join('roles as r', 'r.id', '=', 'users.role_id')
                ->select('users.*', 'r.role')
                ->whereRaw('MONTH(created_at) = ?', [$currentMonth])
                ->where('role_id', '!=', 1)
                ->get();

        $data['usersWeekly'] = User::where('role_id', '!=', 1)
                ->whereBetween('created_at', [$currentWeekStart, $currentWeekEnd])
                ->get();

        $data['usersToday'] = User::whereRaw('DATE(created_at) = ?', [$currentDay])
                ->where('role_id', '!=', 1)
                ->get();

        //Orders  
        $data['ordersMonthly'] = Orders::whereRaw('MONTH(created_at) = ?', [$currentMonth])
                ->where('deleted', 0)
                ->get();

        $data['ordersWeekly'] = Orders::where('deleted', 0)
                ->whereBetween('created_at', [$currentWeekStart, $currentWeekEnd])
                ->get();

        $data['ordersToday'] = Orders::whereRaw('DATE(created_at) = ?', [$currentDay])
                ->where('deleted', 0)
                ->get();

        //Subscriptions  
        $data['subscriptionsMonthly'] = DB::table('ambassadors_subscriptions')
                ->join('users as u', 'u.id', '=', 'ambassadors_subscriptions.ambassador_id')
                ->join('users as v', 'v.id', '=', 'ambassadors_subscriptions.athlete_id')
                ->join('transactions as tx', 'tx.type_id', '=', 'v.id')
                ->select('ambassadors_subscriptions.*', 'u.firstName as ambassador_firstName', 'u.lastName as ambassador_lastName', 'v.firstName as athlete_firstName', 'v.lastName as athlete_lastName', 'u.role_id as amb_role', 'v.role_id as ath_role', 'tx.amount')
                ->where('u.status', 1)
                ->where('v.status', 1)
                ->where('subscriptionStatus', 1)
                ->whereRaw('MONTH(subscriptionDate) = ?', [$currentMonth])
                ->get();

        $data['subscriptionsWeekly'] = DB::table('ambassadors_subscriptions')
                ->join('users as u', 'u.id', '=', 'ambassadors_subscriptions.ambassador_id')
                ->join('users as v', 'v.id', '=', 'ambassadors_subscriptions.athlete_id')
                ->join('transactions as tx', 'tx.type_id', '=', 'v.id')
                ->select('ambassadors_subscriptions.*', 'u.firstName as ambassador_firstName', 'u.lastName as ambassador_lastName', 'v.firstName as athlete_firstName', 'v.lastName as athlete_lastName', 'u.role_id as amb_role', 'v.role_id as ath_role', 'tx.amount')
                ->where('u.status', 1)
                ->where('v.status', 1)
                ->where('subscriptionStatus', 1)
                ->whereBetween('subscriptionDate', [$currentWeekStart, $currentWeekEnd])
                ->get();

        $data['subscriptionsToday'] = DB::table('ambassadors_subscriptions')
                ->join('users as u', 'u.id', '=', 'ambassadors_subscriptions.ambassador_id')
                ->join('users as v', 'v.id', '=', 'ambassadors_subscriptions.athlete_id')
                ->join('transactions as tx', 'tx.type_id', '=', 'v.id')
                ->select('ambassadors_subscriptions.*', 'u.firstName as ambassador_firstName', 'u.lastName as ambassador_lastName', 'v.firstName as athlete_firstName', 'v.lastName as athlete_lastName', 'u.role_id as amb_role', 'v.role_id as ath_role', 'tx.amount')
                ->where('u.status', 1)
                ->where('v.status', 1)
                ->where('subscriptionStatus', 1)
                ->whereRaw('DATE(subscriptionDate) = ?', [$currentDay])
                ->get();

        //Transactions  
        $data['transactionsMonthly'] = Transactions::whereRaw('MONTH(created_at) = ?', [$currentMonth])
                ->where('status', 1)
                ->where('deleted', 0)
                ->get();

        $data['transactionsWeekly'] = Transactions::where('deleted', 0)
                ->where('status', 1)
                ->whereBetween('created_at', [$currentWeekStart, $currentWeekEnd])
                ->get();

        $data['transactionsToday'] = Transactions::whereRaw('DATE(created_at) = ?', [$currentDay])
                ->where('status', 1)
                ->where('deleted', 0)
                ->get();

        return view('admin.home', $data);
    }

    public function accept(Request $request, $id) {
        DB::table('users')->where('id', $id)->update([
            'status' => 1,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('alert-success', 'Successfully Approved!');
        return back();
    }

    public function reject(Request $request, $id) {
        DB::table('users')->where('id', $id)->update([
            'status' => 0,
            'updated_at' => date('Y-m-d'),
        ]);
        $request->session()->flash('alert-success', 'Successfully Disapproved!');
        return back();
    }

    public function edit($id) {
        // $industries = DB::table('categories')->get();
        // $editmanageproducts = DB::table('products')->where('id', $id)->find($id);
        // return view('admin_pages.editmanageproducts',['editmanageproducts' => $editmanageproducts, 'industries' => $industries]);
        $ambassadorCheck = DB::table('ambassadors')->where('user_id', $id)->first();

        return view('admin.ambassador.edit', ['ambassadorCheck' => $ambassadorCheck, 'id' => $id]);
    }

}
