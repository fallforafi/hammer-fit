<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use App\Transactions;
use Session;
use App\Functions\Functions;
use App\Functions\Payflow;
use App\PayflowProfiles;
use App\AmbassadorAthletes;
use App\AmbassadorsSubscriptions;
use App\MembershipCancel;
use App\Cards;
use DB;

class MembershipController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data = array();
        $data['model'] = MembershipCancel::join('users as u', 'u.id', '=', 'membership_cancel.user_id')
                ->select('u.*')
                ->orderBy('membership_cancel.id', 'desc')
                ->get();

        return view('admin.membership.index', $data);
    }

    public function unsubscribe($id) {
        DB::beginTransaction();
        try {
//            User::where(['id' => $user_id, 'role_id' => 2, 'deleted' => 0])->update([
//                'role_id' => 4
//            ]);

            AmbassadorsSubscriptions::where(['athlete_id' => $id, 'subscriptionStatus' => '1'])->update([
                'subscriptionStatus' => '2'
            ]);

            AmbassadorAthletes::where(['athlete_id' => $id, 'subscriptionStatus' => '1'])->update([
                'subscriptionStatus' => '2'
            ]);

//            MembershipCancel::create(['user_id' => $user_id]);
//
//            $data['firstName'] = Auth::user()->firstName;
//            $data['lastName'] = Auth::user()->lastName;
//            $subject = view('emails.membership_cancels.subject');
//            $body = view('emails.membership_cancels.body', $data);
//
//            $getAdmins = User::where(['role_id' => 1, 'deleted' => 0])->get();
//            foreach ($getAdmins as $value) {
//                Functions::sendEmail($value->email, $subject, $body);
//            }

            DB::commit();
            Session::flash('success', 'Successfully Unsubscribed!');
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollBack();
            Session::flash('danger', $e);
            return redirect()->back();
        }
    }

    public function cancel($id) {
        $user_id = $id;

        $getUser = User::where(['id' => $user_id])->first();

        $subscriptions = Transactions::join('payflow_profiles as p', 'p.transaction_id', '=', 'transactions.id')
                ->select('transactions.*', 'p.profileId', 'p.message')
                ->where('transactions.status', 1)
                ->where('p.status', 1)
                ->where('transactions.deleted', 0)
                ->where('transactions.type_id', $user_id)
                ->first();
        //d($subscriptions, 1);
        $data['subscriptions'] = $subscriptions;

        $card = Cards::where('user_id', $user_id)->where('status', 1)->first();
        $data['card'] = $card;

        //$expDate = sprintf("%02d", $request->expMonth) . '' . substr($request->expYear, 2, 3);
        // d($subscriptions, 1);

        DB::beginTransaction();
        try {
            //  $q = "TRXTYPE=R&TENDER=C&VENDOR=" . env('PAYFLOW_VENDOR') . "&PARTNER=" . env('PAYFLOW_PARTNER') . "&USER=" . env('PAYFLOW_USER') . "&PWD=" . env('PAYFLOW_PASSWORD') . "&ACTION=C&ORIGPROFILEID=" . $subscriptions->profileId . "&VERBOSITY=HIGH";

            $PayFlow = new PayFlow(env('PAYFLOW_VENDOR'), env('PAYFLOW_PARTNER'), env('PAYFLOW_USER'), env('PAYFLOW_PASSWORD'), 'recurring');

            $PayFlow->setEnvironment(env('PAYFLOW_ENV'));  // test or live
            $PayFlow->setInvoice($user_id);
            $PayFlow->setTransactionType('R');
            $PayFlow->setPaymentMethod('C');
            $PayFlow->setAmount($subscriptions->amount, FALSE);
            $PayFlow->setCCNumber($card->cc);
            $PayFlow->setCVV($card->cvc);
            $PayFlow->setExpiration($card->expDate);
            $PayFlow->setCreditCardName($card->name);
            $PayFlow->setCustomerFirstName($getUser->firstName);
            $PayFlow->setCustomerLastName($getUser->lastName);
            $PayFlow->setCustomerCountry('US');
            $PayFlow->setCustomerEmail($getUser->email);
            $PayFlow->setProfileAction('C');
            $PayFlow->setProfileId($subscriptions->profileId);
            // d($PayFlow,1);
            if ($PayFlow->processTransaction()) {
                User::where(['id' => $user_id, 'role_id' => 2, 'deleted' => 0])->update([
                    'role_id' => 4
                ]);

                Transactions::where('type_id', $user_id)->update([
                    'status' => 0,
                    'deleted' => 1
                ]);

                Cards::where('user_id', $user_id)->update([
                    'status' => 0,
                    'deleted' => 1
                ]);

                AmbassadorsSubscriptions::where(['athlete_id' => $user_id, 'subscriptionStatus' => '1'])->update([
                    'subscriptionStatus' => '2'
                ]);

                AmbassadorAthletes::where(['athlete_id' => $user_id, 'subscriptionStatus' => '1'])->update([
                    'subscriptionStatus' => '2'
                ]);

                MembershipCancel::create(['user_id' => $user_id]);

                $data['firstName'] = $getUser->firstName;
                $data['lastName'] = $getUser->lastName;
                $subject = view('emails.admin_cancels_membership.subject');
                $body = view('emails.admin_cancels_membership.body', $data);

                Functions::sendEmail($getUser->email, $subject, $body);

                DB::commit();
                Session::flash('success', 'Successfully Cancelled!');

                return redirect()->back();
            } else {
                $response = $PayFlow->getResponse();
                // d($response);
                $response['RESPMSG'] = isset($response['RESPMSG']) ? $response['RESPMSG'] : "";
                Session::flash('danger', $response['RESPMSG']);
                return redirect()->back();
            }
        } catch (Exception $e) {
            DB::rollBack();
            //d($e);
            Session::flash('danger', 'Your action has been unsuccessful, please try again!');
            return redirect()->back();
        }
    }

}
