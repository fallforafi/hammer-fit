<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Functions\Functions;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Validator, Input, Redirect;
use App\Specialities;

class SpecialitiesController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $specialities = Specialities::where('deleted', '0')->get();
//        d($specialities,1);
        return view('admin.specialities.index', compact('specialities'));
    }

    public function create() {
        
        return view('admin.specialities.create');
    }



    public function insert(Request $request) {

        $rules = [
            'title' => 'required|max:100',
        ];

        $rules['description'] = 'required';

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $model = new Specialities;
        $model->title = $request->title; 
        $model->description = $request->description;
        $model->save();

        $request->session()->flash('alert-success', 'Successfully Added!');

        return redirect('admin/specialities/create');
    }

    public function edit($id) {
        $speciality = Specialities::findOrFail($id);
        return view('admin.specialities.edit', compact('speciality'));
    }

    public function update($id, Request $request) {
        $id = $request->id;
        $speciality = Specialities::findOrFail($id);
        $input = $request->all();

        unset($input['_token']);



        $affectedRows = Specialities::where('id', '=', $id)->update($input);

        $request->session()->flash('alert-success', 'Successfully Updated!');
        return redirect('admin/specialities');
    }

    public function delete($id, Request $request) {
        $row = Specialities::where('id', '=', $id)->delete();
        $request->session()->flash('alert-success', 'Successfully Deleted!');
        return back();
    }

}
