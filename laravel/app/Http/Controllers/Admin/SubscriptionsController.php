<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use DB;

class SubscriptionsController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $data['model'] = DB::table('ambassadors_subscriptions')
                ->join('users as u', 'u.id', '=', 'ambassadors_subscriptions.ambassador_id')
                ->join('users as v', 'v.id', '=', 'ambassadors_subscriptions.athlete_id')
                ->join('transactions as tx', 'tx.type_id', '=', 'v.id')
                ->select('ambassadors_subscriptions.*', 'u.firstName as ambassador_firstName', 'u.lastName as ambassador_lastName', 'v.firstName as athlete_firstName', 'v.lastName as athlete_lastName', 'u.role_id as amb_role', 'v.role_id as ath_role', 'tx.amount')
                ->where('u.status', 1)
                ->where('v.status', 1)
                //->where('ambassadors_subscriptions.subscriptionStatus',1)
                ->orderBy('ambassadors_subscriptions.id', 'desc')
                ->paginate(10);
        //d($data,1);
        return view('admin.subscriptions.index', $data);
    }

    public function currentSubscription() {

        $data['model'] = DB::table('ambassadors_subscriptions')
                ->join('users as u', 'u.id', '=', 'ambassadors_subscriptions.ambassador_id')
                ->join('users as v', 'v.id', '=', 'ambassadors_subscriptions.athlete_id')
                ->join('transactions as tx', 'tx.type_id', '=', 'v.id')
                ->select('ambassadors_subscriptions.*', 'u.firstName as ambassador_firstName', 'u.lastName as ambassador_lastName', 'v.firstName as athlete_firstName', 'v.lastName as athlete_lastName', 'u.role_id as amb_role', 'v.role_id as ath_role', 'tx.amount')
                ->where('u.status', 1)
                ->where('v.status', 1)
                ->where('ambassadors_subscriptions.subscriptionStatus',1)
                ->orderBy('ambassadors_subscriptions.id', 'desc')
                ->paginate(10);
        //d($data,1);
        return view('admin.subscriptions.current', $data);
    }

}
