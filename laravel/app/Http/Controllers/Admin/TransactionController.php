<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
//use DB;
use App\Transactions;
//use Illuminate\Http\Request;

class TransactionController extends AdminController {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['model'] = Transactions::leftJoin('payflow_profiles as p','p.transaction_id','=','transactions.id')
                ->select('transactions.*','p.profileId','p.message')
                ->where('transactions.status', 1)->where('transactions.deleted',0)
                ->orderBy('transactions.id','desc')->paginate(15);

        return view('admin.transactions.index', $data);
    }

}
