<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use DB;
use App\Orders;
use App\Transactions;
use Carbon\Carbon;

class UpdatesController extends AdminController {

    public function __construct() {
        parent::__construct();
        $this->currentDay = Carbon::today();
        $now = Carbon::now();
        $this->currentMonth = $now->month;
        $this->currentWeekStart = $now->startOfWeek() . '';
        $this->currentWeekEnd = $now->endOfWeek();
    }

    public function monthly($type) {
        if ($type == 'users') {
            //Users
            $data['model'] = User::join('roles as r', 'r.id', '=', 'users.role_id')
                    ->select('users.*', 'r.role')
                    ->whereRaw('MONTH(created_at) = ?', [$this->currentMonth])
                    ->where('role_id', '!=', 1)
                    ->orderBy('users.id', 'desc')
                    ->get();
        }
        if ($type == 'orders') {
            //Orders
            $data['model'] = Orders::whereRaw('MONTH(created_at) = ?', [$this->currentMonth])
                    ->where('deleted', 0)
                    ->get();
        }
        if ($type == 'subscriptions') {
            //Subscriptions  
            $data['model'] = DB::table('ambassadors_subscriptions')
                    ->join('users as u', 'u.id', '=', 'ambassadors_subscriptions.ambassador_id')
                    ->join('users as v', 'v.id', '=', 'ambassadors_subscriptions.athlete_id')
                    ->join('transactions as tx', 'tx.type_id', '=', 'v.id')
                    ->select('ambassadors_subscriptions.*', 'u.firstName as ambassador_firstName', 'u.lastName as ambassador_lastName', 'v.firstName as athlete_firstName', 'v.lastName as athlete_lastName', 'u.role_id as amb_role', 'v.role_id as ath_role', 'tx.amount')
                    ->where('u.status', 1)
                    ->where('v.status', 1)
                    ->where('subscriptionStatus', 1)
                    ->whereRaw('MONTH(subscriptionDate) = ?', [$this->currentMonth])
                    ->get();
        }
        if ($type == 'transactions') {
            //Transactions
            $data['model'] = Transactions::leftJoin('payflow_profiles as p', 'p.transaction_id', '=', 'transactions.id')
                    ->select('transactions.*', 'p.profileId', 'p.message')
                    ->where('transactions.status', 1)
                    ->where('transactions.deleted', 0)
                    ->whereRaw('MONTH(transactions.created_at) = ?', [$this->currentMonth])
                    ->orderBy('transactions.id', 'desc')
                    ->get();
        }
        return view('admin.updates.monthly.' . $type, $data);
    }

    public function weekly($type) {
        if ($type == 'users') {
            //Users
            $data['model'] = User::join('roles as r', 'r.id', '=', 'users.role_id')
                    ->select('users.*', 'r.role')
                    ->where('role_id', '!=', 1)
                    ->whereBetween('created_at', [$this->currentWeekStart, $this->currentWeekEnd])
                    ->orderBy('users.id', 'desc')
                    ->get();
        }
        if ($type == 'orders') {
            $data['model'] = Orders::where('deleted', 0)
                    ->whereBetween('created_at', [$this->currentWeekStart, $this->currentWeekEnd])
                    ->get();
        }
        if ($type == 'subscriptions') {
            $data['model'] = DB::table('ambassadors_subscriptions')
                    ->join('users as u', 'u.id', '=', 'ambassadors_subscriptions.ambassador_id')
                    ->join('users as v', 'v.id', '=', 'ambassadors_subscriptions.athlete_id')
                    ->join('transactions as tx', 'tx.type_id', '=', 'v.id')
                    ->select('ambassadors_subscriptions.*', 'u.firstName as ambassador_firstName', 'u.lastName as ambassador_lastName', 'v.firstName as athlete_firstName', 'v.lastName as athlete_lastName', 'u.role_id as amb_role', 'v.role_id as ath_role', 'tx.amount')
                    ->where('u.status', 1)
                    ->where('v.status', 1)
                    ->where('subscriptionStatus', 1)
                    ->whereBetween('subscriptionDate', [$this->currentWeekStart, $this->currentWeekEnd])
                    ->get();
        }
        if ($type == 'transactions') {
            $data['model'] = Transactions::leftJoin('payflow_profiles as p', 'p.transaction_id', '=', 'transactions.id')
                    ->select('transactions.*', 'p.profileId', 'p.message')
                    ->where('transactions.status', 1)
                    ->where('transactions.deleted', 0)
                    ->whereBetween('transactions.created_at', [$this->currentWeekStart, $this->currentWeekEnd])
                    ->orderBy('transactions.id', 'desc')
                    ->get();
        }
        return view('admin.updates.weekly.' . $type, $data);
    }

    public function daily($type) {
        if ($type == 'users') {
            //Users
            $data['model'] = User::join('roles as r', 'r.id', '=', 'users.role_id')
                    ->select('users.*', 'r.role')
                    ->whereRaw('DATE(created_at) = ?', [$this->currentDay])
                    ->where('role_id', '!=', 1)
                    ->orderBy('users.id', 'desc')
                    ->get();
        }
        if ($type == 'orders') {
            $data['model'] = Orders::whereRaw('DATE(created_at) = ?', [$this->currentDay])
                    ->where('deleted', 0)
                    ->get();
        }
        if ($type == 'subscriptions') {
            $data['model'] = DB::table('ambassadors_subscriptions')
                    ->join('users as u', 'u.id', '=', 'ambassadors_subscriptions.ambassador_id')
                    ->join('users as v', 'v.id', '=', 'ambassadors_subscriptions.athlete_id')
                    ->join('transactions as tx', 'tx.type_id', '=', 'v.id')
                    ->select('ambassadors_subscriptions.*', 'u.firstName as ambassador_firstName', 'u.lastName as ambassador_lastName', 'v.firstName as athlete_firstName', 'v.lastName as athlete_lastName', 'u.role_id as amb_role', 'v.role_id as ath_role', 'tx.amount')
                    ->where('u.status', 1)
                    ->where('v.status', 1)
                    ->where('subscriptionStatus', 1)
                    ->whereRaw('DATE(subscriptionDate) = ?', [$this->currentDay])
                    ->get();
        }
        if ($type == 'transactions') {
            $data['model'] = Transactions::leftJoin('payflow_profiles as p', 'p.transaction_id', '=', 'transactions.id')
                    ->select('transactions.*', 'p.profileId', 'p.message')
                    ->where('transactions.status', 1)
                    ->where('transactions.deleted', 0)
                    ->whereRaw('DATE(transactions.created_at) = ?', [$this->currentDay])
                    ->orderBy('transactions.id', 'desc')
                    ->get();
        }

        return view('admin.updates.daily.' . $type, $data);
    }

}
