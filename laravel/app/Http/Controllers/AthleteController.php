<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;
use App\Role;
use App\Address;
use DB;
//use App\PaymentPayflow;
use App\Transactions;
use App\PayflowProfiles;
use App\Cards;
use Session;
use App\Functions\Payflow;
use App\Charges;
use Config;
use App\Functions\Functions;

class AthleteController extends Controller {

    public $auth;

    public function __construct() {
        $this->middleware('auth');
        $charges = Charges::get();
        $this->rate = array();
        $this->type = array();
        $this->name = array();
        if (count($charges) > 0) {
            foreach ($charges as $value) {
                $this->rate[] = $value->rate;
                $this->type[] = $value->type;
                $this->name[] = $value->name;
            }
        }
//        d($this->type);
//       die;
    }

    public function register() {
        $data = array();

        $data['joiningRate'] = $this->rate[0];
        $data['joining'] = $this->name[0];
//        $data['mealPlanRate'] = $this->rate[1];
//        $data['workoutPlanRate'] = $this->rate[2];
//        $data['fullServiceRate'] = $this->rate[3];
//
//        $data['mealPlanType'] = $this->type[1];
//        $data['workoutPlanType'] = $this->type[2];
//        $data['fullServiceType'] = $this->type[3];
        $data['charges'] = $charges = Charges::where('type','!=','joining')->get();

        if (Auth::user()->role->role != 'user') {
            return redirect('profile');
        } else {
            $membershipCancel = DB::table('users as u')->whereIn('id', function($q) {
                        $user_id = Auth::user()->id;
                        $q->select('user_id')->from('membership_cancel')->where('user_id', $user_id);
                    })->get();
            if (count($membershipCancel) > 0) {
                return redirect('membership');
            } else {
                return view('front.athlete.register', $data);
            }
        }
    }

    public function postregister(Request $request) {
        $rules = array(
            'cc' => 'required',
            'cvc' => 'required',
            'expMonth' => 'required',
            'expYear' => 'required'
        );
        d($request->all());

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $data = array();
        $user_id = Auth::user()->id;
        $expDate = sprintf("%02d", $request->expMonth) . '' . substr($request->expYear, 2, 3);
//
//        $q = "TRXTYPE=A&TENDER=C&VENDOR=" . env('PAYFLOW_VENDOR') . "&PARTNER=" . env('PAYFLOW_PARTNER') . "&USER=" . env('PAYFLOW_USER') . "&PWD=" . env('PAYFLOW_PASSWORD') . "&AMT=" . Config('params.subscription_fee') . "&ACCT=" . $request->cc . "&EXPDATE=" . $expDate . "&INVNUM=" . $user_id . "&VERBOSITY=HIGH";
//
//
//        $PayFlow = new PayFlow(env('PAYFLOW_VENDOR'), env('PAYFLOW_PARTNER'), env('PAYFLOW_USER'), env('PAYFLOW_PASSWORD'), 'recurring');
//
//        $PayFlow->setEnvironment(env('PAYFLOW_ENV'));  // test or live
//        $PayFlow->setInvoice($user_id);
//        //$PayFlow->setTransactionType('A');
////        $PayFlow->setPaymentMethod('C');
////        //$PayFlow->setAmount(Config('params.subscription_fee'), FALSE);
//        $PayFlow->setOptionalTransactionType('S');
//        $PayFlow->setPaymentMethod('C');
//        $PayFlow->setOptionalTransactionAmount(Config('params.sale_fee'), FALSE);
//        $PayFlow->setCCNumber($request->cc);
//        $PayFlow->setCVV($request->cvc);
//        $PayFlow->setExpiration($expDate);
//        $PayFlow->setCreditCardName($request->name);
//        $PayFlow->setCustomerFirstName(Auth::user()->firstName);
//        $PayFlow->setCustomerLastName(Auth::user()->lastName);
//        $PayFlow->setCustomerCountry('US');
//        $PayFlow->setCustomerEmail(Auth::user()->email);
        // if ($PayFlow->processTransaction()) {

        $response = $this->subscribe($request);

        if ($response['error'] == 0) {

            $ccModel = new Cards();
            $ccModel->user_id = $user_id;
            $ccModel->expDate = $expDate;
            $ccModel->cc = $request->cc;
            $ccModel->cvc = $request->cvc;
            $ccModel->name = $request->name;
            $ccModel->save();

            $roleModel = Role::where('role', 'athlete')->first();
            User::where('id', Auth::user()->id)->update(array('role_id' => $roleModel->id));
            $data['firstName'] = Auth::user()->firstName;
            $data['lastName'] = Auth::user()->lastName;
            $subject = view('emails.subscription.subject');
            $body = view('emails.subscription.body', $data);
            Functions::sendEmail(Auth::user()->email, $subject, $body);
            return redirect('athlete/success');
        }
//        } else {
//            $response = $PayFlow->getResponse();
//        }
        $response['RESPMSG'] = isset($response['RESPMSG']) ? $response['RESPMSG'] : "";
        $validator->errors()->add('error_response', $response['RESPMSG']);
        return redirect()->back()->withErrors($validator->errors());
    }

    public function subscribe($request) {

        $charges = Charges::where('type', $request->type)->first();

        $subscriptionRate = $charges->rate;
        $sale_fee = $this->rate[0];

        $currency = Config('params.currency_default');
        $user_id = Auth::user()->id;
        $address = Address::where('user_id', '=', $user_id)->first();

        $PayFlow = new PayFlow(env('PAYFLOW_VENDOR'), env('PAYFLOW_PARTNER'), env('PAYFLOW_USER'), env('PAYFLOW_PASSWORD'), 'recurring');

        $PayFlow->setEnvironment(env('PAYFLOW_ENV'));                           // test or live
        $PayFlow->setTransactionType('R');
        $PayFlow->setOptionalTransactionType('S');
        $PayFlow->setPaymentMethod('C');
        $PayFlow->setPaymentCurrency($currency);
        $expDate = sprintf("%02d", $request->expMonth) . '' . substr($request->expYear, 2, 3);
        $PayFlow->setAmount($subscriptionRate, FALSE);
        $PayFlow->setOptionalTransactionAmount($sale_fee, FALSE);
        $PayFlow->setCCNumber($request->cc);
        $PayFlow->setCVV($request->cvc);
        $PayFlow->setExpiration($expDate);
        $PayFlow->setCreditCardName($request->name);
        $PayFlow->setProfileAction('A');
        $PayFlow->setProfileName($request->name);
        $PayFlow->setProfileStartDate(date('mdY', strtotime("+1 day")));
        $PayFlow->setProfilePayPeriod('MONT');
        $PayFlow->setProfileTerm(0);

        $PayFlow->setCustomerFirstName(Auth::user()->firstName);
        $PayFlow->setCustomerLastName(Auth::user()->lastName);
        $PayFlow->setCustomerAddress($address->address . ' - ' . $address->address2);
        $PayFlow->setCustomerCity($address->city);
        $PayFlow->setCustomerState($address->state);
        $PayFlow->setCustomerZip($address->zip);
        $PayFlow->setCustomerCountry('US');
        $PayFlow->setCustomerPhone($address->phone);
        $PayFlow->setCustomerEmail(Auth::user()->email);
        $PayFlow->setPaymentComment('Subscriber User ID : ' . Auth::user()->id);
        $PayFlow->setPaymentComment2('Subscription');
        //d($PayFlow,1);

        if ($PayFlow->processTransaction()) {
            $response = $PayFlow->getResponse();
            $model = new Transactions();
            $model->transactionId = $response['TRXPNREF'];
            $model->gateway = 'payflow';
            $model->type_id = Auth::user()->id;
            $model->type = $request->type;
            $model->amount = $subscriptionRate;
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();

            $modelPaypal = new PayflowProfiles();
            $modelPaypal->transaction_id = $model->id;
            $modelPaypal->profileId = $response['PROFILEID'];
            $modelPaypal->rpref = $response['RPREF'];
            $modelPaypal->message = $response['RESPMSG'];
            $modelPaypal->created_at = date('Y-m-d H:i:s');
            $modelPaypal->save();

            $response['error'] = 0;

            /*
              $subscribed = new AmbassadorsSubscriptions;
              $subscribed->ambassador_id = $request->ambassador_id;
              $subscribed->athlete_id = $request->athlete_id;
              $subscribed->subscriptionRate = $request->subscriptionRate;
              $subscribed->subscriptionComission = $request->subscriptionComission;
              $subscribed->subscriptionDate = date('Y-m-d H:i:s');
              $subscribed->save();
             * 
             */
        } else {
            $response = $PayFlow->getResponse();
            $response['error'] = 1;
        }

        return $response;
    }

    public function unsubscribe(Request $request) {
        
    }

    public function success() {

        $data = array();
        return view('front.athlete.success', $data);
    }

}
