<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use App\User;
use Auth;
use Session;
use App\Certifications;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Functions\Functions;
use Intervention\Image\Facades\Image as Image;

class CertificationsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    use AuthenticatesAndRegistersUsers;

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        //d('Success',1);

        $user_id = Auth::user()->id;
//        $user = User::findOrFail($user_id);
//        $states = States::get();
        $certifications = Certifications::where('user_id', '=', $user_id)->get();
//        $countries = Countries::get();
//
        return view('front.certifications.index', compact('certifications'));
    }

    public function create() {
        return view('front.certifications.create');
    }

    public function store(Request $request) {
        $rules['image'] = 'required|mimes:jpeg,bmp,png,jpg,pdf';
        $rules['title'] = 'required';
        $rules['description'] = 'required|max:200';
        $rules['year'] = 'required';
        $validator = Validator::make($request->all(), $rules);
        $fileName = "";
        if ($validator->fails()) {
            return redirect('certifications/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $user_id = Auth::user()->id;
        if (isset($request->image)) {
            $imageName = $request->image->getClientOriginalName();
            $request->image->move(public_path('uploads/users/'), $imageName);
        } else {
            $imageName = 'noimage.jpg';
        }

        $certificate = new Certifications();
        $certificate->title = $request->title;
        $certificate->description = $request->description;
        $certificate->year = $request->year;
        $certificate->user_id = $user_id;
        $certificate->file = $imageName;

        $certificate->save();
        $request->session()->flash('alert-success', 'Successfully Added!');
        return redirect('certifications/create');
    }

    public function edit(Request $request, $id) {

        $user_id = Auth::user()->id;
        $certifications = Certifications::where('id', '=', $id)->first();
        return view('front.certifications.edit', compact('certifications'))->with('user_id', $user_id);
    }

    public function update(Request $request) {
        $rules['image1'] = 'mimes:jpeg,bmp,png,jpg,pdf';
        $rules['title'] = 'required';
        $rules['description'] = 'required|max:200';
        $rules['year'] = 'required';
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        } else {

            if (isset($request->image1)) {
                $imageName = $request->image1->getClientOriginalName();
                $request->image1->move(public_path('uploads/users/'), $imageName);
            } else {
                $imageName = $request->image;
            }

            $data = $request->all();
            array_forget($data, '_token');
            $certificate['title'] = $request->title;
            $certificate['description'] = $request->description;
            $certificate['year'] = $request->year;
            $certificate['file'] = $imageName;

            $affectedRows = Certifications::where('id', '=', $request->certificate_id)->update($certificate);
            Session::flash('success', 'Successfully Updated.');
            return redirect()->back();
        }
    }

    public function delete($id, Request $request) {
        $row = Certifications::where('id', '=', $id)->delete();
        $request->session()->flash('alert-success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
