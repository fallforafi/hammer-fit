<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use App\Contactus;
use App\User;
use Illuminate\Http\Request;
use App\Functions\Functions;

class ContactusController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    // use CaptchaTrait;

    public function __construct() {
        
    }

    // Contact-us
    public function index() {
        return view('front.contact-us');
    }

    public function store(Request $request) {
        $validation = array(
            'f_name' => 'required|max:30',
            'l_name' => 'required|max:30',
            'email' => 'required|email|max:30',
            'phone_number' => 'required|max:30',
            'message' => 'required|min:6|max:200',
            'g-recaptcha-response' => 'required',
                //'captcha'               => 'required'
        );
        $messages = [
            'g-recaptcha-response.required' => 'Captcha is required',
                //'captcha.min'           => 'Wrong captcha, please try again.'
        ];


        $validator = Validator::make($request->all(), $validation, $messages);
        //$validator = Validator::make($request->all(),$validation);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
//
//        $contactus = new Contactus;
//        $contactus->f_name = $request->f_name;
//        $contactus->l_name = $request->l_name;
//        $contactus->email = $request->email;
//        $contactus->phone_number = $request->phone_number;
//        $contactus->message = $request->message;
//        $contactus->save();
        $input = $request->all();
        unset($input['_token']);
        $user = User::where('role_id', 1)->get();
//        $emails = array();
//        foreach ($user as $value) {
//            $emails[] = $value->email;
//        }
//        $subject = view('emails.contactus_email.subject');
//        $body = view('emails.contactus_email.body', compact('input'));
//        Functions::sendEmail($emails, $subject, $body);
        foreach ($user as $value) {
            $subject = view('emails.contactus_email.subject');
            $body = view('emails.contactus_email.body', compact('input'));
            Functions::sendEmail($value->email, $subject, $body);
        }
        $request->session()->flash('alert-success', 'Successfully Submitted, Thanks for contacting us!');
        return redirect('contact-us');
    }

}
