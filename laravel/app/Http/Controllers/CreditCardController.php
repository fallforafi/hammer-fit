<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;
use App\Address;
//use App\PaymentPayflow;
use App\Transactions;
use App\PayflowProfiles;
use App\Cards;
use Session;
use App\Functions\Payflow;
//use App\Charges;
use Config;
use App\Functions\Functions;

class CreditCardController extends Controller {

    public $auth;

    public function __construct() {
        $this->middleware('auth');
//$this->subscription_fee = Charges::where('type','subscription_fee')->lists('rate');
        $this->subscription_fee = Config::get('params.subscription_fee');
    }

    public function index() {
        $data = array();
// $data['subscription_fee'] = $this->subscription_fee[0];
        if (Auth::user()->role->role != 'athlete') {
            return redirect('home');
        } else {
            return view('front.membership.update_credit_card.register', $data);
        }
    }

    public function save(Request $request) {

        $rules = array(
            'cc' => 'required',
            'cvc' => 'required',
            'expMonth' => 'required',
            'expYear' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $data = array();
        $user_id = Auth::user()->id;
        $subscriptions = Transactions::join('payflow_profiles as p', 'p.transaction_id', '=', 'transactions.id')
                ->select('transactions.*', 'p.profileId', 'p.message')
                ->where('transactions.status', 1)
                ->where('transactions.deleted', 0)
                ->where('transactions.type_id', $user_id)
                ->orderBy('p.id', 'desc')
                ->first();
        $data['subscriptions'] = $subscriptions;

        $address = Address::where('user_id', '=', $user_id)->first();
        $data['address'] = $address;

        $expDate = sprintf("%02d", $request->expMonth) . '' . substr($request->expYear, 2, 3);
        DB::beginTransaction();
        try {
            $q = "TRXTYPE=R&TENDER=C&VENDOR=" . env('PAYFLOW_VENDOR') . "&PARTNER=" . env('PAYFLOW_PARTNER') . "&USER=" . env('PAYFLOW_USER') . "&PWD=" . env('PAYFLOW_PASSWORD') . "&ACTION=C&ORIGPROFILEID=" . $subscriptions->profileId . "&VERBOSITY=HIGH";

            $PayFlow = new PayFlow(env('PAYFLOW_VENDOR'), env('PAYFLOW_PARTNER'), env('PAYFLOW_USER'), env('PAYFLOW_PASSWORD'), 'recurring');

            $PayFlow->setEnvironment(env('PAYFLOW_ENV'));  // test or live
            $PayFlow->setInvoice($user_id);
            $PayFlow->setTransactionType('R');
            $PayFlow->setPaymentMethod('C');
            $PayFlow->setAmount($subscriptions->amount, FALSE);
            $PayFlow->setCCNumber($request->cc);
            $PayFlow->setCVV($request->cvc);
            $PayFlow->setExpiration($expDate);
            $PayFlow->setCreditCardName($request->name);
            $PayFlow->setProfileName($request->name);
            $PayFlow->setCustomerFirstName(Auth::user()->firstName);
            $PayFlow->setCustomerLastName(Auth::user()->lastName);
            $PayFlow->setCustomerCountry('US');
            $PayFlow->setCustomerEmail(Auth::user()->email);
            $PayFlow->setProfileAction('M');
            $PayFlow->setProfileId($subscriptions->profileId);
//            $PayFlow->setProfileStartDate(date('mdY', strtotime("+1 day")));
//            $PayFlow->setProfilePayPeriod('MONT');
            $PayFlow->setProfileTerm(0);

            $PayFlow->setCustomerFirstName(Auth::user()->firstName);
            $PayFlow->setCustomerLastName(Auth::user()->lastName);
            $PayFlow->setCustomerAddress($address->address . ' - ' . $address->address2);
            $PayFlow->setCustomerCity($address->city);
            $PayFlow->setCustomerState($address->state);
            $PayFlow->setCustomerZip($address->zip);
            $PayFlow->setCustomerCountry('US');
            $PayFlow->setCustomerPhone($address->phone);
            $PayFlow->setCustomerEmail(Auth::user()->email);
            $PayFlow->setPaymentComment('Subscriber User ID : ' . Auth::user()->id);
            $PayFlow->setPaymentComment2('Subscription');
            // d($PayFlow,1);
            if ($PayFlow->processTransaction()) {
                $response = $PayFlow->getResponse();
                //d($response,1);

                Cards::where('user_id', $user_id)->where('status', 1)->update([
                    'expDate' => $expDate,
                    'cc' => $request->cc,
                    'cvc' => $request->cvc,
                    'name' => $request->name,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                PayflowProfiles::where('profileId', $subscriptions->profileId)->where('status', 1)->update([
                    'profileId' => $response['PROFILEID'],
                    'rpref' => $response['RPREF'],
                    'message' => $response['RESPMSG'],
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                $data['card'] = Cards::where('user_id', $user_id)->where('status', 1)->orderBy('id', 'desc')->first();
                
                $data['userName'] = Auth::user()->firstName . ' ' . Auth::user()->lastName;

                $subject = view('emails.credit_card_update.subject');
                $body = view('emails.credit_card_update.body', $data);
                Functions::sendEmail(Auth::user()->email, $subject, $body);

                DB::commit();
                Session::flash('success', 'Successfully Updated!');

                return redirect()->back();
            } else {
                $response = $PayFlow->getResponse();
                //d($response);
                $response['RESPMSG'] = isset($response['RESPMSG']) ? $response['RESPMSG'] : "";
                Session::flash('alert-danger', $response['RESPMSG']);
                return redirect()->back();
            }
        } catch (Exception $e) {
            DB::rollBack();
            d($e);
            Session::flash('alert-danger', $e);
            return redirect()->back();
        }
    }

}
