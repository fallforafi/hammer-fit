<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Functions\Functions;
use Intervention\Image\Facades\Image as Image;

class CropController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    // use AuthenticatesAndRegistersUsers;

    public function __construct() {
        // $this->middleware('auth');
    }

    public function getHome() {
        return view('front.index');
    }

    public function postCrop(Request $request) {
        $response['success'] = 0;
        $input = $request->all();
        array_forget($input, '_token');
        $image_url = $input['src'];
        $filename_array = explode('/', $image_url);
        $fileName = $filename_array[sizeof($filename_array) - 1];
        $filePath = $filename_array[sizeof($filename_array) - 3];
        if ($filePath == 'front_images') {
            $uploadPath = public_path() . '/uploads/users/results/front_images/thumbnail/';
            $uploadUrl = URL() . '/uploads/users/results/front_images/thumbnail/';
        }
        if ($filePath == 'side_images') {
            $uploadPath = public_path() . '/uploads/users/results/side_images/thumbnail/';
            $uploadUrl = URL() . '/uploads/users/results/side_images/thumbnail/';
        }
        if ($filePath == 'back_images') {
            $uploadPath = public_path() . '/uploads/users/results/back_images/thumbnail/';
            $uploadUrl = URL() . '/uploads/users/results/back_images/thumbnail/';
        }
        $data = $input['image'];

        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);

        $data = base64_decode($data);

        file_put_contents($uploadPath . $fileName, $data);
        Image::make($uploadPath . $fileName)->resize(500, 400)->save($uploadPath . $fileName)->orientate();
        
        //file_put_contents($uploadPath . $filename, $data);

        $response['success'] = 1;

        return json_encode($response);
    }

}
