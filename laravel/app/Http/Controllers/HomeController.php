<?php

namespace App\Http\Controllers;

use App\Attributesvalues;
use App\Categories;
use App\Content;
use App\Functions\Functions;
use App\Products;
use App\ProductsImages;
use App\Specialities;
use App\States;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected $categories = array();
    protected $layout = 'layouts.search';

    public function __construct()
    {
        $this->categories = 1;
    }

    public function index()
    {

        $data['model'] = DB::table('users')
            ->leftJoin('address as add', 'add.user_id', '=', 'users.id')
            ->leftJoin('ambassadors_specialities', 'users.id', '=', 'ambassadors_specialities.ambassador_id')
            ->leftJoin('certifications', 'users.id', '=', 'certifications.user_id')
            ->select('users.*', 'add.state', 'ambassadors_specialities.speciality_id', DB::raw('COUNT(certifications.id) as c_id'))
            ->where('role_id', 3)
            ->where('users.status', 1)
            ->groupBy('users.id')
            ->orderBy('users.id', 'desc')
        //->limit(9)
            ->get();

        $data['specialities'] = Specialities::get();
        $data['states'] = States::get();

        $products = Products::getProducts(array('category_id' => 4));
        $data['products'] = $products;

        $data['apparels'] = Products::getProducts(array('category_id' => 1));

        return view('front.index', $data);
    }

    public function page()
    {

        $model = Content::where('code', 'home')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function aboutus()
    {
        $model = Content::where('code', 'aboutus')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function privacy()
    {
        $model = Content::where('code', 'privacy')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function terms()
    {
        $model = Content::where('code', 'terms')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }
    public function agreement()
    {
        $model = Content::where('code', 'agreement')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function getstarted()
    {
        $model = Content::where('code', 'get-started')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function contact()
    {
        $model = Content::where('code', 'contact')->first();
        $model->body = Functions::setTemplate($model->body, array());
        return view('front.page', compact('model'));
    }

    public function getproduct($id)
    {
        $product = Products::find($id);
        $productImages = array();
        $availabeInColors = array();
        $productImages = ProductsImages::where('product_id', '=', $id)->get();
        $availabeInColors = Attributesvalues::getImagesAndColors($id);
        $relatedProducts = array();
        //$relatedProducts=Products::where('category_id','=',$product->category_id)->limit(3)->get();
        //$category= Categories::find($product->category_id);
        $category = array();
        $attributes = DB::table('attributes as a')
            ->where('pa.product_id', '=', $id)
            ->leftJoin('products_attributes as pa', 'pa.attribute_id', '=', 'a.id')
            ->leftJoin('attributes_values as av', 'av.attribute_id', '=', 'a.id')
            ->select('a.id as attribute_id', 'a.name as name', 'a.type as type', DB::raw('group_concat(av.id) as value_id'), DB::raw('group_concat(av.name) as value_names'), DB::raw('group_concat(av.price) as value_price'))
            ->groupBy('a.id')
            ->orderBy('av.sortOrder', 'asc')
            ->get();

        return view('front.product', compact('product', 'category', 'relatedProducts', 'attributes', 'productImages', 'availabeInColors'))->with('id', $id);
    }

    public function sale()
    {
        $category = "";
        $products = Products::where('sale', '=', 1)->where('price', '>', 'salePrice')->get();
        return view('front.products', compact('products', 'category'));
    }

    public function products($id = 4)
    {
        $category = Categories::find($id);
        $products = Products::getProducts(array('category_id' => $id));
        return view('front.products', compact('products', 'category'));
    }

    public function apparel($id = 1)
    {
        $category = Categories::find($id);
        $products = Products::getProducts(array('category_id' => $id));
        return view('front.products', compact('products', 'category'));
    }

    public function messagePost(Request $request)
    {
        $validation = array('name' => 'required|max:30',
            'email' => 'required|email|max:30',
            'captcha' => 'required|captcha',
            'message' => 'required|min:6|max:200');

        $messages = [
            'captcha' => 'The :attribute field is invalid.',
        ];

        $validator = Validator::make($request->all(), $validation, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $guestbook = new Guestbook;
        $guestbook->name = $request->name;
        $guestbook->email = $request->email;
        $guestbook->message = $request->message;
        $guestbook->save();
        return redirect('guestbook')->withInput();
    }

}
