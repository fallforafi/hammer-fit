<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator,
    Redirect;
use App\MealPlans;
use App\AthleteMp;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\User;
use App\Functions\Functions;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class MealPlansController extends Controller {

    use AuthenticatesAndRegistersUsers;

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user_id = Auth::user()->id;
        $data['mealPlans'] = MealPlans::where([
                    'user_id' => $user_id,
                    'deleted' => 0,
                ])->get();
//       // $dateTime = date('Y-m-d H:i:s', strtotime('-28 days'));
//        $mealPlans = MealPlans::join('athlete_mp as amp', 'mp_id', '=', 'meal_plans.id')
//                ->select('meal_plans.*', 'amp.created_at', 'amp.athlete_id', 'amp.mp_id')
//                ->where('meal_plans.deleted', 0)
//               // ->where('amp.created_at', '<=', $dateTime)
//                ->get();
//        $getAdmin = User::where('role_id', 1)->get();
//        $emails = array();
//        foreach ($getAdmin as $value) {
//            $emails[] = $value->email;
//        }
//        foreach ($mealPlans as $row) {
//
//            $getAthlete = User::where('id', $row->athlete_id)->where('status', 1)->where('deleted', 0)->first();
//            $getAmbassador = User::where('id', Auth::user()->id)->where('status', 1)->where('deleted', 0)->first();
//            $data['athleteName'] = $getAthlete->firstName . ' ' . $getAthlete->lastName;
//            $data['ambassadorName'] = $getAmbassador->firstName . ' ' . $getAmbassador->lastName;
//            
//            $subject = view('emails.crons.meal_plan_reminder_to_coach.subject');
//            $body = view('emails.crons.meal_plan_reminder_to_coach.body', compact('data'));
//            Functions::sendEmail($getAmbassador->email, $subject, $body, '', '', $emails);
//        }
//        d(count($user));
//        d($user, 1);
        return view('front.meal-plans.index', $data);
    }

    public function create() {
        return view('front.meal-plans.create');
    }

    public function store(Request $request) {
        $rules['file'] = 'required|mimes:pdf';
        $rules['title'] = 'required|max:100';
        $rules['description'] = 'required|max:200';
        $validator = Validator::make($request->all(), $rules);
        $fileName = "";
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        }
        $user_id = Auth::user()->id;
        if (isset($request->file)) {
            $destinationPath = public_path('uploads/users/meal-plans/');
            $file = $request->file;
            $fileName = Functions::saveImage($file, $destinationPath);
        }

        $mealPlan = new MealPlans();
        $mealPlan->title = $request->title;
        $mealPlan->description = $request->description;
        $mealPlan->user_id = $user_id;
        $mealPlan->file = $fileName;
        $mealPlan->save();
        Session::flash('success', 'Successfully Added!');
        return redirect('meal-plans');
    }

    public function edit(Request $request, $id) {
        $user_id = Auth::user()->id;
        $data['mealPlans'] = MealPlans::where('id', '=', $id)->first();
        return view('front.meal-plans.edit', $data)->with('user_id', $user_id);
    }

    public function update(Request $request) {
        $rules['file1'] = 'mimes:pdf';
        $rules['title'] = 'required';
        $rules['description'] = 'required|max:200';
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        } else {
            if (isset($request->file1)) {
                $destinationPath = public_path('uploads/users/meal-plans/');
                $file = $request->file1;
                $fileName = Functions::saveImage($file, $destinationPath);
            } else {
                $fileName = $request->file;
            }
            $data = $request->all();
            array_forget($data, '_token');
            $model['title'] = $request->title;
            $model['description'] = $request->description;
            $model['file'] = $fileName;
            MealPlans::where('id', '=', $request->id)->update($model);
            Session::flash('success', 'Successfully Updated.');
            return redirect()->back();
        }
    }

    public function delete($id) {
        MealPlans::where('id', '=', $id)->update([
            'deleted' => 1,
        ]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function addPlan(Request $request) {
        $rules['file'] = 'required|mimes:pdf';
        $rules['title'] = 'required|max:100';
        $rules['description'] = 'required|max:200';
        $validator = Validator::make($request->all(), $rules);
        $fileName = "";
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        }
        $user_id = Auth::user()->id;
        if (isset($request->file)) {
            $destinationPath = public_path('uploads/users/meal-plans/');
            $file = $request->file;
            $fileName = Functions::saveImage($file, $destinationPath);
        }
        $mealPlan = new MealPlans();
        $mealPlan->title = $request->title;
        $mealPlan->description = $request->description;
        $mealPlan->user_id = $user_id;
        $mealPlan->file = $fileName;
        $mealPlan->save();

        $athleteMp = new AthleteMp();
        $athleteMp->mp_id = $mealPlan->id;
        $athleteMp->athlete_id = $request->athlete_id;
        $athleteMp->save();

        $getAthlete = User::where('id', $athleteMp->athlete_id)->where('status', 1)->where('deleted', 0)->first();
        $getAmbassador = User::where('id', $user_id)->where('status', 1)->where('deleted', 0)->first();

        $data['athleteName'] = $getAthlete->firstName . ' ' . $getAthlete->lastName;
        $data['ambassadorName'] = $getAmbassador->firstName . ' ' . $getAmbassador->lastName;

        //Email sent to Athlete that tells the Athlete meal plan added by Coach
        $subjectAthlete = view('emails.add_mealPlan_mailto_athlete.subject');
        $bodyAthlete = view('emails.add_mealPlan_mailto_athlete.body', compact('data'));
        Functions::sendEmail($getAthlete->email, $subjectAthlete, $bodyAthlete);

        Session::flash('success', 'Successfully Added!');
        return redirect()->back();
    }

}
