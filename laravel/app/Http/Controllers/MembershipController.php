<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Transactions;
use Session;
use App\Functions\Functions;
use App\AmbassadorAthletes;
use App\AmbassadorsSubscriptions;
use App\MembershipCancel;
use App\Functions\Payflow;
use App\PayflowProfiles;
use App\Cards;
use App\Charges;
use Validator;
use DB;
use App\Address;
use Illuminate\Http\Request;

class MembershipController extends Controller {

    public $auth;

    public function __construct() {
        $this->middleware('auth');
        $charges = Charges::get();
        $this->rate = array();
        $this->type = array();
        if (count($charges) > 0) {
            foreach ($charges as $value) {
                $this->rate[] = $value->rate;
                $this->type[] = $value->type;
            }
        }
    }

    public function index() {
        $user_id = Auth::user()->id;
        if (Auth::user()->role->role == 'admin') {
            return redirect('home');
        } else {
            $membershipCheck = MembershipCancel::join('users as u', 'u.id', '=', 'membership_cancel.user_id')
                    ->where([
                        'membership_cancel.user_id' => $user_id,
                        'membership_cancel.status' => 1,
                        'membership_cancel.deleted' => 0,
                    ])
                    ->first();
            if (Auth::user()->role_id == 4 && count($membershipCheck) == 0) {
                return redirect('athlete/register');
            }
            $data = array();
//            $data['subscriptions'] = Transactions::join('payflow_profiles as p', 'p.transaction_id', '=', 'transactions.id')
//                    ->select('transactions.*', 'p.profileId', 'p.message')
////                    ->where('transactions.status', 1)
////                    ->where('transactions.deleted', 0)
//                    ->where('transactions.type_id', $user_id)
//                    ->orderBy('transactions.id', 'desc')
//                    ->get();
            $data['subscriptions'] = Transactions::where('type_id', $user_id)
                    ->orderBy('id', 'desc')
                    ->get();

            $membershipCancel = MembershipCancel::join('users as u', 'u.id', '=', 'membership_cancel.user_id')
                    ->where(['membership_cancel.user_id' => $user_id, 'membership_cancel.status' => 1, 'membership_cancel.deleted' => 0])
                    ->where('u.role_id', 4)
                    ->first();
            $transactions = Transactions::where('status', 1)->where('deleted', 0)
                            ->where('type_id', $user_id)->first();
            $data['currentSubscriptions'] = Null;
            $data['charges'] = Null;
            if (isset($transactions->type) &&
                    $transactions->type != 'rejoin' &&
                    $transactions->type != 'subscription') {
                $data['currentSubscriptions'] = Charges::where('type', $transactions->type)->first();
                $data['charges'] = Charges::where('type', '!=', 'joining')->where('type', '!=', $transactions->type)->get();
            } else {
                $data['charges'] = Charges::where('type', '!=', 'joining')->get();
            }

            //d($data['currentSubscriptions']->type,1);

            $data['membershipCancel'] = $membershipCancel;
            $data['joiningRate'] = $this->rate[0];
            $data['mealPlanRate'] = $this->rate[1];
            $data['workoutPlanRate'] = $this->rate[2];
            $data['fullServiceRate'] = $this->rate[3];

            $data['joiningType'] = $this->type[0];
            $data['mealPlanType'] = $this->type[1];
            $data['workoutPlanType'] = $this->type[2];
            $data['fullServiceType'] = $this->type[3];

            return view('front.membership.index', $data);
        }
    }

    public function cancel() {
        $user_id = Auth::user()->id;
        $subscriptions = Transactions::join('payflow_profiles as p', 'p.transaction_id', '=', 'transactions.id')
                ->select('transactions.*', 'p.profileId', 'p.message')
                ->where('transactions.status', 1)
                ->where('p.status', 1)
                ->where('transactions.deleted', 0)
                ->where('transactions.type_id', $user_id)
                ->first();
        //d($subscriptions, 1);
        $data['subscriptions'] = $subscriptions;

        $card = Cards::where('user_id', $user_id)->where('status', 1)->first();
        $data['card'] = $card;

        //$expDate = sprintf("%02d", $request->expMonth) . '' . substr($request->expYear, 2, 3);
        // d($subscriptions, 1);

        DB::beginTransaction();
        try {
            //  $q = "TRXTYPE=R&TENDER=C&VENDOR=" . env('PAYFLOW_VENDOR') . "&PARTNER=" . env('PAYFLOW_PARTNER') . "&USER=" . env('PAYFLOW_USER') . "&PWD=" . env('PAYFLOW_PASSWORD') . "&ACTION=C&ORIGPROFILEID=" . $subscriptions->profileId . "&VERBOSITY=HIGH";

            $PayFlow = new PayFlow(env('PAYFLOW_VENDOR'), env('PAYFLOW_PARTNER'), env('PAYFLOW_USER'), env('PAYFLOW_PASSWORD'), 'recurring');

            $PayFlow->setEnvironment(env('PAYFLOW_ENV'));  // test or live
            $PayFlow->setInvoice($user_id);
            $PayFlow->setTransactionType('R');
            $PayFlow->setPaymentMethod('C');
            $PayFlow->setAmount($subscriptions->amount, FALSE);
            $PayFlow->setCCNumber($card->cc);
            $PayFlow->setCVV($card->cvc);
            $PayFlow->setExpiration($card->expDate);
            $PayFlow->setCreditCardName($card->name);
            $PayFlow->setCustomerFirstName(Auth::user()->firstName);
            $PayFlow->setCustomerLastName(Auth::user()->lastName);
            $PayFlow->setCustomerCountry('US');
            $PayFlow->setCustomerEmail(Auth::user()->email);
            $PayFlow->setProfileAction('C');
            $PayFlow->setProfileId($subscriptions->profileId);
            // d($PayFlow,1);
            if ($PayFlow->processTransaction()) {
                User::where(['id' => $user_id, 'role_id' => 2, 'deleted' => 0])->update([
                    'role_id' => 4
                ]);

                Transactions::where('type_id', $user_id)->update([
                    'status' => 0,
                    'deleted' => 1
                ]);

                Cards::where('user_id', $user_id)->update([
                    'status' => 0,
                    'deleted' => 1
                ]);

                AmbassadorsSubscriptions::where(['athlete_id' => $user_id, 'subscriptionStatus' => '1'])->update([
                    'subscriptionStatus' => '2'
                ]);

                AmbassadorAthletes::where(['athlete_id' => $user_id, 'subscriptionStatus' => '1'])->update([
                    'subscriptionStatus' => '2'
                ]);

                MembershipCancel::create(['user_id' => $user_id]);

                $data['firstName'] = Auth::user()->firstName;
                $data['lastName'] = Auth::user()->lastName;
                $subject = view('emails.membership_cancels.subject');
                $body = view('emails.membership_cancels.body', $data);

                $getAdmins = User::where(['role_id' => 1, 'deleted' => 0])->get();
                foreach ($getAdmins as $value) {
                    Functions::sendEmail($value->email, $subject, $body);
                }
                DB::commit();
                Session::flash('success', 'Successfully Cancelled!');

                return redirect('athlete/register');
            } else {
                $response = $PayFlow->getResponse();
                // d($response);
                $response['RESPMSG'] = isset($response['RESPMSG']) ? $response['RESPMSG'] : "";
                Session::flash('danger', $response['RESPMSG']);
                return redirect()->back();
            }
        } catch (Exception $e) {
            DB::rollBack();
            //d($e);
            Session::flash('danger', 'Your action has been unsuccessful, please try again!');
            return redirect()->back();
        }
    }

    public function rejoin(Request $request) {
        $charges = Charges::where('type', $request->type)->first();

        $rules = array(
            'cc' => 'required',
            'cvc' => 'required',
            'expMonth' => 'required',
            'expYear' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $data = array();
        $user_id = Auth::user()->id;
        $expDate = sprintf("%02d", $request->expMonth) . '' . substr($request->expYear, 2, 3);

        $subscriptions = Transactions::join('payflow_profiles as p', 'p.transaction_id', '=', 'transactions.id')
                ->select('transactions.*', 'p.profileId', 'p.message')
                //->where('transactions.status', 1)
                //->where('transactions.deleted', 0)
                ->where('transactions.type_id', $user_id)
                ->first();
        //$data['subscriptions'] = $subscriptions;

        DB::beginTransaction();
        try {
            // $q = "TRXTYPE=R&TENDER=C&VENDOR=" . env('PAYFLOW_VENDOR') . "&PARTNER=" . env('PAYFLOW_PARTNER') . "&USER=" . env('PAYFLOW_USER') . "&PWD=" . env('PAYFLOW_PASSWORD') . "&ACTION=C&ORIGPROFILEID=" . $subscriptions->profileId . "&VERBOSITY=HIGH";

            $PayFlow = new PayFlow(env('PAYFLOW_VENDOR'), env('PAYFLOW_PARTNER'), env('PAYFLOW_USER'), env('PAYFLOW_PASSWORD'), 'recurring');

            $PayFlow->setEnvironment(env('PAYFLOW_ENV'));  // test or live
            $PayFlow->setInvoice($user_id);
            $PayFlow->setTransactionType('R');
            $PayFlow->setPaymentMethod('C');
            $PayFlow->setAmount($charges->rate, FALSE);
            $PayFlow->setCCNumber($request->cc);
            $PayFlow->setCVV($request->cvc);
            $PayFlow->setExpiration($expDate);
            $PayFlow->setCreditCardName($request->name);
            $PayFlow->setProfileName($request->name);
            $PayFlow->setProfileStartDate(date('mdY', strtotime("+1 day")));
            $PayFlow->setProfilePayPeriod('MONT');
            $PayFlow->setProfileTerm(0);
            $PayFlow->setCustomerFirstName(Auth::user()->firstName);
            $PayFlow->setCustomerLastName(Auth::user()->lastName);
            $PayFlow->setCustomerCountry('US');
            $PayFlow->setCustomerEmail(Auth::user()->email);
            $PayFlow->setProfileAction('R');
            $PayFlow->setProfileId($subscriptions->profileId);
            // d($PayFlow,1);
            if ($PayFlow->processTransaction()) {

                $response = $PayFlow->getResponse();

                User::where(['id' => $user_id, 'role_id' => 4, 'deleted' => 0])->update([
                    'role_id' => 2,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                PayflowProfiles::where('profileId', $subscriptions->profileId)
                        ->update([
                            'status' => 0,
                            'deleted' => 1,
                            'updated_at' => date('Y-m-d H:i:s')
                ]);

                Cards::where('user_id', $user_id)->update([
                    'expDate' => $expDate,
                    'cc' => $request->cc,
                    'cvc' => $request->cvc,
                    'name' => $request->name,
                    'status' => 1,
                    'deleted' => 0,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                MembershipCancel::where('user_id', $user_id)->update([
                    'status' => 0,
                    'deleted' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                // d($response,1);

                $model = new Transactions();
                $model->transactionId = $subscriptions->transactionId;
                $model->gateway = 'payflow';
                $model->type_id = $user_id;
                $model->type = $request->type;
                $model->amount = $charges->rate;
                $model->rejoin = 1;
                $model->created_at = date('Y-m-d H:i:s');
                $model->save();

                $modelPaypal = new PayflowProfiles();
                $modelPaypal->transaction_id = $model->id;
                $modelPaypal->profileId = $response['PROFILEID'];
                $modelPaypal->rpref = $response['RPREF'];
                $modelPaypal->message = $response['RESPMSG'];
                $modelPaypal->created_at = date('Y-m-d H:i:s');
                $modelPaypal->save();



                $data['firstName'] = Auth::user()->firstName;
                $data['lastName'] = Auth::user()->lastName;
                $subject = view('emails.membership_rejoins.subject');
                $body = view('emails.membership_rejoins.body', $data);

                $getAdmins = User::where(['role_id' => 1, 'deleted' => 0])->get();
                foreach ($getAdmins as $value) {
                    Functions::sendEmail($value->email, $subject, $body);
                }
                DB::commit();
                //Session::flash('success', 'Successfully Cancelled!');
                return redirect('athlete/success');
            } else {
                $response = $PayFlow->getResponse();
                //d($response,1);
                $response['RESPMSG'] = isset($response['RESPMSG']) ? $response['RESPMSG'] : "";
                Session::flash('danger', $response['RESPMSG']);
                return redirect()->back();
            }
        } catch (Exception $e) {
            //d($e,1);
            DB::rollBack();
            Session::flash('danger', 'Your action has been unsuccessful, please try again!');
            return redirect()->back();
        }
    }

    public function update(Request $request) {

        $data = array();

        $rules = array(
            'type' => 'required',
        );
        $messages = array(
            'type.required' => 'You have to choose atleast one package',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $charges = Charges::where('type', $request->type)->first();
        $data['charges'] = $charges;

        $user_id = Auth::user()->id;
        $subscriptions = Transactions::join('payflow_profiles as p', 'p.transaction_id', '=', 'transactions.id')
                ->join('charges as c', 'c.type', '=', 'transactions.type')
                ->select('transactions.*', 'p.profileId', 'p.message', 'c.name')
//                ->where('transactions.status', 1)
//                ->where('transactions.deleted', 0)
                ->where('transactions.type_id', $user_id)
                ->orderBy('p.id', 'desc')
                ->first();
        $data['subscriptions'] = $subscriptions;

        $address = Address::where('user_id', '=', $user_id)->first();
        $data['address'] = $address;

        $card = Cards::where('user_id', $user_id)->where('status', 1)->first();
        $data['card'] = $card;
//        d($data, 1);

        DB::beginTransaction();
        try {
            //$q = "TRXTYPE=R&TENDER=C&VENDOR=" . env('PAYFLOW_VENDOR') . "&PARTNER=" . env('PAYFLOW_PARTNER') . "&USER=" . env('PAYFLOW_USER') . "&PWD=" . env('PAYFLOW_PASSWORD') . "&ACTION=C&ORIGPROFILEID=" . $subscriptions->profileId . "&VERBOSITY=HIGH";

            $PayFlow = new PayFlow(env('PAYFLOW_VENDOR'), env('PAYFLOW_PARTNER'), env('PAYFLOW_USER'), env('PAYFLOW_PASSWORD'), 'recurring');

            $PayFlow->setEnvironment(env('PAYFLOW_ENV'));  // test or live
            $PayFlow->setInvoice($user_id);
            $PayFlow->setTransactionType('R');
            $PayFlow->setPaymentMethod('C');
            $PayFlow->setAmount($charges->rate, FALSE);
            $PayFlow->setCCNumber($card->cc);
            $PayFlow->setCVV($card->cvc);
            $PayFlow->setExpiration($card->expDate);
            $PayFlow->setCreditCardName($card->name);
            $PayFlow->setCustomerFirstName(Auth::user()->firstName);
            $PayFlow->setCustomerLastName(Auth::user()->lastName);
            $PayFlow->setCustomerCountry('US');
            $PayFlow->setCustomerEmail(Auth::user()->email);
            $PayFlow->setProfileAction('M');
            $PayFlow->setProfileId($subscriptions->profileId);
            $PayFlow->setProfileStartDate(date('mdY', strtotime("+1 day")));
            $PayFlow->setProfilePayPeriod('MONT');
            $PayFlow->setProfileTerm(0);

            $PayFlow->setCustomerFirstName(Auth::user()->firstName);
            $PayFlow->setCustomerLastName(Auth::user()->lastName);
            $PayFlow->setCustomerAddress($address->address . ' - ' . $address->address2);
            $PayFlow->setCustomerCity($address->city);
            $PayFlow->setCustomerState($address->state);
            $PayFlow->setCustomerZip($address->zip);
            $PayFlow->setCustomerCountry('US');
            $PayFlow->setCustomerPhone($address->phone);
            $PayFlow->setCustomerEmail(Auth::user()->email);
            $PayFlow->setPaymentComment('Subscriber User ID : ' . Auth::user()->id);
            $PayFlow->setPaymentComment2($charges->name);
            // d($PayFlow,1);
            if ($PayFlow->processTransaction()) {

                $response = $PayFlow->getResponse();

                Transactions::where('type_id', $user_id)->update([
                    'status' => 0,
                    'deleted' => 1
                ]);
                PayflowProfiles::where('profileId', $subscriptions->profileId)
                        ->update([
                            'status' => 0,
                            'deleted' => 1,
                            'updated_at' => date('Y-m-d H:i:s')
                ]);

                $model = new Transactions();
                $model->transactionId = $subscriptions->transactionId;
                $model->gateway = 'payflow';
                $model->type_id = $user_id;
                $model->type = $charges->type;
                $model->amount = $charges->rate;
                $model->created_at = date('Y-m-d H:i:s');
                $model->save();

                $modelPaypal = new PayflowProfiles();
                $modelPaypal->transaction_id = $model->id;
                $modelPaypal->profileId = $response['PROFILEID'];
                $modelPaypal->rpref = $response['RPREF'];
                $modelPaypal->message = $response['RESPMSG'];
                $modelPaypal->created_at = date('Y-m-d H:i:s');
                $modelPaypal->save();

                $data['firstName'] = Auth::user()->firstName;
                $data['lastName'] = Auth::user()->lastName;
                $data['from'] = $subscriptions->name;
                $data['to'] = $charges->name;

                $subject = view('emails.membership_update.subject');
                $body = view('emails.membership_update.body', $data);

                $getAdmins = User::where(['role_id' => 1, 'deleted' => 0])->get();
                foreach ($getAdmins as $value) {
                    Functions::sendEmail($value->email, $subject, $body);
                }
                DB::commit();
                Session::flash('success', 'Successfully Changed');

                return redirect()->back();
            } else {
                $response = $PayFlow->getResponse();
                d($response,1);
                $response['RESPMSG'] = isset($response['RESPMSG']) ? $response['RESPMSG'] : "";
                Session::flash('danger', $response['RESPMSG']);
                return redirect()->back();
            }
        } catch (Exception $e) {
            DB::rollBack();
            d('catch',1);
            Session::flash('danger', $e);
            return redirect()->back();
        }
    }

}
