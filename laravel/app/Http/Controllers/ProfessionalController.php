<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator,
    Redirect;
use App\User;
use Auth;
use DB;
use Session;
use App\AmbassadorsSpecialities;
use App\Specialities;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class ProfessionalController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    use AuthenticatesAndRegistersUsers;

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $user_id = Auth::user()->id;
        $data['user'] = User::findOrFail($user_id);
        $data['speciality'] = DB::table('ambassadors_specialities')
                ->join('specialities', 'ambassadors_specialities.speciality_id', '=', 'specialities.id')
                ->select('specialities.title')
                ->where('ambassadors_specialities.ambassador_id', '=', $user_id)
                ->first();

        return view('front.professional.index', $data)->with('user_id', $user_id);
    }

    public function edit() {
        $data = array();
        $user_id = Auth::user()->id;
        $user = User::findOrFail($user_id);
        if (isset($user->dob)) {
            list($year, $month, $date) = explode('-', $user->dob);
            $user->date = $date;
            $user->month = $month;
            $user->year = $year;
        }
        $data['user'] = $user;
        $data['years'] = Functions::dob('year', '1950', date('Y'));
        $data['months'] = Functions::dob('month', '1', '12');
        $data['days'] = Functions::dob('day', '1', '31');

        $data['speciality_id'] = DB::table('ambassadors_specialities')
                ->join('specialities', 'ambassadors_specialities.speciality_id', '=', 'specialities.id')
                ->select('specialities.id')
                ->where('ambassadors_specialities.ambassador_id', '=', $user_id)
                ->first();
        //d($speciality_id,1);
        $data['speciality'] = Specialities::get();
//
//        $data['date'] = $date;
//        $data['month'] = $month;
//        $data['year'] = $year;

        return view('front.professional.edit', $data)->with('user_id', $user_id);
    }

    public function update(Request $request) {

        $user_id = Auth::user()->id;
        $rules = array(
            'goals' => 'max:50',
            'medicalConcerns' => 'max:50',
            'day' => 'required',
            'month' => 'required',
            'year' => 'required',
            'weight' => 'required',
            'height' => 'required',
        );

        if ($request->isCompetitor == 'on') {
            $rules['federation'] = 'max:50';
            $request->isCompetitor = 1;
        } else {
            $request->isCompetitor = 0;
            $request->federation = '';
        }

        $validator = Validator::make($request->all(), $rules);
        // $checkEmail=User::where('email','!=',$user->email)->where('email',$request->email)->count();

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        } else {
            $data = $request->all();
            $input['about'] = $request->about;
            $input['goals'] = $request->goals;
            $input['experience'] = $request->experience;
            $input['dob'] = $request->year.'-'.$request->month.'-'.$request->day;

            $input['medicalConcerns'] = $request->medicalConcerns;
            if ($request->height != '') {
                $input['height'] = $request->height;
            }
            $input['weight'] = $request->weight;
            $input['isCompetitor'] = $request->isCompetitor;
            $input['federation'] = $request->federation;
            array_forget($data, '_token');
            User::where('id', '=', $user_id)->update($input);
            AmbassadorsSpecialities::where('ambassador_id', '=', $user_id)->update([
                'speciality_id' => $request->specialities]);
            Session::flash('success', 'Successfully Updated.');
            return redirect()->back();
        }
    }

}
