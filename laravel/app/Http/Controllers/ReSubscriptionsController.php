<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;
use App\Role;
use App\Address;
use Session;
//use App\PaymentPayflow;
use App\Transactions;
use App\PayflowProfiles;
use App\Cards;
use App\Functions\Payflow;
use App\Functions\Functions;

class ReSubscriptionsController extends Controller {

    public $auth;

    public function __construct() {
        $this->middleware('auth');
    }

    public function register() {
        $data = array();

        if (Auth::user()->role->role == 'user') {
            return view('front.athlete.re-register', $data);
        }
        die('you are not a user');
    }

    public function postregister(Request $request) {

//        $currency = Config('params.currency_default');
        $rules = array(
            'cc' => 'required',
            'cvc' => 'required',
            'expMonth' => 'required',
            'expYear' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $data = array();
//        $amount = Config('params.athlete_fee');
        $user_id = Auth::user()->id;
//        $address = Address::where('user_id', '=', $user_id)->first();
//
//        $PayFlow = new PayFlow(env('PAYFLOW_VENDOR'), env('PAYFLOW_PARTNER'), env('PAYFLOW_USER'), env('PAYFLOW_PASSWORD'), 'single');
//
//        $PayFlow->setEnvironment(env('PAYFLOW_ENV'));                           // test or live
//        $PayFlow->setTransactionType('S');
//        $PayFlow->setPaymentMethod('C');
//        $PayFlow->setPaymentCurrency($currency);
        $expDate = sprintf("%02d", $request->expMonth) . '' . substr($request->expYear, 2, 3);
//        $PayFlow->setAmount($amount, FALSE);
//        $PayFlow->setCCNumber($request->cc);
//        $PayFlow->setCVV($request->cvc);
//        $PayFlow->setExpiration($expDate);
//        $PayFlow->setCreditCardName($request->name);
//        $PayFlow->setCustomerFirstName(Auth::user()->firstName);
//        $PayFlow->setCustomerLastName(Auth::user()->lastName);
//        $PayFlow->setCustomerAddress($address->address . ' - ' . $address->address2);
//        $PayFlow->setCustomerCity($address->city);
//        $PayFlow->setCustomerState($address->state);
//        $PayFlow->setCustomerZip($address->zip);
//        $PayFlow->setCustomerCountry('US');
//        $PayFlow->setCustomerPhone($address->phone);
//        $PayFlow->setCustomerEmail(Auth::user()->email);
//        $PayFlow->setPaymentComment('user ID # ' . Auth::user()->id);
//        $PayFlow->setPaymentComment2('Email ' . Auth::user()->email);

        $ccModel = new Cards();
        $ccModel->user_id = $user_id;
        $ccModel->expDate = $expDate;
        $ccModel->cc = $request->cc;
        $ccModel->cvc = $request->cvc;
        $ccModel->name = $request->name;
        $ccModel->save();

        if ($ccModel) {

//            $response = $PayFlow->getResponse();
//            $model = new Transactions();
//            $model->referenceId = $response['PNREF'];
//            $model->gateway = 'payflow';
//            $model->type_id = Auth::user()->id;
//            $model->type = 'athlete';
//            $model->amount = $amount;
//            $model->created_at = date('Y-m-d H:i:s');
//            $model->save();
//
//            $modelPaypal = new PaymentPayflow();
//            $modelPaypal->transaction_id = $model->id;
//            $modelPaypal->pnref = $response['PNREF'];
//            $modelPaypal->ppref = $response['PPREF'];
//            $modelPaypal->correlationId = $response['CORRELATIONID'];
//            $modelPaypal->created_at = date('Y-m-d H:i:s');
//            $modelPaypal->save();

            $this->subscribe($request);

            $roleModel = Role::where('role', 'athlete')->first();
            User::where('id', Auth::user()->id)->update(array('role_id' => $roleModel->id));
            $data['firstName'] = Auth::user()->firstName;
            $data['lastName'] = Auth::user()->lastName;
            $subject = view('emails.subscription.subject');
            $body = view('emails.subscription.body', $data);
            Functions::sendEmail(Auth::user()->email, $subject, $body);

            return redirect('athlete/success');
        } else {
            Session::flash('danger', 'Error....');
            return redirect()->back();
        }
    }

    public function subscribe($request) {

        $subscriptionRate = Config('params.subscription_fee_special');

        $currency = Config('params.currency_default');
        $user_id = Auth::user()->id;
        $address = Address::where('user_id', '=', $user_id)->first();

        $PayFlow = new PayFlow(env('PAYFLOW_VENDOR'), env('PAYFLOW_PARTNER'), env('PAYFLOW_USER'), env('PAYFLOW_PASSWORD'), 'recurring');

        $PayFlow->setEnvironment(env('PAYFLOW_ENV'));                           // test or live
        $PayFlow->setTransactionType('R');
        $PayFlow->setPaymentMethod('C');
        $PayFlow->setPaymentCurrency($currency);
        $expDate = sprintf("%02d", $request->expMonth) . '' . substr($request->expYear, 2, 3);
        $PayFlow->setAmount($subscriptionRate, FALSE);
        //$PayFlow->setCCNumber(5105105105105100);
        $PayFlow->setCCNumber($request->cc);
        $PayFlow->setCVV($request->cvc);
        $PayFlow->setExpiration($expDate);
        $PayFlow->setCreditCardName($request->name);
        $PayFlow->setProfileAction('A');
        $PayFlow->setProfileName(Auth::user()->firstName . '_' . Auth::user()->lastName);
        $PayFlow->setProfileStartDate(date('mdY', strtotime("+1 day")));
        $PayFlow->setProfilePayPeriod('MONT');
        $PayFlow->setFrequency(30);
        $PayFlow->setProfileTerm(0);

        $PayFlow->setCustomerFirstName(Auth::user()->firstName);
        $PayFlow->setCustomerLastName(Auth::user()->lastName);
        $PayFlow->setCustomerAddress($address->address . ' - ' . $address->address2);
        $PayFlow->setCustomerCity($address->city);
        $PayFlow->setCustomerState($address->state);
        $PayFlow->setCustomerZip($address->zip);
        $PayFlow->setCustomerCountry('US');
        $PayFlow->setCustomerPhone($address->phone);
        $PayFlow->setCustomerEmail(Auth::user()->email);
        $PayFlow->setPaymentComment('Subscriber User ID : ' . Auth::user()->id);
        $PayFlow->setPaymentComment2('Subscription');

        if ($PayFlow->processTransaction()) {
            $response = $PayFlow->getResponse();
            $model = new Transactions();
            $model->referenceId = $response['RPREF'];
            $model->gateway = 'payflow';
            $model->type_id = Auth::user()->id;
            $model->type = 'subscription';
            $model->amount = $subscriptionRate;
            $model->created_at = date('Y-m-d H:i:s');
            $model->save();

            $modelPaypal = new PayflowProfiles();
            $modelPaypal->transaction_id = $model->id;
            $modelPaypal->profileId = $response['PROFILEID'];
            $modelPaypal->rpref = $response['RPREF'];
            $modelPaypal->message = $response['RESPMSG'];
            $model->created_at = date('Y-m-d H:i:s');
            $modelPaypal->save();

            $response['error'] = 0;

            /*
              $subscribed = new AmbassadorsSubscriptions;
              $subscribed->ambassador_id = $request->ambassador_id;
              $subscribed->athlete_id = $request->athlete_id;
              $subscribed->subscriptionRate = $request->subscriptionRate;
              $subscribed->subscriptionComission = $request->subscriptionComission;
              $subscribed->subscriptionDate = date('Y-m-d H:i:s');
              $subscribed->save();
             * 
             */
        } else {
            $response = $PayFlow->getResponse();
            $response['error'] = 1;
        }

        return $response;
    }

    public function unsubscribe(Request $request) {
        
    }

    public function success() {

        $data = array();
        return view('front.athlete.success', $data);
    }

}
