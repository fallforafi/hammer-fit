<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator,
    Input;
use Auth;
use App\User;
use App\AmbassadorsSubscriptions;
use App\Results;
use File;
use Session;
use Illuminate\Http\Request;
use App\Functions\Functions;
use Intervention\Image\Facades\Image as Image;

class ResultsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user_id = Auth::user()->id;
        $data = array();
        $data['model'] = Results::where('user_id', $user_id)->orderBy('date', 'desc')->get();
        return view('front.results.index', $data)->with('user_id', $user_id);
    }

    public function save(Request $request) {


        $rules = array(
            'caption' => 'required|min:5|max:100',
            'currentWeight' => 'required',
            'date' => 'required',
            'frontImage' => 'required|mimes:jpeg,bmp,png,gif,jpg',
            'sideImage' => 'mimes:jpeg,bmp,png,gif,jpg',
            'backImage' => 'mimes:jpeg,bmp,png,gif,jpg',
        );
        $validator = Validator::make($request->all(), $rules);

        $input = $request->all();
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $input['user_id'] = Auth::user()->id;
        if (Input::hasFile('frontImage')) {
            $destinationPath = public_path() . '/uploads/users/results/front_images/';
            $input['frontImage'] = self::postImage(Input::file('frontImage'), $destinationPath);
        }

        if (Input::hasFile('sideImage')) {
            $destinationPath = public_path() . '/uploads/users/results/side_images/';
            $input['sideImage'] = self::postImage(Input::file('sideImage'), $destinationPath);
        }

        if (Input::hasFile('backImage')) {
            $destinationPath = public_path() . '/uploads/users/results/back_images/';
            $input['backImage'] = self::postImage(Input::file('backImage'), $destinationPath);
        }
        $input['date'] = date('Y-m-d', strtotime($request->date));

        unset($input['_token']);

        $id = Results::insertGetId($input);

        $getUser = AmbassadorsSubscriptions::where(['athlete_id' => $input['user_id'], 'subscriptionStatus' => '1'])->first();

        $getAthlete = User::where('id', $getUser->athlete_id)->where('status', 1)->where('deleted', 0)->first();
        $getAmbassador = User::where('id', $getUser->ambassador_id)->where('status', 1)->where('deleted', 0)->first();

        $data['athleteName'] = $getAthlete->firstName . ' ' . $getAthlete->lastName;
        $data['ambassadorName'] = $getAmbassador->firstName . ' ' . $getAmbassador->lastName;

        //Email sent to Coach that tells the Coach about update progress by Athlete
        $subjectAmbassador = view('emails.updateProgress_mailto_coach.subject');
        $bodyAmbassador = view('emails.updateProgress_mailto_coach.body', compact('data'));
        Functions::sendEmail($getAmbassador->email, $subjectAmbassador, $bodyAmbassador);

        Session::flash('success', 'Successfully Added!');
        return redirect()->back();
    }

    public function savedate(Request $request) {

        $rules = array(
            'date' => 'required',
        );
        $input['date'] = $request->value;
        $input['id'] = $request->id;
        $validator = Validator::make($input, $rules);
        $response['error'] = 0;
        if ($validator->fails()) {

            $response['error'] = 1;
            $response['messages'] = $validator->messages()->toJson();
        } else {
            $input['date'] = date('Y-m-d', strtotime($input['date']));
            $id = $input['id'];
            unset($input['id']);
            $response['messages'] = Results::where('id', '=', $id)->update($input);
        }
        echo json_encode($response);
    }

    public function savecaption(Request $request) {

        $rules = array(
            'caption' => 'required|min:5|max:100',
        );
        $input['caption'] = $request->value;
        $input['id'] = $request->id;
        $validator = Validator::make($input, $rules);
        $response['error'] = 0;
        if ($validator->fails()) {
            $response['error'] = 1;
            $response['messages'] = $validator->messages()->toJson();
        } else {
            $id = $input['id'];
            unset($input['id']);
            $response['messages'] = Results::where('id', '=', $id)->update($input);
        }
        echo json_encode($response);
    }

    public function saveweight(Request $request) {

        $rules = array(
            'currentWeight' => 'required',
        );
        $input['currentWeight'] = $request->value;
        $input['id'] = $request->id;
        $validator = Validator::make($input, $rules);
        $response['error'] = 0;
        if ($validator->fails()) {
            $response['error'] = 1;
            $response['messages'] = $validator->messages()->toJson();
        } else {
            $id = $input['id'];
            unset($input['id']);
            $response['messages'] = Results::where('id', '=', $id)->update($input);
        }
        echo json_encode($response);
    }

    public function savebodyfat(Request $request) {

        $rules = array(
            'currentBodyFat' => 'required',
        );
        $input['currentBodyFat'] = $request->value;
        $input['id'] = $request->id;
        $validator = Validator::make($input, $rules);
        $response['error'] = 0;
        if ($validator->fails()) {
            $response['error'] = 1;
            $response['messages'] = $validator->messages()->toJson();
        } else {
            $id = $input['id'];
            unset($input['id']);
            $response['messages'] = Results::where('id', '=', $id)->update($input);
        }
        echo json_encode($response);
    }

    public function saveimage(Request $request) {
        $rules = array(
            'image' => 'required|mimes:jpeg,bmp,png,gif,jpg',
        );
        $validator = Validator::make($request->all(), $rules);

        $input = $request->all();
        list($l, $r) = explode("_", $input['side']);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $destinationPath = public_path() . '/uploads/users/results/' . $input['side'] . 's/';

        $input[$l . ucfirst($r)] = self::postImage(Input::file('image'), $destinationPath);
        $id = $input['id'];
        $cImage = $input['cImage'];

        if ($input['cImage']) {
            unlink($destinationPath . $input['cImage']);
            unlink($destinationPath . 'thumbnail/' . $input['cImage']);
            unlink($destinationPath . 'original/' . $input['cImage']);
        }

        unset($input['id']);
        unset($input['side']);
        unset($input['image']);
        unset($input['cImage']);
        unset($input['_token']);
        $response['messages'] = Results::where('id', '=', $id)->update($input);
        Session::flash('success', 'Successfully Changed.');
        return redirect()->back();
    }

    public function deleteimage(Request $request) {
        $input = $request->all();
        $id = $input['id'];
        unset($input['id']);
        unset($input['_token']);
        $response['messages'] = Results::where('id', '=', $id)->update(array($input['side'] => ""));
        echo json_encode($response);
    }

    public static function postImage($file, $destinationPath) {

        $destinationPathThumb = $destinationPath . 'thumbnail/';

        $destinationPathOrig = $destinationPath . 'original/';

        $fileName = Functions::saveImage($file, $destinationPath, $destinationPathThumb);
        $thumbnail = Image::make($destinationPath . $fileName)->resize(500, 400)->save($destinationPathThumb . $fileName)->orientate();
        $original = Image::make($destinationPath . $fileName)->save($destinationPathOrig . $fileName)->orientate();
        return $fileName;
    }

    public function deleteResult($id, Request $request) {
        $results = Results::where('id', '=', $id)->first();

        if (isset($results->frontImage) && $results->frontImage != "") {
            $destinationPath = public_path() . '/uploads/users/results/front_images/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $destinationPathOrig = $destinationPath . 'original/';

            $file1 = $destinationPath . $results->frontImage;
            $file2 = $destinationPathThumb . $results->frontImage;
            $file3 = $destinationPathOrig . $results->frontImage;
            File::delete($file1, $file2, $file3);
        }
        if (isset($results->sideImage) && $results->sideImage != "") {
            $destinationPath = public_path() . '/uploads/users/results/side_images/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $destinationPathOrig = $destinationPath . 'original/';

            $file1 = $destinationPath . $results->sideImage;
            $file2 = $destinationPathThumb . $results->sideImage;
            $file3 = $destinationPathOrig . $results->sideImage;
            File::delete($file1, $file2, $file3);
        }
        if (isset($results->backImage) && $results->backImage != "") {
            $destinationPath = public_path() . '/uploads/users/results/back_images/';
            $destinationPathThumb = $destinationPath . 'thumbnail/';
            $destinationPathOrig = $destinationPath . 'original/';

            $file1 = $destinationPath . $results->backImage;
            $file2 = $destinationPathThumb . $results->backImage;
            $file3 = $destinationPathOrig . $results->backImage;
            File::delete($file1, $file2, $file3);
        }

        Results::where('id', '=', $id)->delete();
        $request->session()->flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
