<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,
    Redirect;
use App\Shows;
use Auth;
use Session;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class ShowsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    use AuthenticatesAndRegistersUsers;

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $user_id = Auth::user()->id;
        $data['shows'] = Shows::where('user_id', '=', $user_id)->get();

        return view('front.shows.index', $data)->with('user_id', $user_id);
    }

    public function create() {
        return view('front.shows.create');
    }

    public function store(Request $request) {
        $inputs = $request->all();
        $inputs['date'] = date("Y-m-d", strtotime($request->date));
        $rules['title'] = 'required';
        $rules['description'] = 'required|max:200';
        $rules['date'] = 'required|date|date_format:Y-m-d|after:yesterday|unique_custom:shows,date,user_id,' . Auth::id();
        $messages = [
            'date.unique_custom' => 'You have already taken this Date',
        ];
        $validator = Validator::make($inputs, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        }
        $user_id = Auth::user()->id;

        $show = new Shows();
        $show->title = $request->title;
        $show->description = $request->description;
        $show->date = $inputs['date'];
        $show->user_id = $user_id;
        $show->save();

        Session::flash('success', 'Successfully Added!');
        return redirect('shows/create');
    }

    public function edit($id) {
        $user_id = Auth::user()->id;
        $data['shows'] = Shows::where('id', '=', $id)->first();
        return view('front.shows.edit', $data)->with('user_id', $user_id);
    }

    public function update(Request $request) {
        $inputs = $request->all();
        $inputs['date'] = date("Y-m-d", strtotime($request->date));
        $rules['title'] = 'required';
        $rules['description'] = 'required|max:200';
        $rules['date'] = 'required|date|date_format:Y-m-d|after:yesterday|unique_custom:shows,date,user_id,' . Auth::id();
        $messages = [
            'date.unique_custom' => 'You have already taken this Date',
        ];
        $validator = Validator::make($inputs, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        } else {
            $data = $request->all();
            array_forget($data, '_token');
            $show['title'] = $request->title;
            $show['description'] = $request->description;
            $show['date'] = $inputs['date'];
            $affectedRows = Shows::where('id', '=', $request->show_id)->update($show);
            Session::flash('success', 'Successfully Updated.');
            return redirect()->back();
        }
    }

    public function delete($id, Request $request) {
        $row = Shows::where('id', '=', $id)->delete();
        $request->session()->flash('alert-success', 'Successfully Deleted!');
        return redirect()->back();
    }

}
