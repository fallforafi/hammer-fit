<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use App\MealPlans;
//use App\AthleteMp;
use App\WorkoutPlans;
use App\User;
use App\AmbassadorsSubscriptions;
use App\Functions\Functions;
use Session;
use Auth;

//use App\Functions\Payflow;

class SubscriptionsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $user_id = Auth::user()->id;
        if (Auth::user()->role_id == 2) {
            $data['model'] = DB::table('ambassadors_subscriptions')
                    ->join('users', 'users.id', '=', 'ambassadors_subscriptions.ambassador_id')
                    //->join('transactions as tx','tx.type_id','=','ambassadors_subscriptions.athlete_id')
                    ->select('ambassadors_subscriptions.*', 'users.*')
                    //->select('ambassadors_subscriptions.*', 'users.*','tx.amount')
                    ->where('athlete_id', '=', $user_id)
                    // ->where('users.status', 1)
                    ->where('subscriptionStatus', 2)
                    ->get();
            $data['user'] = DB::table('ambassadors_subscriptions')
                    ->join('users', 'users.id', '=', 'ambassadors_subscriptions.ambassador_id')
                    //->join('transactions as tx', 'tx.type_id', '=', 'ambassadors_subscriptions.athlete_id')
                    ->select('ambassadors_subscriptions.*', 'users.*')
                    //->select('ambassadors_subscriptions.*', 'users.*', 'tx.amount')
                    ->where('athlete_id', '=', $user_id)
                    // ->where('users.status', 1)
                    ->where('subscriptionStatus', 1)
                    ->first();

            return view('front.subscriptions.index', $data)->with('user_id', $user_id);
        } else {
            $data['model'] = DB::table('ambassadors_subscriptions')
                    ->join('users', 'users.id', '=', 'ambassadors_subscriptions.athlete_id')
                    //->join('transactions as tx', 'tx.type_id', '=', 'users.id')
                    ->select('ambassadors_subscriptions.*', 'users.*')
                    //->select('ambassadors_subscriptions.*', 'users.*', 'tx.amount')
                    ->where('ambassador_id', '=', $user_id)
                    ->where('users.status', 1)
                    ->where('subscriptionStatus', 2)
                    ->get();
            $data['users'] = DB::table('ambassadors_subscriptions')
                    ->join('users', 'users.id', '=', 'ambassadors_subscriptions.athlete_id')
                    //->join('transactions as tx', 'tx.type_id', '=', 'users.id')
                    ->select('ambassadors_subscriptions.*', 'users.*')
                    // ->select('ambassadors_subscriptions.*', 'users.*', 'tx.amount')
                    ->where('ambassador_id', '=', $user_id)
                    ->where('users.status', 1)
                    ->where('subscriptionStatus', 1)
                    ->get();
            $data['mealPlans'] = MealPlans::where(['user_id' => $user_id, 'deleted' => 0,])->get();
            foreach ($data['users'] as $user) {
                $data['athleteMp'][$user->id] = MealPlans::join('athlete_mp', 'athlete_mp.mp_id', '=', 'meal_plans.id')
                        ->select('meal_plans.title as mp_name', 'athlete_mp.athlete_id as a_mid', 'athlete_mp.created_at')
                        ->where([
                            'meal_plans.deleted' => 0,
                            'athlete_mp.athlete_id' => $user->id,
                        ])
                        ->orderBy('athlete_mp.id', 'desc')
                        ->first();
                $data['athleteWp'][$user->id] = WorkoutPlans::join('athlete_wp', 'athlete_wp.wp_id', '=', 'workout_plans.id')
                        ->select('workout_plans.title as wp_name', 'athlete_wp.athlete_id as a_wid', 'athlete_wp.created_at')
                        ->where([
                            'athlete_wp.athlete_id' => $user->id,
                            'workout_plans.deleted' => 0,
                        ])
                        ->orderBy('athlete_wp.id', 'desc')
                        ->first();
            }
            $data['workoutPlans'] = WorkoutPlans::where([
                        'user_id' => $user_id,
                        'deleted' => 0,
                    ])->get();

            return view('front.subscriptions.index', $data)->with('user_id', $user_id);
        }
    }

    public function updatesNeeded($id) {

        $athlete_id = $id;

        $getUser = AmbassadorsSubscriptions::where(['athlete_id' => $athlete_id, 'subscriptionStatus' => '1'])->first();

        $getAthlete = User::where('id', $getUser->athlete_id)->where('status', 1)->where('deleted', 0)->first();
        $getAmbassador = User::where('id', $getUser->ambassador_id)->where('status', 1)->where('deleted', 0)->first();

        $data['athleteName'] = $getAthlete->firstName . ' ' . $getAthlete->lastName;
        $data['ambassadorName'] = $getAmbassador->firstName . ' ' . $getAmbassador->lastName;

        //Email sent to Athlete that tells the Athlete about update needed
        $subjectAmbassador = view('emails.updatesNeeded_mailto_athlete.subject');
        $bodyAmbassador = view('emails.updatesNeeded_mailto_athlete.body', compact('data'));
        Functions::sendEmail($getAthlete->email, $subjectAmbassador, $bodyAmbassador);

        Session::flash('success', 'Successfully Sent!');
        return redirect()->back();
    }

}
