<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator,
    Redirect;
use Auth;
use App\WorkoutPlans;
use App\AthleteWp;
use Session;
use App\User;
//use DB;
use App\Functions\Functions;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class WorkoutPlansController extends Controller {

    use AuthenticatesAndRegistersUsers;

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user_id = Auth::user()->id;
        $data['workoutPlans'] = WorkoutPlans::where([
                    'user_id' => $user_id,
                    'deleted' => 0,
                ])->get();
//        $model = DB::table('ambassadors_subscriptions as amb')
//                ->select('amb.athlete_id')
//                ->where('subscriptionStatus', 1)
//                ->get();
//        d($model, 1);
//        // $dateTime = date('Y-m-d H:i:s', strtotime('-28 days'));
//        $workoutPlans = DB::table('ambassadors_subscriptions as amb')
//                ->get();
////        $workoutPlans = WorkoutPlans::join('athlete_wp as awp', 'wp_id', '=', 'workout_plans.id')
////                ->select('workout_plans.*', 'awp.created_at', 'awp.athlete_id', 'awp.wp_id')
////                ->where('workout_plans.deleted', 0)
////                // ->where('amp.created_at', '<=', $dateTime)
////                ->get();
//        d($model, 1);
////        $getAdmin = User::where('role_id', 1)->get();
////        $emails = array();
////        foreach ($getAdmin as $value) {
////            $emails[] = $value->email;
////        }
////        foreach ($workoutPlans as $row) {
////
////            $getAthlete = User::where('id', $row->athlete_id)->where('status', 1)->where('deleted', 0)->first();
////            $getAmbassador = User::where('id', Auth::user()->id)->where('status', 1)->where('deleted', 0)->first();
////            $data['athleteName'] = $getAthlete->firstName . ' ' . $getAthlete->lastName;
////            $data['ambassadorName'] = $getAmbassador->firstName . ' ' . $getAmbassador->lastName;
////
////            $subject = view('emails.crons.workout_plan_reminder_to_coach.subject');
////            $body = view('emails.crons.workout_plan_reminder_to_coach.body', compact('data'));
////            Functions::sendEmail($getAmbassador->email, $subject, $body, '', '', $emails);
////        }
        return view('front.workout-plans.index', $data);
    }

    public function create() {
        return view('front.workout-plans.create');
    }

    public function store(Request $request) {
        $rules['file'] = 'required|mimes:pdf';
        $rules['title'] = 'required|max:100';
        $rules['description'] = 'required|max:200';
        $validator = Validator::make($request->all(), $rules);
        $fileName = "";
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        }
        $user_id = Auth::user()->id;
        if (isset($request->file)) {
            $destinationPath = public_path('uploads/users/workout-plans/');
            $file = $request->file;
            $fileName = Functions::saveImage($file, $destinationPath);
        }
        $workoutPlan = new WorkoutPlans();
        $workoutPlan->title = $request->title;
        $workoutPlan->description = $request->description;
        $workoutPlan->user_id = $user_id;
        $workoutPlan->file = $fileName;
        $workoutPlan->save();
        Session::flash('success', 'Successfully Added!');
        return redirect('workout-plans');
    }

    public function edit($id) {
        $user_id = Auth::user()->id;
        $data['workoutPlans'] = WorkoutPlans::where('id', '=', $id)->first();
        return view('front.workout-plans.edit', $data)->with('user_id', $user_id);
    }

    public function update(Request $request) {
        $rules['file1'] = 'mimes:pdf';
        $rules['title'] = 'required';
        $rules['description'] = 'required|max:200';
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        } else {
            if (isset($request->file1)) {
                $destinationPath = public_path('uploads/users/workout-plans/');
                $file = $request->file1;
                $fileName = Functions::saveImage($file, $destinationPath);
            } else {
                $fileName = $request->file;
            }
            $data = $request->all();
            array_forget($data, '_token');
            $model['title'] = $request->title;
            $model['description'] = $request->description;
            $model['file'] = $fileName;
            WorkoutPlans::where('id', '=', $request->id)->update($model);
            Session::flash('success', 'Successfully Updated.');
            return redirect()->back();
        }
    }

    public function delete($id) {
        WorkoutPlans::where('id', '=', $id)->update([
            'deleted' => 1,
        ]);
        Session::flash('success', 'Successfully Deleted!');
        return redirect()->back();
    }

    public function addPlan(Request $request) {

        $rules['file'] = 'required|mimes:pdf';
        $rules['title'] = 'required|max:100';
        $rules['description'] = 'required|max:200';
        $validator = Validator::make($request->all(), $rules);
        $fileName = "";
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        }
        $user_id = Auth::user()->id;
        if (isset($request->file)) {
            $destinationPath = public_path('uploads/users/workout-plans/');
            $file = $request->file;
            $fileName = Functions::saveImage($file, $destinationPath);
        }
        $workoutPlan = new WorkoutPlans();
        $workoutPlan->title = $request->title;
        $workoutPlan->description = $request->description;
        $workoutPlan->user_id = $user_id;
        $workoutPlan->file = $fileName;
        $workoutPlan->save();

        $athleteWp = new AthleteWp();
        $athleteWp->wp_id = $workoutPlan->id;
        $athleteWp->athlete_id = $request->athlete_id;
        $athleteWp->save();

        $getAthlete = User::where('id', $athleteWp->athlete_id)->where('status', 1)->where('deleted', 0)->first();
        $getAmbassador = User::where('id', $user_id)->where('status', 1)->where('deleted', 0)->first();

        $data['athleteName'] = $getAthlete->firstName . ' ' . $getAthlete->lastName;
        $data['ambassadorName'] = $getAmbassador->firstName . ' ' . $getAmbassador->lastName;

        //Email sent to Athlete that tells the Athlete workout plan added by Coach
        $subjectAthlete = view('emails.add_workoutPlan_mailto_athlete.subject');
        $bodyAthlete = view('emails.add_workoutPlan_mailto_athlete.body', compact('data'));
        Functions::sendEmail($getAthlete->email, $subjectAthlete, $bodyAthlete);

        Session::flash('success', 'Successfully Added!');
        return redirect()->back();
    }

}
