<?php

Route::post('crop', 'CropController@postCrop');
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::get('how-to-order', 'HomeController@howtoorder');
Route::get('about-us', 'HomeController@aboutus');
Route::get('terms', 'HomeController@terms');
Route::get('agreement', 'HomeController@agreement');
Route::get('privacy', 'HomeController@privacy');
Route::get('contact-us', 'ContactusController@index');
Route::get('get-started', 'HomeController@getstarted');

Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController',]);
Route::post('contact-send', 'ContactusController@store');
Route::get('search', 'HomeController@search');
Route::get('products/{id}', 'HomeController@products');
Route::get('shop', 'HomeController@products');
Route::get('product/{id}', 'HomeController@getproduct');
Route::get('apparel/{id}', 'HomeController@getproduct');
Route::get('page/{code}', 'PageController@view');

Route::get('products/{id}', 'HomeController@products');
Route::get('products', 'HomeController@products');
Route::get('apparels', 'HomeController@apparel');
Route::get('/guestbook', [
    'uses' => 'HomeController@guestbook',
    'as' => 'guestbook.messages',
]);

Route::get('forgot', 'SignupController@forgot_password');
Route::post('reset', 'SignupController@reset_password');
Route::post('savemessage', 'HomeController@messagePost');
Route::get('page/{code}', 'PageController@view');
Route::get('myorders', 'OrdersController@myorders');
Route::get('order/{id}', 'OrdersController@order');
Route::get('cart/add', 'CartController@add');
Route::get('cart/addsimple', 'CartController@addsimple');
Route::get('cart/updateproductprice', 'CartController@updateproductprice');
Route::get('cart/view', 'CartController@mycart');
Route::get('cart/', 'CartController@mycart');
Route::get('cart/delete/{id}', 'CartController@delete');
Route::get('cart/update', 'CartController@update');
Route::get('checkout', 'CheckoutController@index');
Route::post('postOrder', 'CheckoutController@order');
Route::get('checkout/success/{id}', 'CheckoutController@success');
Route::get('checkout/fail', 'CheckoutController@fail');
Route::get('register/success/{id}', 'SignupController@success');
Route::get('mycart', 'CartController@index');
Route::post('admin/clientOrderStatus', 'ClientsController@ordersBystatus');
Route::post('changeprofileimage', 'CustomersController@changeprofileimage');

Route::get('profile-picture', 'CustomersController@profilePicture');
Route::get('user/available/{id}', 'CustomersController@available');
Route::get('user/unavailable/{id}', 'CustomersController@unavailable');

//Route for Newsletter
Route::post('newsletter/store', 'NewsletterController@store');

Route::get('login', 'SignupController@login');
Route::get('signup', 'SignupController@signup');
Route::get('signup/athlete', 'SignupController@register');
Route::get('signup/ambassador', 'SignupController@register');
Route::get('signup/user', 'SignupController@register');
Route::post('/signUpPost', 'SignupController@store');
Route::post('postLogin', 'SignupController@postLogin');
Route::get('changepassword', 'CustomersController@changepassword');
Route::post('postchangepassword', 'CustomersController@postchangepassword');
Route::get('register/verify/{confirmation_code}', 'SignupController@confirmEmail');

Route::get('certifications/create', 'CertificationsController@create');
Route::post('certifications/store', 'CertificationsController@store');
Route::get('certifications/edit/{id}', 'CertificationsController@edit');
Route::post('certifications/update', 'CertificationsController@update');
Route::get('certifications/delete/{id}', 'CertificationsController@delete');

Route::get('shows/create', 'ShowsController@create');
Route::post('shows/store', 'ShowsController@store');
Route::get('shows/edit/{id}', 'ShowsController@edit');
Route::post('shows/update', 'ShowsController@update');
Route::get('shows/delete/{id}', 'ShowsController@delete');

Route::get('profile/edit', 'CustomersController@editprofile');
Route::get('professional/edit', 'ProfessionalController@edit');
Route::get('address/edit', 'AddressController@edit');

Route::get('profile', 'CustomersController@profile');
Route::get('professional', 'ProfessionalController@index');
Route::get('address', 'AddressController@index');
Route::get('shows', 'ShowsController@index');
Route::get('certifications', 'CertificationsController@index');
Route::get('meal-plans', 'MealPlansController@index');
Route::get('workout-plans', 'WorkoutPlansController@index');
Route::get('training-details', 'TrainingController@index');
Route::get('calculators', 'CalculatorController@index');
Route::get('calculators/fitness-detail', 'CalculatorController@detail');

// Subscriptions routes
Route::get('subscriptions', 'SubscriptionsController@index');
Route::get('updates-needed/{id}', 'SubscriptionsController@updatesNeeded');

Route::post('updateprofile', 'CustomersController@updateprofile');
Route::post('updateprofessional', 'ProfessionalController@update');
Route::post('updateaddress', 'AddressController@update');

//MealPlans Route
Route::get('meal-plans/create', 'MealPlansController@create');
Route::post('meal-plans/store', 'MealPlansController@store');
Route::get('meal-plans/edit/{id}', 'MealPlansController@edit');
Route::post('meal-plans/update', 'MealPlansController@update');
Route::get('meal-plans/delete/{id}', 'MealPlansController@delete');
//WorkoutPlans Route
Route::get('workout-plans/create', 'WorkoutPlansController@create');
Route::post('workout-plans/store', 'WorkoutPlansController@store');
Route::get('workout-plans/edit/{id}', 'WorkoutPlansController@edit');
Route::post('workout-plans/update', 'WorkoutPlansController@update');
Route::get('workout-plans/delete/{id}', 'WorkoutPlansController@delete');

Route::post('add-mplan', 'MealPlansController@addPlan');
Route::post('add-wplan', 'WorkoutPlansController@addPlan');

//Search Route
Route::get('search', 'SearchController@search');
Route::get('/user/{id}', 'ProfileController@index');
//Route for Ambassadors Subscriptions
Route::post('subscribe', 'ProfileController@store');
Route::get('unsubscribe/{id}', 'ProfileController@update');
Route::post('collage', 'ProfileController@makeCollage');
Route::get('meal-plan/delete/{id}', 'ProfileController@deleteMealPlan');
Route::get('workout-plan/delete/{id}', 'ProfileController@deleteWorkoutPlan');

//Route for Messages
Route::post('messages', 'ProfileController@saveReview');

Route::get('results', 'ResultsController@index');
Route::post('results/save', 'ResultsController@save');

//Route for Results
Route::get('results/savedate', 'ResultsController@savedate');
Route::get('results/savecaption', 'ResultsController@savecaption');
Route::get('results/saveweight', 'ResultsController@saveweight');
Route::get('results/savebodyfat', 'ResultsController@savebodyfat');
Route::post('results/saveimage', 'ResultsController@saveimage');
Route::post('results/deleteimage', 'ResultsController@deleteimage');
Route::get('result/delete/{id}', 'ResultsController@deleteResult');


Route::post('coupons/apply', 'CouponsController@apply');
Route::group(
        array('prefix' => 'paypal'), function() {
    $folder = "Payments\\";
    Route::get('success', $folder . 'PaypalController@success');
    Route::get('cancel', $folder . 'PaypalController@cancel');
}
);

Route::group(
        array('prefix' => 'check'), function() {
    $folder = "Payments\\";
    Route::get('success', $folder . 'CheckController@success');
    Route::get('cancel', $folder . 'CheckController@cancel');
}
);

Route::get('blog', 'BlogController@index');
Route::get('blog/{q}', 'BlogController@index');
Route::get('blog/post/{id}', 'BlogController@post');

Route::group(['middleware' => 'prevent-back-history'], function() {
    Route::get('athlete/register', 'AthleteController@register');
});

Route::post('athlete/postregister', 'AthleteController@postregister');
Route::get('athlete/success', 'AthleteController@success');
Route::get('athlete/cancel', 'AthleteController@cancel');

//Route::get('athlete/re-register', 'ReSubscriptionsController@register');
//Route::post('athlete/re-postregister', 'ReSubscriptionsController@postregister');
//Route::get('athlete/re-success', 'ReSubscriptionsController@success');
//Route::get('athlete/re-cancel', 'ReSubscriptionsController@cancel');

Route::get('athlete/subscribe', 'AthleteController@subscribe');
Route::get('athlete/subscribe/success', 'AthleteController@subscribeSuccess');
Route::get('athlete/subscribe/cancel', 'AthleteController@subscribeCancel');

Route::get('membership', 'MembershipController@index');
Route::get('membership/cancel', 'MembershipController@cancel');
Route::post('membership/rejoin', 'MembershipController@rejoin');
Route::post('membership/update', 'MembershipController@update');

//Route::get('update/credit-card', 'CreditCardController@index');
Route::post('save/credit-card', 'CreditCardController@save');

Route::get('test', 'TestController@index');

Route::get('state/get/{id}','AjaxController@getState');

Route::get('city/get/{id}','AjaxController@getCity');

include('routes_admin.php');
