<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembershipCancel extends Model {

    protected $table = 'membership_cancel';
    
    protected $fillable = ['user_id'];

}
