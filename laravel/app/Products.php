<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model {

    static function getProducts($query) {
        return $products = Products::where('pc.category_id', '=', $query['category_id'])
                ->leftJoin('products_categories as pc', 'pc.product_id', '=', 'products.id')
                ->select('products.*', 'pc.id as pppid')
                ->get();
    }

}
