<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestedCoaches extends Model {

    protected $table = 'requested_coaches';

}
