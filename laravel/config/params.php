<?php

return [
    'site_name' => 'Hammer Fit',
    'sale_fee' => 10.00,
    'subscription_fee' => 100.00,
    'athlete_fee_special' => 0.5,
    'subscription_fee_special' => 60,
    'currency' => [
        'USD' => ['symbol' => '$', 'name' => 'US Dollors'],
    ],
    'currency_default' => 'USD',
    'languages' => [
        'en_uk' => 'English (UK)',
        'en_us' => 'English (Us)',
    ],
    'language_default' => 'en_uk',
    'contentTypes' => [
        'page' => 'Page',
        'email' => 'Email',
        'block' => 'Block',
    ],
    'per_month' => '/month',
    'order_prefix' => "hammerfit",
    'order_email' => 'orders@hammerfit.net',
    'type' => [
        'product' => 'Product',
        'blog' => 'Blog',
        'category' => 'Category',
    ]
];