@extends('admin/admin_template')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
        @endforeach
    </div> <!-- end .flash-message -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Set Commissions</h3>
        </div>
        <!-- form start -->

        @if(isset($ambassadorCheck))
        <form action="<?php echo url('admin/ambassador/update/' . $ambassadorCheck->id); ?>" enctype="multipart/form-data" method="post">
            {{ method_field('PATCH') }}
            @else
            <form role="form" action="{{ url('admin/ambassador/store') }}" enctype="multipart/form-data" method="post">
                @endif 

                <div class="box-body">

                    <div class="form-group col-sm-12">
                        <label>Subscription Rate</label>

                        @if(isset($ambassadorCheck))
                        <input type="number" class="form-control" placeholder="Subscription Rate" min="1" step="any" name="subscriptionRate" value="{{ $ambassadorCheck->subscriptionRate}}" required="required">

                        @else
                        <input type="number" class="form-control" placeholder="Subscription Rate" min="1" step="any" name="subscriptionRate" value="{{ old('subscriptionRate') }}" required="required">

                        @endif

                        @if ($errors->has('subscriptionRate'))
                        <span class="help-block">
                            <strong>{{ $errors->first('subscriptionRate') }}</strong>
                        </span>
                        @endif

                    </div>

                    <div class="form-group col-sm-12">
                        <label>Subscription Comission(%)</label>
                        @if(isset($ambassadorCheck))


                        <input type="number" class="form-control" placeholder="Subscription Comission in %" min="1" step="any" name="subscriptionComission" value="{{ $ambassadorCheck->subscriptionComission}}" required="required">


                        @else
                        <input type="number" class="form-control" placeholder="Subscription Comission in %" min="1" step="any" name="subscriptionComission" value="{{ old('subscriptionComission') }}" required="required">

                        @endif

                        @if ($errors->has('subscriptionComission'))
                        <span class="help-block">
                            <strong>{{ $errors->first('subscriptionComission') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-12">
                        <label>Product Comission(%)</label>

                        @if(isset($ambassadorCheck))
                        <input type="number" class="form-control" placeholder="Product Comission in %" min="1" step="any"  name="productComission" value="{{ $ambassadorCheck->productComission}}" required="required">

                        @else
                        <input type="number" class="form-control" placeholder="Product Comission in %" min="1" step="any" name="productComission" value="{{ old('subscriptionComission') }}">

                        @endif

                        @if ($errors->has('productComission'))
                        <span class="help-block">
                            <strong>{{ $errors->first('productComission') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group col-sm-12">
                        @if(isset($ambassadorCheck))

                        @else	
                        <input type="hidden" name="user_id" value="{{ $id }}">
                        @endif
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        @if(isset($ambassadorCheck))
                        <button type="submit" class="btn btn-success">Update</button>
                        <a href="{{ URL::previous() }}" class="btn btn-primary pull-right">Back</a> 
                        @else
                        <button type="submit" class="btn btn-primary">Submit</button> 
                        <a href="{{ URL::previous() }}" class="btn btn-primary pull-right">Back</a>
                        @endif

                    </div>

                </div>
            </form>           

    </div>

</section>
@endsection