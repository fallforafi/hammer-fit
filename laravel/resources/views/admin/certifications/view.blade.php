@extends('admin/admin_template')

@section('content')

<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <div class="col-md-12" >
      <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
       @endif
       @endforeach
       </div> <!-- end .flash-message -->
        <!-- PRODUCT LIST -->
        <div class="box-body table-responsive no-padding">

            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-header with-border">
                        <h3 class="box-title">Award/Certification Information</h3>

<!--                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-info btn-flat" href="{{url('admin/specialities/create')}}">Add new Speciality</a>

                        </div>-->
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" name="title" value="{{$certifications->title}}">
                </div>
                    
                <div class="form-group">
                    <label>Description</label>
                    <textarea name="description" class="form-control">{{$certifications->description}}</textarea>
                </div>
                <div class="form-group">
                    <label>File</label><br>
                    <a href="{{ url('uploads/users/'.$certifications->file) }}" download="{{$certifications->file}}" class="btn btn-info"><i class="fa fa-download" aria-hidden="true"></i> Download</a></td>
                </div>
                <div class="form-group">
                    <a href="{{ url('admin/certifications')}}" class="btn btn-primary btn-flat">Back</a>
                </div>
                    
                </div>
                
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                </div>
                <!-- /.box-footer -->
            </div>

        </div>
    </div>
</div>

@endsection

