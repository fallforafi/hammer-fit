<?php
$required = "required";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('Name *') !!}
    @if(isset($charges))
    <input type="text" class="form-control" placeholder="Name"  name="name" value="{{ $charges->name }}" required="required">

    @else
    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" required="required">

    @endif

</div>
<div class="form-group">
    {!! Form::label('Type *') !!}
    <select class="form-control"  id="type" name="type" required="required">
        <option value="">Select Type *</option>
        <option value="joining" @if(isset($charges) && $charges->type == 'joining') selected='selected' @endif>Joining</option>
        <option value="meal_plan" @if(isset($charges) && $charges->type == 'meal_plan') selected='selected' @endif>Meal Plan</option>
        <option value="workout_plan" @if(isset($charges) && $charges->type == 'workout_plan') selected='selected' @endif>Workout Plan</option>
        <option value="full_service" @if(isset($charges) && $charges->type == 'full_service') selected='selected' @endif>Full Service</option>
    </select>
</div>


<div class="form-group">
    {!! Form::label('Rate *') !!}
    @if(isset($charges))
    <input type="number" class="form-control" placeholder="Rate" min="1" step="any" name="rate" value="{{ $charges->rate }}" required="required">

    @else
    <input type="number" class="form-control" placeholder="Rate" min="1" step="any" name="rate" value="{{ old('rate') }}" required="required">

    @endif

</div>


<div class="form-group">

    <button type="submit" class="btn btn-primary btn-flat ">Save</button>


    <a href="{{ url('admin/charges')}}" class="btn btn-warning btn-flat">Cancel</a>

</div>
<script>
    $(document).ready(function () {
        $(".select").select2();
    });
</script>