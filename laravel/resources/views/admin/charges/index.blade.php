@extends('admin/admin_template')
<?php
$currency = Config::get('params.currency');
?>
@section('content')

<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
        @include('front/common/errors')
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Charges (Total: {{count($model)}} )</h3>
                <div class="box-tools pull-right">
<!--                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>-->

                </div>
            </div>
            <div class="box-body">

                <ul class="products-list product-list-in-box" style="
                    overflow: auto;
                    max-height: 700px;">
                    @if(count($model)>0)

                    <table class="table" id="order_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Rate</th>
                                <th>Date</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach ($model as $row)
                            <tr>
                                <td><a href="{{ url('admin/charges/edit/'.$row->id) }}"><?php echo $i; ?></a></td>
                                <td><a href="{{ url('admin/charges/edit/'.$row->id) }}">{{$row->name}}</a></td>
                                <td><a href="{{ url('admin/charges/edit/'.$row->id) }}">{{$row->type}}</a></td>
                                <td>{{ $currency[Config::get('params.currency_default')]['symbol']}} {{$row->rate}}</td>
                                <td>{{ date('d/m/Y',strtotime($row->created_at))}}</td>
                            </tr>

                            <?php $i++; ?>
                            @endforeach
                        </tbody>

                    </table>
                    @else
                    <div class="col-sm-6">
                        <h3>No Data found. . .</h3>
                    </div>
                    @endif
                    <?php
                    //echo $model->render(); 
                    ?>
                </ul>
            </div>

        </div>
    </div>
</div>
<script>
    jQuery('.delete').click(function ()
    {
        $('#delete').attr('href', $(this).data('id'));
    });
</script>
@endsection