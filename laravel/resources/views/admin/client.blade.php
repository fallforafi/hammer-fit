@extends('admin/admin_template')

@section('content')
<?php
$currency = Config::get('params.currency');
$orderPrefix = Config::get('params.order_prefix');
$per_month = Config::get('params.per_month');
?>
<style>
    .result_img img {
        /*height: 200px;*/
        width: 80%; 
    padding-left: 140px;
    padding-right: 50px;
        /*        padding-bottom: 50px;
                padding-top: 50px;*/
    }

</style>


<div class="row">
    <div class="col-md-12">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>

            @endif
            @endforeach
        </div> <!-- end .flash-message -->
        @include('admin.commons.errors')

        <div class="box box-primary">

            <?php
            $user = $data;
            ?>        
            @if($data->status == 1)
            <div class="box-body client__info">

                @else
                <div class="box-body">

                    @endif
                    <div class="row">
                        <div class="col-sm-3 client__img">
                            <div class="__img">
                                @if($data->image == '')
                                <img  src="{{ asset('front/images/usr.jpg')}}" alt="User Avatar">
                                @else
                                <img src="{{ asset('uploads/users/'.$data->image) }}" alt="User Avatar" style="">
                                @endif
                            </div>
                        </div>


                        <div class="col-md-9">

                            <div class="hed col-sm-12">
                                <h3>Account Status: 
                                    @if($data->status == 1)
                                    <span class="green">Approved</span>
                                    @else
                                    <span class="red">Dispproved</span>
                                    @endif
                                </h3>

                                <div class="actions pul-rgt">
                                    @include('admin/commons/users_action')
                                </div>
                            </div>

                            <div class="col-sm-7 client cont p0">

                                <div class="box-header with-border">
                                    <h3 class="box-title"> Customer's Information ( @if($data->role_id == 2) Athlete @elseif($data->role_id == 4) General User @else Ambassador @endif )</h3>
                                </div>
                                <ul>
                                    <li>First Name:  {{ $data->firstName }}</li>
                                    <li>Middle Name: {{ $data->middleName }}</li>
                                    <li>Last Name: {{ $data->lastName }}</li>
                                    <li>DOB: {{$data->dob }}</li>
                                    <li>Gender: @if($data->gender == 'f') Female @else Male @endif </li>
                                    <li>Email: {{ $data->email }}</li>
                                    <li>Height: {{ $data->height }}</li>
                                    <li>Weight: {{ $data->weight }}</li>
                                    <li>Medical Concerns: {{ $data->medicalConcerns }}</li>
                                    <li>Goals: {{ $data->goals }}</li>
                                    <li>Experience: @if($data->experience == '') Not Mentioned Yet @else {{ $data->experience }}+ @endif </li>
                                    <li>Member Since: {{ date('d M Y',strtotime($data->created_at))}}</li>
                                </ul>
                            </div>

                            <div class="col-sm-5">
                                <div class="box-header with-border">
                                    <h3 class="box-title"> Address Information</h3>
                                </div>
                                <ul>
                                    <li>Address :{{ $data->address }}</li>
                                    <li>Address 2:{{ $data->address2 }}</li>
                                    <li>Phone :{{ $data->phone }}</li>
<!--                                    <li>Mobile :{{ $data->mobile }}</li>-->
                                    <li>Country: {{ $data->countryName }}</li>
                                    <li>State: {{ $data->stateName }}  </li>
                                    <li>City: {{ $data->city }}</li>
                                    <li>Zip Code: {{ $data->zip }}</li>
                                </ul>
                                <div class="col-sm-8">
                                    <h4>Date of Next Show:</h4>
                                </div>
                                <div class="col-sm-4">
                                    <h4 class="bg-info">@if(is_null($shows)) No Date @else {{ date('d/m/Y', strtotime($shows->date)) }}@endif</h4></div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
        @if(!($data->role_id == 3))
        <div class="col-md-12">
            <div class="box">
                <div class="box-body client__info">
                    <div class="box-body">  
                        <div class="hed col-sm-12">
                            <h3>Requested Coach: 
                                @if(count($coach)>0)
                                <span><a href="{{ url('admin/client/'.$coach->id) }}">{{$coach->firstName}} {{$coach->lastName}}</a></span>
                                @else
                                <span>No Data found. . .</span>
                                @endif
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if($data->role_id == 2)
        @include('admin.client_athlete')
        @elseif($data->role_id == 3)
        @include('admin.client_ambassador')
        
        @endif
        @if(!($data->role_id == 4))
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">( Total Subscriptions : {{ count($subscriptions) }} ) </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>
                <div class="box-body" style="
                     overflow: auto;
                     max-height: 700px;">
                    <ul class="products-list product-list-in-box">
                        @if(count($subscriptions)>0)
                        <table class="table table-">
                            <thead class="text-center fixed">
                            <th>#</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach($subscriptions as $row)
                                <tr>
                                    <td><?php
                                        echo $i;
                                        $i++;
                                        ?></td>
                                    @if($data->id == $row->athlete_id)
                                    <td><a href="{{ url('admin/client/'.$row->ambassador_id) }}" class="product-title">{{ $row->ambassadorFirstname }} {{ $row->ambassadorLastname }}</a></td>
                                    @else
                                    <td><a href="{{ url('admin/client/'.$row->athlete_id) }}" class="product-title">{{ $row->athleteFirstname }} {{ $row->athleteLastname }}</a></td>
                                    @endif
                                    <td>{{ date("d/m/Y", strtotime($row->created_at)) }}</td>
                                    <td>@if($row->subscriptionStatus == 1)<span class="label label-success">SUBSCRIBED</span>@elseif($row->subscriptionStatus == 2)<span class="label label-warning">UNSUBSCRIBED</span>@endif</td>
                                    <td>
                                        <button type="button" @if($row->subscriptionStatus != 1)disabled="disabled"@endif class="btn btn-danger btn-sm delete" data-toggle="modal" data-target="#myModal" data-id="<?php echo url('admin/unsubscribe/' . $row->athlete_id); ?>"><i class="fa fa-ban"></i></button>
                                    </td>

                                </tr>
                                @endforeach 
                            </tbody>
                            @include('admin/commons/delete_modal')

                        </table>
                        @else
                        <div class="bg-warning">Sorry, there is no subscription added yet </div>
                        @endif
                    </ul>
                </div>
            </div>
        </div>         
        @endif
    </div>
    <script>
        jQuery('.delete').click(function ()
        {
            $('#delete').attr('href', $(this).data('id'));
        });
    </script>
    @endsection
