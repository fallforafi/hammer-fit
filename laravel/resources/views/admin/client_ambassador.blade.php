<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">( Total Certifications : {{ count($certifications) }} ) </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>

            </div>
        </div>

        <div class="box-body">
            <ul class="products-list product-list-in-box">
                @if(count($certifications)>0)
                <div class="table-responsive">          
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Year</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Download Link</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($certifications as $row)
                            <tr>
                                <td><?php
                                    echo $i;
                                    $i++;
                                    ?></td>
                                <td>{{ $row->year }}</td>         
                                <td>{{ $row->title }}</td>
                                <td>{{ $row->description }}</td>      
                                <td>
                                    <a href="{{ url('uploads/users/'.$row->file) }}" download="{{$row->file}}" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i> Download</a></td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="bg-warning">Sorry, there is no certificate added yet</div>
                @endif
            </ul>
        </div>
    </div>
</div>