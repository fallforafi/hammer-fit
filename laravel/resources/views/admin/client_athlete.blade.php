<?php

use App\Functions\Functions;
?>
<link rel="stylesheet" href="{{asset('front/croppie.css')}}">
<script src="{{asset('front/js/croppie.js')}}"></script>
<div class="col-sm-12">
    <div class="box box-primary">
        <div class="box-body">
            <div class="col-sm-4 col-sm-offset-4">
                <br>
                <button type="button" class="btn btn-primary btn-block btn-lg delete" data-toggle="modal" data-target="#myModal" data-id="<?php echo url('admin/cancel-membership/user/' . $data->id); ?>">
                    Membership Cancel
                </button>
                <br>
            </div>
            <div class="col-sm-12">
                <div class="alert alert-warning">
                    <p><span>Note: </span><strong>Once you cancel the membership, this user will lose access as an "Athlete" immediately.</strong></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Current Meal Plan </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>

            </div>
        </div>

        <div class="box-body">
            <ul class="products-list product-list-in-box">
                @if(count($athleteMp)>0)
                <div class="table-responsive">          
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Download Link</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
<?php $i = 1; ?>
                            @foreach($athleteMp as $row)
                            <tr>
                                <td><?php
                                    echo $i;
                                    $i++;
                                    ?></td>
                                <td>{{ $row->title }}</td>
                                <td>{{ $row->description }}</td>
                                <td>
                                    <a href="{{ url('uploads/users/meal-plans/'.$row->file) }}" download="{{$row->file}}" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i> Download</a>
                                </td>
                                <td><button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-id="<?php echo url('admin/meal-plan/delete/' . $row->id); ?>"><i class="fa fa-trash"></i></button></td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="bg-warning">Sorry, there is no plan added yet </div>
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Current Workout Plan </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>

            </div>
        </div>

        <div class="box-body">
            <ul class="products-list product-list-in-box">
                @if(count($athleteWp)>0)
                <div class="table-responsive">          
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Download Link</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
<?php $i = 1; ?>
                            @foreach($athleteWp as $row)
                            <tr>
                                <td><?php
                                    echo $i;
                                    $i++;
                                    ?></td>
                                <td>{{ $row->title }}</td>
                                <td>{{ $row->description }}</td>
                                <td>
                                    <a href="{{ url('uploads/users/workout-plans/'.$row->file) }}" download="{{$row->file}}" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i> Download</a></td>
                                <td><button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-id="<?php echo url('admin/workout-plan/delete/' . $row->id); ?>"><i class="fa fa-trash"></i></button></td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="bg-warning">Sorry, there is no plan added yet </div>
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="col-sm-12 col-xs-12">
    <form method="POST" action="<?php echo url('admin/collage'); ?>">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">( Total Results : {{ count($results) }} ) </h3> (<label>Press Ctrl + F5 to refresh</label>)
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            @if(count($results)>0)
            <div class="box-body">
                <div class="box-tools pull-right">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <button type="submit" class="btn btn-primary">Compare Images</button>
                </div><br><br>
                <ul class="products-list product-list-in-box col-sm-12 col-xs-12">
                    @foreach($results as $row)
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered">
                            <td colspan="3" class="text-left" for="Updates 1"><center><strong>{{ $row->caption }}</strong></center></td>
                            <td><button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-id="<?php echo url('admin/result/delete/' . $row->id); ?>"><i class="fa fa-trash"></i></button></td>
                            <tbody>

                                <tr class="text-center">
                                    <td>Front Picture</td>
                                    <td>Side Picture</td>
                                    <td>Back Picture</td>
                                </tr>
                                <tr class="">

                                    <td>
                                        <div id="success-front-<?php echo $row->id; ?>"></div>
                                        <div class=" col-sm-8 actions pull-right">
                                            <i class="fa fa-rotate-left vanilla-front-rotate-<?php echo $row->id; ?> btn btn-default" data-deg="-90" aria-hidden="true"></i>
                                            <i class="fa fa-rotate-right vanilla-front-rotate-<?php echo $row->id; ?> btn btn-default" data-deg="90" aria-hidden="true"></i>

                                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                            <i class="btn btn-default upload-front-result-<?php echo $row->id; ?> fa fa-save"></i>
                                            <i class="btn btn-default refresh fa fa-refresh"></i>

                                        </div>
                                        <div class="" id="front-<?php echo $row->id; ?>" style="">
                                        </div>
                                        <script type="text/javascript">
<?php if (is_null($row->frontImage)) { ?>
    var srcFront<?php echo $row->id; ?> = "{{ asset('front/images/no_result.jpg')}}";
<?php } else { ?>
    var srcFront<?php echo $row->id; ?> = "{{ asset('uploads/users/results/front_images/thumbnail/'. $row->frontImage)}}";
<?php } ?>
var el = document.getElementById('front-<?php echo $row->id; ?>');
vanillaFront<?php echo $row->id; ?> = new Croppie(el, {
    viewport: {width: 200, height: 200},
    boundary: {width: 300, height: 300},
    enableResize: true,
    enableOrientation: true
});
//$('#imgInp').on('change', function () {
//   var reader = new FileReader();
//   reader.onload = function (e) {
vanillaFront<?php echo $row->id; ?>.bind({
    url: srcFront<?php echo $row->id; ?>,
    zoom: 0
}).then(function () {
    console.log('jQuery bind complete');
});

$('.vanilla-front-rotate-<?php echo $row->id; ?>').on('click', function (ev) {
    vanillaFront<?php echo $row->id; ?>.rotate(parseInt($(this).data('deg')));
});

$('.upload-front-result-<?php echo $row->id; ?>').on('click', function (ev) {
    vanillaFront<?php echo $row->id; ?>.result({
        type: 'canvas',
        size: 'viewport'
    }).then(function (resp) {
        var _token = $('#_token').val();
        $.ajax({
            url: "<?php echo url('crop'); ?>",
            type: "POST",
            dataType: 'json',
            data: {"image": resp, '_token': _token, "src": srcFront<?php echo $row->id; ?>},
            success: function (data) {
                console.log(data);
                if (data.success === 1) {
                    var errorsHtml = '<div class="alert alert-success"> <i class="fa fa-check"></i> Picture Updated Successfully.</div>';
                    $('#success-front-<?php echo $row->id; ?>').html(errorsHtml).show();
                    setTimeout(function () {
                        location.reload(true);
                    }, 300);
                }
            }
        });
    });
    //
});
                                        </script>
                                        <!--                                        <div class="result_img">
                                                                                    @if(is_null($row->frontImage))
                                                                                    <img  alt="" src="{{ asset('front/images/no_result.jpg')}}"/>
                                                                                    @else
                                                                                    <img src="{{ url('uploads/users/results/front_images/thumbnail/'. $row->frontImage)}}">
                                                                                    @endif
                                                                                </div>-->

                            <center>
                                <?php
                                $frontImage = public_path() . '/uploads/users/results/front_images/original/' . $row->frontImage;
                                if (file_exists($frontImage)) {
                                    ?>
                                    <a href="{{asset('uploads/users/results/front_images/original/')}}/<?php echo $row->frontImage; ?>" class="btn btn-default btn-sm" target="_blank">View Original</a>
                                <?php } else { ?>
                                    <a href="{{asset('uploads/users/results/front_images/')}}/<?php echo $row->frontImage; ?>" class="btn btn-default btn-sm" target="_blank">View Original</a>
<?php } ?>
                            </center>
                            <label><input type="checkbox" name="image[<?php echo $row->id ?>][front]" value="<?php echo $row->frontImage; ?>">Select</label>
                            </td>
                            <!--Side Images -->                       
                            <td>
                                <div id="success-side-<?php echo $row->id; ?>"></div>
                                <div class=" col-sm-8 actions pull-right">
                                    <i class="fa fa-rotate-left vanilla-side-rotate-<?php echo $row->id; ?> btn btn-default" data-deg="-90" aria-hidden="true"></i>
                                    <i class="fa fa-rotate-right vanilla-side-rotate-<?php echo $row->id; ?> btn btn-default" data-deg="90" aria-hidden="true"></i>

                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                    <i class="btn btn-default upload-side-result-<?php echo $row->id; ?> fa fa-save"></i>
                                    <i class="btn btn-default refresh fa fa-refresh"></i>

                                </div>
                                <div class="" id="side-<?php echo $row->id; ?>" style="">
                                </div>
                                <script type="text/javascript">
<?php if (is_null($row->sideImage)) { ?>
                                        var srcSide<?php echo $row->id; ?> = "{{ asset('front/images/no_result.jpg')}}";
<?php } else { ?>
                                        var srcSide<?php echo $row->id; ?> = "{{ asset('uploads/users/results/side_images/thumbnail/'. $row->sideImage)}}";
<?php } ?>
                                    var el = document.getElementById('side-<?php echo $row->id; ?>');
                                    vanilla<?php echo $row->id; ?> = new Croppie(el, {
                                        viewport: {width: 200, height: 200},
                                        boundary: {width: 300, height: 300},
                                        enableResize: true,
                                        enableOrientation: true
                                    });
//$('#imgInp').on('change', function () {
//   var reader = new FileReader();
//   reader.onload = function (e) {
                                    vanilla<?php echo $row->id; ?>.bind({
                                        url: srcSide<?php echo $row->id; ?>,
                                        zoom: 0
                                    }).then(function () {
                                        console.log('jQuery bind complete');
                                    });

                                    $('.vanilla-side-rotate-<?php echo $row->id; ?>').on('click', function (ev) {
                                        vanilla<?php echo $row->id; ?>.rotate(parseInt($(this).data('deg')));
                                    });

                                    $('.upload-side-result-<?php echo $row->id; ?>').on('click', function (ev) {
                                        vanilla<?php echo $row->id; ?>.result({
                                            type: 'canvas',
                                            size: 'viewport'
                                        }).then(function (resp) {
                                            var _token = $('#_token').val();
                                            $.ajax({
                                                url: "<?php echo url('crop'); ?>",
                                                type: "POST",
                                                dataType: 'json',
                                                data: {"image": resp, '_token': _token, "src": srcSide<?php echo $row->id; ?>},
                                                success: function (data) {
                                                    console.log(data);
                                                    if (data.success === 1) {
                                                        var errorsHtml = '<div class="alert alert-success"> <i class="fa fa-check"></i> Picture Updated Successfully.</div>';
                                                        $('#success-side-<?php echo $row->id; ?>').html(errorsHtml).show();
                                                        setTimeout(function () {
                                                            location.reload(true);
                                                        }, 300);
                                                    }
                                                }
                                            });
                                        });
//
                                    });
                                </script>
                                <!--                                        <div class="result_img">
                                                                            @if(is_null($row->frontImage))
                                                                            <img  alt="" src="{{ asset('front/images/no_result.jpg')}}"/>
                                                                            @else
                                                                            <img src="{{ url('uploads/users/results/front_images/thumbnail/'. $row->frontImage)}}">
                                                                            @endif
                                                                        </div>-->
                            <center>
                                <?php
                                $sideImage = public_path() . '/uploads/users/results/side_images/original/' . $row->sideImage;
                                if (file_exists($sideImage)) {
                                    ?>
                                    <a href="{{asset('uploads/users/results/side_images/original/')}}/<?php echo $row->sideImage; ?>" class="btn btn-default" target="_blank">View Original</a>
                                <?php } else { ?>
                                    <a href="{{asset('uploads/users/results/side_images/')}}/<?php echo $row->sideImage; ?>" class="btn btn-default" target="_blank">View Original</a>
<?php } ?>
                            </center>
                            <label><input type="checkbox" name="image[<?php echo $row->id ?>][side]" value="<?php echo $row->sideImage; ?>">Select</label>
                            </td>
                            <!--Back Images -->                           
                            <td>
                                <div id="success-back-<?php echo $row->id; ?>"></div>
                                <div class=" col-sm-8 actions pull-right">
                                    <i class="fa fa-rotate-left vanilla-back-rotate-<?php echo $row->id; ?> btn btn-default" data-deg="-90" aria-hidden="true"></i>
                                    <i class="fa fa-rotate-right vanilla-back-rotate-<?php echo $row->id; ?> btn btn-default" data-deg="90" aria-hidden="true"></i>

                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                    <i class="btn btn-default upload-back-result-<?php echo $row->id; ?> fa fa-save"></i>
                                    <i class="btn btn-default refresh fa fa-refresh"></i>

                                </div>
                                <div class="" id="back-<?php echo $row->id; ?>" style="">
                                </div>
                                <script type="text/javascript">
<?php if (is_null($row->backImage)) { ?>
                                        var srcBack<?php echo $row->id; ?> = "{{ asset('front/images/no_result.jpg')}}";
<?php } else { ?>
                                        var srcBack<?php echo $row->id; ?> = "{{ asset('uploads/users/results/back_images/thumbnail/'. $row->backImage)}}";
<?php } ?>
                                    var el = document.getElementById('back-<?php echo $row->id; ?>');
                                    vanillaBack<?php echo $row->id; ?> = new Croppie(el, {
                                        viewport: {width: 200, height: 200},
                                        boundary: {width: 300, height: 300},
                                        enableResize: true,
                                        enableOrientation: true
                                    });
//$('#imgInp').on('change', function () {
//   var reader = new FileReader();
//   reader.onload = function (e) {
                                    vanillaBack<?php echo $row->id; ?>.bind({
                                        url: srcBack<?php echo $row->id; ?>,
                                        zoom: 0
                                    }).then(function () {
                                        console.log('jQuery bind complete');
                                    });

                                    $('.vanilla-back-rotate-<?php echo $row->id; ?>').on('click', function (ev) {
                                        vanillaBack<?php echo $row->id; ?>.rotate(parseInt($(this).data('deg')));
                                    });

                                    $('.upload-back-result-<?php echo $row->id; ?>').on('click', function (ev) {
                                        vanillaBack<?php echo $row->id; ?>.result({
                                            type: 'canvas',
                                            size: 'viewport'
                                        }).then(function (resp) {
                                            var _token = $('#_token').val();
                                            $.ajax({
                                                url: "<?php echo url('crop'); ?>",
                                                type: "POST",
                                                dataType: 'json',
                                                data: {"image": resp, '_token': _token, "src": srcBack<?php echo $row->id; ?>},
                                                success: function (data) {
                                                    console.log(data);
                                                    if (data.success === 1) {
                                                        var errorsHtml = '<div class="alert alert-success"> <i class="fa fa-check"></i> Picture Updated Successfully.</div>';
                                                        $('#success-back-<?php echo $row->id; ?>').html(errorsHtml).show();
                                                        setTimeout(function () {
                                                            location.reload(true);
                                                        }, 300);
                                                    }
                                                }
                                            });
                                        });
//
                                    });
                                </script>
                                <!--                                        <div class="result_img">
                                                                            @if(is_null($row->frontImage))
                                                                            <img  alt="" src="{{ asset('front/images/no_result.jpg')}}"/>
                                                                            @else
                                                                            <img src="{{ url('uploads/users/results/front_images/thumbnail/'. $row->frontImage)}}">
                                                                            @endif
                                                                        </div>-->
                            <center>
                                <?php
                                $backImage = public_path() . '/uploads/users/results/back_images/original/' . $row->backImage;
                                if (file_exists($backImage)) {
                                    ?>
                                    <a href="{{asset('uploads/users/results/back_images/original/')}}/<?php echo $row->backImage; ?>" class="btn btn-default" target="_blank">View Original</a>
                                <?php } else { ?>
                                    <a href="{{asset('uploads/users/results/back_images/')}}/<?php echo $row->backImage; ?>" class="btn btn-default" target="_blank">View Original</a>
<?php } ?>
                            </center>
                            <label><input type="checkbox" name="image[<?php echo $row->id ?>][back]" value="<?php echo $row->backImage; ?>">Select</label>
                            </td>

                            </tr>
                            <tr class="upd__cmt__hdr">
    <!--                            <td colspan="2" class="text-left" for="Updates 1"><i class="fa fa-comments-o"></i> Ambassador Reviews</td>-->
                                <td>Current Weight:   <strong> {{ ($row->currentWeight != "")? $row->currentWeight.' '.'LBS':'Not Added Yet'  }}</strong> </td>
    <!--                            <td colspan="2" class="text-left" for="Updates 1"><i class="fa fa-comments-o"></i> Ambassador Reviews</td>-->
                                <td>Body Fat Est:   <strong> {{ ($row->currentBodyFat != "")? $row->currentBodyFat.' '.'%':'Not Added Yet'  }}</strong></td>

    <!--                            <td colspan="2" class="text-left" for="Updates 1"><i class="fa fa-comments-o"></i> Ambassador Reviews</td>-->
                                <td>Uploaded Date:  <i class="fa fa-calendar"></i> <strong> {{ date('d/m/Y', strtotime($row->date)) }}</strong></td>
                            </tr>

                            </tbody>

                        </table>

                    </div>
                    <!--                <div class="">
                                        @foreach($messages as $message)
                                        @if($message->result_id == $row->id)
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="{{ asset('front/images/amb-review-img.png') }}" class="media-object">
                                            </div>
                                            <div class="review__online"><img src="{{ asset('front/images/online-icon.png') }}"></div>
                    
                                            <div class="media-body media-body-icon">
                                                <p>
                                                    {{ $message->message }}
                                                </p>
                                                <ul>
                                                    <li class="pul-lft"><i class="fa fa-clock-o"></i><?php
                    echo
                    Functions::relativeTime(strtotime($message->created_at), true);
                    ?></li>
                                        <li class="pul-rgt"><i class="fa fa-check-circle-o"></i></li>
                                    </ul>
                                </div>
        
                            </div>
                            @endif
                            @endforeach                       
                        </div>-->
                    @endforeach 
                    @include('admin/commons/delete_modal')


                </ul>
            </div>
            @else
            <div class="box-body">
                <div class="bg-warning">Sorry, there is no result added yet </div>
            </div>
            @endif

        </div>
    </form>
</div>
 @include('admin/commons/delete_modal')
<script>
    $('.refresh').on('click', function (ev) {
        window.location.reload(true);
    });
    jQuery('.delete').click(function ()
    {
        $('#delete').attr('href', $(this).data('id'));
    });
</script>
