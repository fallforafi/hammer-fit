@extends('admin/admin_template')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Horizontal Form -->
<div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
       @endif
       @endforeach
       </div> <!-- end .flash-message -->
        <!-- /.box -->
        <!-- general form elements disabled -->
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Speciality</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                {!! Form::model($speciality, ['files' => true,'class' => 'form','url' => ['admin/specialities/update', $speciality->id], 'method' => 'post']) !!}
                <!-- text input -->

                @include('admin.specialities.form')

                {!! Form::close() !!}
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

@endsection