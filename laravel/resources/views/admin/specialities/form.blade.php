<?php
$required = "required";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('Title *') !!}
    {!! Form::text('title', null , array('class' => 'form-control',$required) ) !!}
</div>


<div class="form-group">
    {!! Form::label('description *') !!}
    {!! Form::textarea('description', null, ['size' => '105x3','class' => 'form-control ckeditor',$required]) !!} 
</div>


<div class="form-group">
   
        <button type="submit" value="products" class="btn btn-primary btn-flat">Save</button>
  

        <a href="{{ url('admin/specialities')}}" class="btn btn-warning btn-flat">Cancel</a>

</div>
<script>
    $(document).ready(function () {
        $(".select").select2();
    });
</script>