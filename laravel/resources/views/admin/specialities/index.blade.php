@extends('admin/admin_template')

@section('content')


<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <div class="col-md-12" >
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
            @endforeach
        </div> <!-- end .flash-message -->
        <!-- PRODUCT LIST -->
        <div class="box-body table-responsive no-padding">

            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-header with-border">
                        <h3 class="box-title">Specialities( Total : {{ count($specialities) }} ) </h3>

                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-info btn-flat" href="{{url('admin/specialities/create')}}">Add new Speciality</a>

                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Added</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($specialities as $row)
                                <tr>
                                    <td><a href="specialities/edit/<?php echo $row->id ?>" class="product-title"><?php echo $row->id; ?></a></td>
                                    <td><a href="specialities/edit/<?php echo $row->id ?>" class="product-title"><?php echo $row->title; ?></a></td>
                                    <td><?php echo $row->description; ?></td>
                                    <td><?php echo $row->created_at; ?></td>
                                    <td><button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-id="specialities/delete/<?php echo $row->id ?>"><i class="fa fa-trash"></i></button></td>
                                </tr>
                                @endforeach
                            @include('admin/commons/delete_modal')
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                </div>
                <!-- /.box-footer -->
            </div>

        </div>
    </div>
</div>
<script>
    jQuery('.delete').click(function ()
    {
        $('#delete').attr('href', $(this).data('id'));
    });
</script>
@endsection

