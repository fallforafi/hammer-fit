@extends('admin/admin_template')
<?php
$currency = Config::get('params.currency');
$per_month =Config::get('params.per_month');
?> 
@section('content')


<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <div class="col-md-12" >
      <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
       @endif
       @endforeach
       </div> <!-- end .flash-message -->
        <!-- PRODUCT LIST -->
        <div class="box-body table-responsive no-padding">

            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="box-header with-border">
                        <h3 class="box-title">All Transactions ( Total : {{ count($model) }} ) </h3>

<!--                        <div class="box-tools pull-right">
                            <a class="btn btn-sm btn-info btn-flat" href="{{url('admin/specialities/create')}}">Add new Speciality</a>

                        </div>-->
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                  <th>User ID</th>
                                  <th>Type</th>
                                  <th>Gateway</th>
                                  <th>Profile ID</th>
                                  <th>Message</th>
                                  <th>Date Time</th>
                                  <th>Amount</th>   
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($model as $row)
                                <tr>
                                    <td>{{ $row->type_id }}</td>
                                    <td>{{ $row->type }}</td>
                                    <td>{{ $row->gateway }}</td>
                                    <td>{{ $row->profileId }}</td>
                                    <td>{{ $row->message }}</td>
                                    <td>{{ date('d/m/Y h:i:s',strtotime($row->created_at)) }}</td>
                                    <td><?php echo $currency[Config::get('params.currency_default')]['symbol'] ?> {{ $row->amount }}</td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                      
                    </div>
                    <!-- /.table-responsive -->
                     <?php echo $model->render(); ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                </div>
                <!-- /.box-footer -->
            </div>

        </div>
    </div>
</div>

@endsection

