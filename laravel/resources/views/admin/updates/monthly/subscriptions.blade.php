@extends('admin/admin_template')
<?php
$currency = Config::get('params.currency');
$per_month = Config::get('params.per_month');
?> 
@section('content')


<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <div class="col-md-12" >
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
            @endforeach
        </div> <!-- end .flash-message -->
        <!-- PRODUCT LIST -->
        <div class="box-body table-responsive no-padding">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="box-header with-border">
                        <h3 class="box-title">All Monthly Ambassador's Subscriptions ( Total : {{ count($model) }} ) </h3>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="
                     overflow: auto;
                     max-height: 700px;">
                    <ul class="products-list product-list-in-box">
                        @if(count($model)>0)
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Subscribed By</th>
                                    <th>Subscribed To</th>
                                    <th>Fees</th>
                                    <th>Date</th>   
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($model as $row)
                                <tr>
                                    <td><?php echo $i;
                                $i++;
                                ?></td>
                                    <td><a href="{{ url('admin/client/'.$row->athlete_id) }}" class="product-title">{{ $row->athlete_firstName }} {{ $row->athlete_lastName }}</a></td>
                                    <td><a href="{{ url('admin/client/'.$row->ambassador_id) }}" class="product-title">{{ $row->ambassador_firstName }} {{ $row->ambassador_lastName }}</a></td>
                                    <td><?php echo $currency[Config::get('params.currency_default')]['symbol'] ?> {{ $row->amount }} {{$per_month}}</td>
                                    <td>{{ date("d/m/Y", strtotime($row->subscriptionDate)) }}</td>
                                    <td>@if($row->subscriptionStatus == 1)<span class="bg-success">SUBSCRIBED</span>@elseif($row->subscriptionStatus == 2)<span class="bg-warning">UNSUBSCRIBED</span>@endif</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <div class="col-sm-6">
                            <h3>No Data found. . . </h3>
                        </div>
                        @endif
                    </ul>

                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
        </div>

    </div>
</div>
</div>

@endsection

