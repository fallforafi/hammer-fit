@extends('admin/admin_template')

@section('content')
<?php
$currency = Config::get('params.currency');
$orderPrefix = Config::get('params.order_prefix');
?>
<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
        @include('admin.commons.errors')
        <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All Weekly Orders (Total: {{ count($model) }} ) </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="
                     overflow: auto;
                     max-height: 700px;">
                    <ul class="products-list product-list-in-box">
                    @if(count($model)>0)
                    <table class="table" id="order_table">
                        <thead>
                            <tr>
                                <th>Order Id</th>
                                <th>Name</th>
                                <th>Added</th>
                                <th>Status</th>
                                <th>Total</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($model as $order)
                            <tr>
                                <td><?php echo $orderPrefix; ?>{{$order->id}}</td>
                                <td>{{ $order->billingFirstName.' '.$order->billingLastName }}</td>
                                <td>{{ date('d M Y',strtotime($order->created_at))}}</td>
                                <td>{{ ucfirst($order->orderStatus)}}</td>
                                <td>{{ $currency[Config::get('params.currency_default')]['symbol']}}{{ $order->grandTotal}}</td>
                                <td><a class="btn btn-info" href="{{ url('admin/order/'.$order->id) }}">Detail</a></td>
                                <td><button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-id="<?php echo url('admin/order/delete/'.$order->id); ?>"><i class="fa fa-trash"></i></button></td>                                    
                            </tr>
                            @endforeach 
                            @include('admin/commons/delete_modal')
                        </tbody>
                    </table>
                    @else
                    <div class="col-sm-6">
                        <h3>No Data found. . . </h3>
                    </div>
                    @endif
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <!-- /.col -->

</div>
<script>
jQuery('.delete').click(function ()
{
    $('#delete').attr('href', $(this).data('id'));
});
</script>
@endsection

