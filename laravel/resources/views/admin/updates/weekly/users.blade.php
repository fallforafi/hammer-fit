@extends('admin/admin_template')

@section('content')

<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
        @include('front/common/errors')
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">All Weekly Users (Total: {{count($model)}} )</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>

                </div>
            </div>
            <div class="box-body" style="
                     overflow: auto;
                     max-height: 700px;">
                    <ul class="products-list product-list-in-box">
                    @if(count($model)>0)
                    <table class="table" id="order_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Joined As</th>
                                <th>Email</th>
                                <th>Registration Date</th>
                                <th>Status</th>
                                <td></td> 

                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach ($model as $row)
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $row->firstName . ' ' . $row->lastName; ?></td>
                                <td><?php echo ucfirst($row->role); ?></td>
                                <td><?php echo $row->email; ?></td>
                                <td><?php echo date("d M Y", strtotime($row->created_at)); ?></td>
                                <td><?php if ($row->status) { ?> <span class="green">Approved</span> <?php } else { ?><span class="red"> Disapproved </span><?php } ?></td>
                                <td><a href="{{ url('admin/client/'.$row->id) }}" class="btn btn-info btn-sm">Detail</a></td>
                                <td><button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-id="<?php echo url('admin/client/delete/' . $row->id); ?>"><i class="fa fa-trash"></i></button></td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @include('admin/commons/delete_modal')
                        </tbody>

                    </table>
                    @else
                    <div class="col-sm-6">
                        <h3>No Data found. . . </h3>
                    </div>
                    @endif
                </ul>
            </div>

        </div>
    </div>
</div>
<script>
    jQuery('.delete').click(function ()
    {
        $('#delete').attr('href', $(this).data('id'));
    });
</script>
@endsection