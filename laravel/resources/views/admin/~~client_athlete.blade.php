<?php

use App\Functions\Functions;
?>
<link rel="stylesheet" href="{{ asset('front/croppic.css') }}">
<script src="{{asset('front/js/croppic.min.js')}}"></script>

<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Current Meal Plan </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>

            </div>
        </div>

        <div class="box-body">
            <ul class="products-list product-list-in-box">
                @if(count($athleteMp)>0)
                <div class="table-responsive">          
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Download Link</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr> 
                                <td>{{ $athleteWp->title }}</td>
                                <td>{{ $athleteWp->description }}</td>
                                <td>
                                    <a href="{{ url('uploads/users/meal-plans/'.$athleteWp->file) }}" download="{{$athleteWp->file}}" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i> Download</a></td>

                            </tr>

                        </tbody>
                    </table>
                </div>
                @else
                <div class="bg-warning">Sorry, there is no plan added yet </div>
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Current Workout Plan </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>

            </div>
        </div>

        <div class="box-body">
            <ul class="products-list product-list-in-box">
                @if(count($athleteMp)>0)
                <div class="table-responsive">          
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Download Link</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr> 
                                <td>{{ $athleteMp->title }}</td>
                                <td>{{ $athleteMp->description }}</td>
                                <td>
                                    <a href="{{ url('uploads/users/workout-plans/'.$athleteMp->file) }}" download="{{$athleteMp->file}}" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i> Download</a></td>

                            </tr>

                        </tbody>
                    </table>
                </div>
                @else
                <div class="bg-warning">Sorry, there is no plan added yet </div>
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="col-sm-12 col-xs-12">
    <form method="POST" action="<?php echo url('admin/collage'); ?>">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">( Total Results : {{ count($results) }} ) </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            @if(count($results)>0)
            <div class="box-body">
                <div class="box-tools pull-right">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <button type="submit" class="btn btn-primary">Compare Images</button>
                </div><br><br>
                <ul class="products-list product-list-in-box col-sm-12 col-xs-12">
                    @foreach($results as $row)
                    <div class="table-responsive">
                        <table class="table table-condensed table-bordered">
                            <td colspan="3" class="text-left" for="Updates 1"><center><strong>{{ $row->caption }}</strong></center></td>
                            <td><button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-id="<?php echo url('admin/result/delete/' . $row->id); ?>"><i class="fa fa-trash"></i></button></td>
                            <tbody>

                                <tr class="text-center">
                                    <td>Front Picture</td>
                                    <td>Side Picture</td>
                                    <td>Back Picture</td>
                                </tr>
                                <tr class="">
                                    <td>
                                        <style>
                                            #front-<?php echo $row->id; ?>{ width:250px; height:200px; position: relative; border:1px solid #ccc;}
                                        </style>
                                        <div id="front-<?php echo $row->id; ?>" class="okok"></div>

                                        <script>
<?php if (is_null($row->frontImage)) { ?>
    var src = "{{ asset('front/images/no_result.jpg')}}";
<?php } else { ?>
    var src = "{{ asset('uploads/users/results/front_images/thumbnail/'. $row->frontImage)}}";
<?php } ?>

var eyeCandy = $('#front-<?php echo $row->id; ?>');
var croppedOptions = {

    imgEyecandy: true,
    imgEyecandyOpacity: 0.2,
    //uploadUrl: 'upload',
    cropUrl: '<?php echo url('crop'); ?>',
    loadPicture: src,
    loaderHtml: '<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
    cropData: {
        'width': eyeCandy.width(),
        'height': eyeCandy.height()
    }
};

var cropperHeader = new Croppic('front-<?php echo $row->id; ?>', croppedOptions);
                                        </script>
                                        <br>
                            <center>
                                <?php
                                $frontImage = public_path() . '/uploads/users/results/front_images/thumbnail/' . $row->frontImage;
                                if (file_exists($frontImage)) {
                                    ?>
                                    <a href="{{asset('uploads/users/results/front_images/thumbnail/')}}/<?php echo $row->frontImage; ?>" class="btn btn-default btn-sm" target="_blank">View Enlarge</a>
                                <?php } else { ?>
                                    <a href="{{asset('uploads/users/results/front_images/')}}/<?php echo $row->frontImage; ?>" class="btn btn-default btn-sm" target="_blank">View Enlarge</a>
                                <?php } ?>
                            </center>

                            <label><input type="checkbox" name="image[<?php echo $row->id ?>][front]" value="<?php echo $row->frontImage; ?>">Select</label>
                            </td>
                            <td>
                                <div class="result_img">
                                    @if(is_null($row->sideImage))
                                    <img  alt="" src="{{ asset('front/images/no_result.jpg')}}"/>
                                    @else
                                    <img src="{{ url('uploads/users/results/side_images/thumbnail/'. $row->sideImage)}}">
                                    @endif                             
                                </div><br>
                            <center>
                                <?php
                                $sideImage = public_path() . '/uploads/users/results/side_images/original/' . $row->sideImage;
                                if (file_exists($sideImage)) {
                                    ?>
                                    <a href="{{asset('uploads/users/results/side_images/original/')}}/<?php echo $row->sideImage; ?>" class="btn btn-default" target="_blank">View Enlarge</a>
                                <?php } else { ?>
                                    <a href="{{asset('uploads/users/results/side_images/')}}/<?php echo $row->sideImage; ?>" class="btn btn-default" target="_blank">View Enlarge</a>
                                <?php } ?>
                            </center>
                            <label><input type="checkbox" name="image[<?php echo $row->id ?>][side]" value="<?php echo $row->sideImage; ?>">Select</label>
                            </td>
                            <td>
                                <div class="result_img">
                                    @if(is_null($row->backImage))
                                    <img  alt="" src="{{ asset('front/images/no_result.jpg')}}"/>
                                    @else                                 
                                    <img src="{{ url('uploads/users/results/back_images/thumbnail/'. $row->backImage)}}">
                                    @endif
                                </div><br>
                            <center>
                                <?php
                                $backImage = public_path() . '/uploads/users/results/back_images/original/' . $row->backImage;
                                if (file_exists($backImage)) {
                                    ?>
                                    <a href="{{asset('uploads/users/results/back_images/original/')}}/<?php echo $row->backImage; ?>" class="btn btn-default" target="_blank">View Enlarge</a>
                                <?php } else { ?>
                                    <a href="{{asset('uploads/users/results/back_images/')}}/<?php echo $row->backImage; ?>" class="btn btn-default" target="_blank">View Enlarge</a>
                                <?php } ?>
                            </center>
                            <label><input type="checkbox" name="image[<?php echo $row->id ?>][back]" value="<?php echo $row->backImage; ?>">Select</label>
                            </td>
                            </tr>
                            <tr class="upd__cmt__hdr">
    <!--                            <td colspan="2" class="text-left" for="Updates 1"><i class="fa fa-comments-o"></i> Ambassador Reviews</td>-->
                                <td colspan="3">Uploaded Date:  <i class="fa fa-calendar"></i> <strong> {{ date('d/m/Y', strtotime($row->date)) }}</strong></td>
                            </tr>

                            </tbody>


                        </table>

                    </div>
                    <!--                <div class="">
                                        @foreach($messages as $message)
                                        @if($message->result_id == $row->id)
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="{{ asset('front/images/amb-review-img.png') }}" class="media-object">
                                            </div>
                                            <div class="review__online"><img src="{{ asset('front/images/online-icon.png') }}"></div>
                    
                                            <div class="media-body media-body-icon">
                                                <p>
                                                    {{ $message->message }}
                                                </p>
                                                <ul>
                                                    <li class="pul-lft"><i class="fa fa-clock-o"></i><?php
                    echo
                    Functions::relativeTime(strtotime($message->created_at), true);
                    ?></li>
                                        <li class="pul-rgt"><i class="fa fa-check-circle-o"></i></li>
                                    </ul>
                                </div>
        
                            </div>
                            @endif
                            @endforeach                       
                        </div>-->
                    @endforeach 
                    @include('admin/commons/delete_modal')


                </ul>
            </div>
            @else
            <div class="box-body">
                <div class="bg-warning">Sorry, there is no result added yet </div>
            </div>
            @endif

        </div>
    </form>
</div>
<script>
    jQuery('.delete').click(function ()
    {
        $('#delete').attr('href', $(this).data('id'));
    });
</script>