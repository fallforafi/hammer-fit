<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
            <h1 style="display:inline; color:#333; margin-top:40px;margin-bottom:40px; padding:3px 0px; text-transform:uppercase; font-weight:300;">Membership Cancelled!</h1>
            <p style="margin-bottom:0px;">Hi {{ $firstName }} {{ $lastName }},</p> 
            <p><strong>Admin</strong> has cancelled your Membership at Hammer Fit.</p>
        </div>
        <footer style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
            <p style="margin-bottom:0px;font-weight:bold;">Many thanks,</p>
            <p style="margin-bottom:0px;font-weight:bold;">The Hammer Fit Team</p>
            <br>
            <div style="width:150px;margin-bottom:0px;"><a href="{{ URL::to('/') }}"><img src="{{ URL::to('front/images/logo.png') }}" alt="logo" style="width:100%"></a>
            </div>
        </footer>
    </body>
</html>