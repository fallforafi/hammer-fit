<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
            <h1 style="display:inline; color:#333; margin-top:40px;margin-bottom:40px; padding:3px 0px; text-transform:uppercase; font-weight:300;">Credit Card Updated!</h1>
            <p style="margin-bottom:0px;">Hi <?php echo $userName; ?>,</p> 
            <p>Congratulations! your <span>Credit Card</span> has been successfully updated.</p><br>
            
            <p><strong>Name: </strong> <span><?php echo $card->name; ?></span></p>
            <p><strong>Credit Card Number: </strong> <span><?php echo $card->cc; ?></span></p>
            <p><strong>CVC: </strong> <span><?php echo $card->cvc; ?></span></p>
            <p><strong>Expiry: </strong> <span><?php echo $card->expDate; ?></span></p>
        </div>
        <footer style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
            <p style="margin-bottom:0px;font-weight:bold;">Many thanks,</p>
            <p style="margin-bottom:0px;font-weight:bold;">The Hammer Fit Team</p>
            <br>
            <div style="width:150px;margin-bottom:0px;"><a href="{{ URL::to('/') }}"><img src="{{ asset('front/images/logo.png') }}" alt="HammerFit Logo" style="width:100%"></a>
            </div>
        </footer>
    </body>
</html>