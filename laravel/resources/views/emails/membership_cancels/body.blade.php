<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <div style="padding:8px; box-sizing:border-box;font-family:sans-serif;">
            <h1 style="display:inline; color:#333; margin-top:40px;margin-bottom:40px; padding:3px 0px; text-transform:uppercase; font-weight:300;">Membership Cancelled!</h1>
            <p style="margin-bottom:0px;">Hi Admin,</p> 
            <p><strong>{{ $firstName }} {{ $lastName }}</strong> has cancelled its Membership at Hammer Fit.</p>
        </div>
    </body>
</html>