@extends('layout')

@section('content')
<?php
$currency = Config::get('params.currency');
$required = 'required';
?>
<section class="account-area">
    <div class="container">
        <div class="hed">
            <h2>Become our athlete</h2><br>
            <p>Membership Fee (Charged monthly on Date of Renewal until cancelled) : <?php echo $currency[Config::get('params.currency_default')]['symbol'] ?><?php echo Config::get('params.subscription_fee_special'); ?></p>
        </div>
        <div class="acct__login acct__reg acct__inr__title col-sm-12">
            @include('front/common/errors')

            {!! Form::open(array( 'class' => 'form','url' => url('athlete/re-postregister'), 'name' => 'checkout')) !!}    

            @include('front/common/cc_form')

            <div class="form-group text-center">
                <button class="btn btn-lg btn-primary" type="submit" >Pay Now</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection