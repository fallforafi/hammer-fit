@extends('layout')

@section('content')
<?php
$currency = Config::get('params.currency');
$required = 'required';
?>
<section class="account-area">
    <div class="container">
        {!! Form::open(array( 'class' => 'form','url' => url('athlete/postregister'), 'name' => 'checkout')) !!}    
        <div class="hed update-pkg-label">
            <h2>Become our athlete</h2><br>
            <p><?php echo $joining;?> (One Time) : <?php echo $currency[Config::get('params.currency_default')]['symbol'] ?><?php echo $joiningRate; ?></p>
            <p>Below are the Membership Fees (Charged monthly on Date of Renewal until cancelled)</p>
            <?php
            if (count($charges) > 0) {
                foreach ($charges as $value) {
                    ?>
                    <label><input type="radio" name="type" required="required" value="<?php echo $value->type; ?>"> <?php echo $value->name; ?>  : <?php echo $currency[Config::get('params.currency_default')]['symbol'] ?><?php echo $value->rate; ?></label>
                <?php
                }
            }
            ?>

        </div>
        <div class="acct__login acct__reg acct__inr__title col-sm-12">
            @include('front/common/errors')

            @include('front/common/cc_form')

            <div class="payment_method clrlist text-center">
                <ul>
                    <li>
                        Managed by: 
                    </li>
                    <li>
                        <span class='paypalicon'><img src="{{ asset('front/images/paypal-logo.png') }}"></span>
                    </li>
                </ul>
            </div>
            <div class="form-group text-center">
                <span><p><strong>Note:</strong> This form submission could take <b>20 seconds</b> or longer, so keep patience!</p></span>
                <button class="btn btn-lg btn-primary" type="submit">Pay Now</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</section>
<script>
    $('form').submit(function () {
        $(this).find('button[type=submit]').prop('disabled', true);
    });
</script>
@endsection
