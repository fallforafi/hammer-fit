@extends('layout')

@section('content')
<?php
$currency = Config::get('params.currency');
?>
<section class="account-area">
    <div class="container">
        <div class="hed">
            <h2>Thank you for subscription at <?php echo Config('params.site_name') ?></h2>
        </div>
        <div class="acct__login acct__reg acct__inr__title text-center col-sm-12">
        </div>
    </div>
</div>
</section>
@endsection