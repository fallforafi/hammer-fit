@extends('customer')

@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        
        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="calculators">
                    <div class="workout-main fitness-main col-sm-12">
                        <div class="workout-inr fitness-inr">
                            <div class="hed">
                                <h2>PERCENTAGE OF FAT IN FOOD <span>CALCULATOR</span></h2>
                            </div>
                            <div class="fitness__calc__inputs">
                                <form>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <input type="text" class="form-control" id="itemName" placeholder="Item Name">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <input type="text" class="form-control" id="grams" placeholder="Fat in Grams">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <input type="text" class="form-control" id="calories" placeholder="Total Calories">
                                        </div>
                                    </div>
                                    <div class="fit__sub__reset clrlist">
                                        <ul class="pul-rgt">
                                            <li><button type="button" class="fit__submit">Submit</button></li>
                                            <li><button type="button" class="fit__reset">Reset</button></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div> 
                
            </div>
        </div>
        
    </div>
</section>

@endsection

