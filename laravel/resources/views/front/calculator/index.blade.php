@extends('customer')

@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        
        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="calculators" >
		    <div class="workout-main fitness-main col-sm-12">
					<div class="workout-inr fitness-inr">
						<div class="hed">
							<h2>HEALTH & FITNESS <span>CALCULATOR</span></h2>
						</div>
						<div class="workout__plans fitness__plans col-sm-4">
							<div class="workout__box fitness__box">
								<div class="workout__img fitness__img">
									<img src="{{ url('front/images/fitness1.png') }}" alt="fitness plan">
								</div>
								<div class="workout__plan__title fitness__plan__title">
									<h4>Food Fat Calculator</h4>
								</div>
								<div class="fitness__dtl">
									<a href="calculators/fitness-detail">Details</a>
								</div>
							</div>
						</div>
						<div class="workout__plans fitness__plans col-sm-4">
							<div class="workout__box fitness__box">
								<div class="workout__img fitness__img">
									<img src="{{ url('front/images/fitness2.png') }}" alt="fitness plan">
								</div>
								<div class="workout__plan__title fitness__plan__title">
									<h4>Protein Calculator</h4>
								</div>
								<div class="fitness__dtl">
									<a href="calculators/fitness-detail">Details</a>
								</div>
							</div>
						</div>
						<div class="workout__plans fitness__plans col-sm-4">
							<div class="workout__box fitness__box">
								<div class="workout__img fitness__img">
									<img src="{{ url('front/images/fitness3.png') }}" alt="fitness plan">
								</div>
								<div class="workout__plan__title fitness__plan__title">
									<h4>Calorie Calculator</h4>
								</div>
								<div class="fitness__dtl">
									<a href="calculators/fitness-detail">Details</a>
								</div>
							</div>
						</div>
						<div class="workout__plans fitness__plans col-sm-4">
							<div class="workout__box fitness__box">
								<div class="workout__img fitness__img">
									<img src="{{ url('front/images/fitness1.png') }}" alt="fitness plan">
								</div>
								<div class="workout__plan__title fitness__plan__title">
									<h4>Food Fat Calculator</h4>
								</div>
								<div class="fitness__dtl">
									<a href="calculators/fitness-detail">Details</a>
								</div>
							</div>
						</div>
						<div class="workout__plans fitness__plans col-sm-4">
							<div class="workout__box fitness__box">
								<div class="workout__img fitness__img">
									<img src="{{ url('front/images/fitness2.png') }}" alt="fitness plan">
								</div>
								<div class="workout__plan__title fitness__plan__title">
									<h4>Protein Calculator</h4>
								</div>
								<div class="fitness__dtl">
									<a href="calculators/fitness-detail">Details</a>
								</div>
							</div>
						</div>
						<div class="workout__plans fitness__plans col-sm-4">
							<div class="workout__box fitness__box">
								<div class="workout__img fitness__img">
									<img src="{{ url('front/images/fitness3.png') }}" alt="fitness plan">
								</div>
								<div class="workout__plan__title fitness__plan__title">
									<h4>Calorie Calculator</h4>
								</div>
								<div class="fitness__dtl">
									<a href="calculators/fitness-detail">Details</a>
								</div>
							</div>
						</div>
						<div class="workout__plans fitness__plans col-sm-4">
							<div class="workout__box fitness__box">
								<div class="workout__img fitness__img">
									<img src="{{ url('front/images/fitness1.png') }}" alt="fitness plan">
								</div>
								<div class="workout__plan__title fitness__plan__title">
									<h4>Food Fat Calculator</h4>
								</div>
								<div class="fitness__dtl">
									<a href="calculators/fitness-detail">Details</a>
								</div>
							</div>
						</div>
						<div class="workout__plans fitness__plans col-sm-4">
							<div class="workout__box fitness__box">
								<div class="workout__img fitness__img">
									<img src="{{ url('front/images/fitness2.png') }}" alt="fitness plan">
								</div>
								<div class="workout__plan__title fitness__plan__title">
									<h4>Protein Calculator</h4>
								</div>
								<div class="fitness__dtl">
									<a href="calculators/fitness-detail">Details</a>
								</div>
							</div>
						</div>
						<div class="workout__plans fitness__plans col-sm-4">
							<div class="workout__box fitness__box">
								<div class="workout__img fitness__img">
									<img src="{{ url('front/images/fitness3.png') }}" alt="fitness plan">
								</div>
								<div class="workout__plan__title fitness__plan__title">
									<h4>Calorie Calculator</h4>
								</div>
								<div class="fitness__dtl">
									<a href="calculators/fitness-detail">Details</a>
								</div>
							</div>
						</div>
						
					</div>
				  </div>
				</div>
        
            </div>
        </div>
    </div>
</section>
@endsection
