@extends('customer')

@section('content')
<section class="dashboard-area">
    <div class="container">
              <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
       @endif
       @endforeach
       </div> <!-- end .flash-message -->  
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                    <div id="trainingDetails">
                        <div class="workout-main col-sm-12">
                            <div class="workout-inr">
                @include('front.certifications.form')
                            </div>
                            
                        </div>
                    </div></div></div>
    </div>
</section>
@endsection

