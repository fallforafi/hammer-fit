<?php
    $required = 'required';
?>


                    
<form method="post" action="<?php echo url('certifications/store');?>" enctype="multipart/form-data">
                        <div class="profile__desc0 col-sm-12">
                            <h3>Title:</h3>
                            <input type="text" name="title" class="form-control" placeholder="Title *" required="required">
                            @if ($errors->has('title'))
                                              <span class="help-block">
                                              <strong>{{ $errors->first('title') }}</strong>
                                              </span>
                                              @endif
                            
                        </div>
                        <div class="profile__desc0 col-sm-12">
                            <h3>Description:</h3>
                            <textarea name="description" class="form-control" placeholder="Description *" required="required" maxlength="200"></textarea>
                            @if ($errors->has('description'))
                                              <span class="help-block">
                                              <strong>{{ $errors->first('description') }}</strong>
                                              </span>
                                              @endif
                           
                        </div>
                    <div class="profile__desc0 col-sm-3">
                        <h3>Year:</h3>
                        <div class="form-group fnc-select">
                        {!! Form::selectRange('year',2016,1930,null,['class' => 'form-control',$required])!!}
                        @if ($errors->has('ques_3'))
                                              <span class="help-block">
                                              <strong>{{ $errors->first('ques_3') }}</strong>
                                              </span>
                                              @endif
                        </div>
                    </div>  
					<div class="clearfix"></div>
					
                    <div class="profile_desc col-sm-6">
                        <div class="form-img"><label id="blah"></label></div> 
                        
                        <div class="fit__sub__reset clrlis">    
                        <button type="button" class="fit__submit" id="upfile2" name="image" style="cursor: pointer;">Add File *</button>
                        <input type="file" id="file2" name="image" style="display: none">
                        <label>Pdf, Jpeg, Png</label>
                        @if ($errors->has('image'))
                                              <span class="help-block">
                                              <strong>{{ $errors->first('image') }}</strong>
                                              </span>
                                              @endif
                        </div>  
                    </div>
  
                    <div class="col-sm-12 fit__sub__reset clrlist">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"><br>
                    <button type="submit" class="fit__submit">Save</button>
                    <button type="button" class="fit__reset" onclick="back()">Back</button>
                    </div>
                    <br>

                    </form>
                  
  

<script type="text/javascript">
jQuery('#file2').bind("change", function() {

  var imgVal = $('#file2').val();
  if (imgVal != '') {
    jQuery("#upfile2").text("File Added");
  } else {
    alert("Please add photo");
  }
  return false;
});
</script>
<script type="text/javascript">
$("#upfile2").click(function () {
$("#file2").trigger('click');
});
</script>
