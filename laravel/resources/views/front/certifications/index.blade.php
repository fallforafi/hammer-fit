@extends('customer')


@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
       @endif
       @endforeach
       </div> <!-- end .flash-message -->
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        
        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="trainingDetails">
                    <div class="workout-main col-sm-12">
                        <div class="workout-inr">
                            <div class="hed">
                            
                                <a href="{{url('certifications/create')}}" class="btn btn-info btn-flat pull-right"><i class="fa fa-pencil"></i>Add New</a>
                            <br>
                            <h2>UPLOADED <span>Awards/Certifications</span></h2>
                            </div>
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Title</th>
        <th>Description</th>
        <th>Year</th>
        <th></th>
        <th></th>
        
      </tr>
    </thead>
    <tbody>
        <?php $i = 1;?>
    @foreach($certifications as $certificate)
      <tr>
          <td><a href="certifications/edit/{{ $certificate->id }}"><?php echo $i; $i++; ?></a></td>
          <td><a href="certifications/edit/{{ $certificate->id }}">{{ $certificate->title }}</a></td>
        <td>{{ $certificate->description }}</td>
        
        <td>{{ $certificate->year }}</td>
        <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal-{{ $certificate->id }}"><i class="fa fa-trash"></i></button></td>
            <div class="modal fade" id="myModal-{{ $certificate->id }}">
				<div class="modal-dialog">
				<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" style="color: red">Alert</h4>
				</div>
				<div class="modal-body">
				<h3>Are you sure do you want to delete this?</h3>
				</div>
				<div class="modal-footer">
				<a class="btn btn-danger pull-left" href="certifications/delete/<?php echo $certificate->id ?>">Yes</a>

				<button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
				</div>
				</div>
				<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
<!--            <a href="certifications/delete/{{ $certificate->id }}" class="btn btn-danger"><i class="fa fa-remove" aria-hidden="true"></i> Delete</a>-->
                                <td>
        <a href="{{ url('uploads/users/'.$certificate->file) }}" download="{{$certificate->file}}" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i></a></td>
        
      </tr>
    @endforeach
    </tbody>
  </table>
  </div>
                        </div>
                    </div>
                    </div></div></div></div>
                        </section>                       
@endsection                        

