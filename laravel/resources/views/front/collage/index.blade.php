<!DOCTYPE html>
<html>
    <head>
        <link href="<?php echo asset('front/css/'); ?>/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo asset('front/css/'); ?>/style.css" rel="stylesheet">

        <style>
.thumbnail {
    height: 350px;
    width: 350px;
    text-align: center;
}
.thumbnail img {
    max-height: 100%;
    max-width: 100%;
    height: 100%;
    width: 100%;
}


            body {
                background-color: #2d2923;
                color:#fff;
            }

            body label {
                color: #fff;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <table class="table col-sm-12">
                <tbody>
                    <tr >
                        <td >
                            <div style="width:100px;">
                                <img src="<?php echo asset('front/images/logo.png'); ?>" /> 
                            </div>
                        </td>
                        <td class="text-center">
                            <h3>Photo Collage</h3></td>
                        <td class="text-right" colspan="3">
                            <h3>
                                <div class="view-btn pull-right">
                                    <a class="btn btn-primary" href="{{ URL::previous()}}">Back</a>
                                </div></h3>
                        </td>
                    </tr>
                    <tr>
                        @if(isset($first['front']))
                        <td>
                <center><label >Front Picture</label></center>

                <div class="thumbnail">
                    <img src="<?php echo asset('uploads/users/results/front_images/thumbnail/' . $first['front']); ?>">   
                </div>

                </td>
                @endif
                @if(isset($first['side']))
                <td>
                <center><label>Side Picture</label></center>

                <div class="thumbnail">
                    <img src="<?php echo asset('uploads/users/results/side_images/thumbnail/' . $first['side']); ?>">
                </div>

                </td>
                @endif
                @if(isset($first['back']))
                <td>
                <center><label>Back Picture</label></center>

                <div class="thumbnail">
                    <img src="<?php echo asset('uploads/users/results/back_images/thumbnail/' . $first['back']); ?>">
                </div>

                </td>
                @endif
                </tr>

                <tr>
                    @if(isset($second['front']))
                    <td>
                <center><label>Front Picture</label></center>


                <div class="thumbnail">

                    <img src="<?php echo asset('uploads/users/results/front_images/thumbnail/' . $second['front']); ?>">   
                </div>


                </td>
                @endif
                @if(isset($second['side']))
                <td>
                <center><label>Side Picture</label></center>

                <div class="thumbnail">
                    <img src="<?php echo asset('uploads/users/results/side_images/thumbnail/' . $second['side']); ?>">
                </div>


                </td>
                @endif
                @if(isset($second['back']))
                <td>
                <center><label>Back Picture</label></center>

                <div class="thumbnail">

                    <img src="<?php echo asset('uploads/users/results/back_images/thumbnail/' . $second['back']); ?>">  
                </div>

                </td>
                @endif
                </tr>
                </tbody>
            </table>
            <br><br>

        </div>
    </body>
</html>
