<ul class="nav nav-pills nav-stacked">
    <?php
    $role = Auth::user()->role->role;
    ?>
    @if(Auth::user()->image != '')
    <li class="nav_profile-img "><a class="img-cntr0" href="#_"><img src="{{ asset('/uploads/users/thumbnail/')}}/<?php echo Auth::user()->image; ?>" /></a></li>
    @else
    <li class="nav_profile-img "><a class="img-cntr" href="#_"><img src="{{asset('')}}/front/images/no_result.jpg" /></a></li>
    @endif
    <li class=""><a href="{{ url('profile') }}"><i class="fa fa-user"></i>Personal</a></li>
    <li class=""><a href="{{ url('address') }}"><i class="fa fa-location-arrow"></i>Address</a></li>
    @include('front/common/left_'.$role)
    <!--
        <li class=""><a href="{{ url('training-details') }}">Training Details</a></li>
        <li class=""><a href="{{ url('calculators') }}">Calculators</a></li>
    -->
    @if(Auth::user()->role->role == 'user' || Auth::user()->role->role == 'athlete')
    <li class=""><a href="{{ url('membership') }}"><i class="fa fa-money"></i> Membership Details</a></li>
    @endif
    <li ><a href="{{ url('myorders')}}" ><i class="fa fa-shopping-bag"></i>My Orders</a></li>
    <li><a href="{{ url('changepassword')}}" ><i class="fa fa-key"></i>Change Password</a></li>
    <li><a href="{{ url('auth/logout')}}"><i class="fa fa-sign-out"></i>Log Out</a></li>
</ul>