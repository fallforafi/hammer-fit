<li><a href="{{ url('profile-picture')}}" ><i class="fa fa-picture-o"></i> Profile Picture</a></li>
<li><a href="{{ url('shows')}}" ><i class="fa fa-eye"></i> Shows</a></li>
<li class=""><a href="{{ url('professional') }}"><i class="fa fa-picture-o"></i> Professional</a></li>
<li class=""><a href="{{ url('subscriptions') }}"><i class="fa fa-envelope"></i> Subscriptions</a></li>
<li><a href="{{ url('results')}}" ><i class="fa fa-list"></i> Results</a></li>
<li class=""><a href="{{ url('user/'.Auth::user()->id) }}"><i class="fa fa-user"></i> Public Profile</a></li>
