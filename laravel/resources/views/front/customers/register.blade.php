<?php
$required = 'required';
?>

@if (count($errors->register) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->register->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (Session::has('success'))
<div class="alert alert-success">
    <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
</div>
@endif

<div class="placelabeler ">

    <div class="row fnc-fom">
        <div class="form-group col-sm-4">
            <input type="text" class="form-control" name="firstName" id="firstName" placeholder="First Name"  required="required" value="{{ old('firstName') }}">
            <label>First Name</label>
        </div>
        <div class="form-group col-sm-4">
            <input type="text" class="form-control" name="middleName" id="midName" placeholder="Middle Name" value="{{ old('middleName') }}">
            <label>Middle Name</label>
        </div>
        <div class="form-group col-sm-4">
            <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Last Name" required="required" value="{{ old('lastName') }}">
            <label>Last Name</label>
        </div>
    </div>

    <div class="row fnc-fom">
        <div class="form-group col-sm-4">
            <input type="email" class="form-control" name="email" id="email" placeholder="Email Address *"  required="required" value="{{ old('email') }}">
            <label>Email</label>
        </div>
        <div class="form-group col-sm-4">
            <input type="password" class="form-control" name="password" id="password" placeholder="Password *" required="required">
            <label>Password</label>
        </div>
        <div class="form-group col-sm-4">
            <input type="password" class="form-control" data-match-error="Whoops, these don't match" data-match="#password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password *" required="required">
            <label>Confirm Password</label>
        </div>
    </div>

    <div class="row mt10">    
        <div class="ml15">
            {!! Form::label('gender', 'Date of birth *') !!} 
        </div>
        <div class="fnc-fom form-group col-sm-2">
            <select class="form-control" name="day" id="day" required="required">
                <option value="">Day</option>
                @foreach($days as $day)
                <option value="{{ $day }}">{{ $day }}</option> 
                @endforeach
            </select>
        </div>
        <div class="fnc-fom form-group col-sm-2">
            <select class="form-control" name="month" id="month" required="required">
                <option value="">Month</option>
                @foreach($months as $month)
                <option value="{{ $month }}">{{ $month }}</option> 
                @endforeach
            </select>
        </div>
        <div class="fnc-fom form-group col-sm-2">
            <select class="form-control" name="year" id="year" required="required">
                <option value="">Year</option>
                @foreach($years as $year)
                <option value="{{ $year }}">{{ $year }}</option> 
                @endforeach
            </select>
        </div>

        <div class="form__date--label form-group col-sm-4 date0">
            {!! Form::label('gender', 'Gender *') !!}
            <label class="radio-inline">
                {!! Form::radio('gender', 'm',null, array('id'=>'gender-m', $required)) !!} Male

            </label>
            <label class="radio-inline">
                {!! Form::radio('gender', 'f', null, array('id'=>'gender-f', $required)) !!} Female
            </label>   
        </div>        
    </div>
    <div class="row fnc-fom">
        <div class="form-group col-sm-4">
            <select class="form-control" name="country" id="country" required="required">
                <option value="">Select Country</option>
                @foreach($countries as $country)
                <option value="{{ $country->id }}">{{ $country->name }}</option> 
                @endforeach
            </select>
        </div>
        <div class="form-group col-sm-4">
            <select class="form-control" name="state" id="state" required="required">
                <option value=''>Select country first</option> 
            </select>
        </div>    

        <div class="form-group col-sm-4">
            <input type="text" class="form-control" name="city" id="city" placeholder="City" value="{{ old('city') }}">
            <label>City</label>
        </div>
    </div>
    <div class="row fnc-fom">
        <div class="form-group col-sm-6">
            <input type="text" class="form-control" name="address" id="address" placeholder="Your Address Line 1 *" required="required" value="{{ old('address') }}">
            <label>Address</label>
        </div>

        <div class="form-group col-sm-6">
            <input type="text" class="form-control" name="address2" id="address" placeholder="Your Address Line 2 *" value="{{ old('address2') }}">
            <label>Address 2</label>
        </div>
    </div>

    <div class="row fnc-fom">
        <div class="form-group fnc-select col-sm-6">
            <input type="text" class="form-control" name="zip" id="zip" placeholder="Zip Code" value="{{ old('zip') }}">
            <label>Zip Code</label>
        </div>
        <div class="form-group col-sm-6">
            <input type="text" name="phone" class="form-control" id="phone" placeholder="Phone *" value="{{ old('phone') }}">
            <label>Phone</label>
        </div>
    </div>
    @if($role_id == 4)
    <div class="row fnc-fom">
        <div class="fom col-sm-6">
            <label></label>
            <select class="form-control" name="coach_id">
                <option value="">Select Coach Here</option>
                @foreach($getAmbassador as $row)
                <option value="{{$row->id}}">{{$row->firstName}} {{$row->lastName}}</option>
                @endforeach
            </select>
        </div><br>
    </div>
    @endif


    @if($role_id == 3)
    <div class="row">
        <div class="form-group fnc-select col-sm-6">
            <select class="form-control" name="specialities">
                <option value="" selected="selected">Specialities *</option>
                @foreach($specialities as $speciality)
                <option value="{{ $speciality->id }}">{{ $speciality->title }}</option>
                @endforeach
            </select>
        </div>
    </div>
    @endif

    <div class="row fnc-fom">
        <div class="form-group col-sm-3">
            <select class="form-control" name="height" required="required">
                <option value="" selected="selected">Height *</option>
                <option value="4'10''">4'10"</option>
                <option value="4'11''">4'11"</option>
                <option value="5'0''">5'0"</option>
                <option value="5'1''">5'1"</option>
                <option value="5'2''">5'2"</option>
                <option value="5'3''">5'3"</option>
                <option value="5'4''">5'4"</option>
                <option value="5'5''">5'5"</option>
                <option value="5'6''">5'6"</option>
                <option value="5'7''">5'7"</option>
                <option value="5'8''">5'8"</option>
                <option value="5'9''">5'9"</option>
                <option value="5'10''">5'10"</option>
                <option value="5'11''">5'11"</option>
                <option value="6'0''">6'0"</option>
                <option value="6'1''">6'1"</option>
                <option value="6'2''">6'2"</option>
                <option value="6'3''">6'3"</option>
                <option value="6'4''">6'4"</option>
                <option value="6'5''">6'5"</option>
                <option value="6'6''">6'6"</option>
                <option value="6'7''">6'7"</option>
                <option value="6'8''">6'8"</option>
                <option value="6'9''">6'9"</option>
                <option value="6'10''">6'10"</option>
            </select>
        </div>
        <div class="form-group col-sm-3">
            <input type="number" class="form-control" name="weight" id="weight" placeholder="Weight *" required="required" value="{{ old('weight') }}">
            <label>Weight</label>
        </div>
        @if(!($role_id == 4))

        <div class="form-group col-sm-4">
            <input type="text" class="form-control" name="medicalConcerns" id="medicalConcerns" placeholder="Medical Concerns" required="required" value="{{ old('medicalConcerns') }}">
            <label>Medical Concerns</label>
        </div>
        <div class="form-group col-sm-4">
            <input type="text" class="form-control" name="goals" id="goals" placeholder="Goals" required="required" value="{{ old('goals') }}">
            <label>Goals</label>
        </div>
        @endif
    </div>

    @if(!($role_id == 4))
    <div class="row">
        <div class="form-group0 col-sm-12">
            <h4>Are you a competitor?</h4>
            <input class="pul-lft mr15" type="checkbox" name="isCompetitor" id="isCompetitor" /><span> &nbsp; Yes</span>
        </div>

    </div>

    <div class="row" id="div_federation" style="display:none;">
        <div class="form-group col-sm-6" >
            <input type="text" class="form-control federation" name="federation" id="federation" placeholder="If so, which Federation?">
        </div>
    </div>
    @endif

    <div class="row">
        <div class="form-group col-sm-12 p0">  
            @if(!($role_id == 4))
            <br>
            @include('front/common/terms')
            <br>
            @endif   
            <br>
            @include('front/common/agreement')
        </div>
    </div>

    <div class="reg__photos text-center col-sm-12 p0 clrlist">
        <button type="submit" class="btn btn-default reg__submit__btn" id="register_button">Register</button>
    </div>

</div>

<script>
    $("#isCompetitor").change(function () {
        //  alert(this.checked);
        if (this.checked) {
            //Do stuff
            $('#div_federation').show();
            $('.federation').addClass('required');
        } else {
            $('#div_federation').hide();
            $('.federation').removeClass('required');
        }
    });
    $('.register').submit(function (event) {

        var form = $('#register');
        form.find('#register_button').prop('disabled', true);
        $('.terms-errors').hide();
        var term = check_terms_services();
        if (term === false) {
            return false;
        }

        $('.agreement-errors').hide();
        var agreement = check_agreement_services();
        if (agreement === false) {
            return false;
        }

    });</script>
<script>
    jQuery('[data-provide="datepicker"]').datepicker({
        format: "dd-mm-yyyy"
    }).on('change', function () {
        jQuery('.datepicker').hide();
    });</script>
<script>
    $('#country').on('change', function () {
        $.ajax({
            type: "GET",
            url: "<?php echo url('/state/get/'); ?>/" + this.value,
            data: "",
            async: true
        }).success(function (val) {
            var response = JSON.parse(val);
            if (response.length > 0) {
                var html = "<option value=''>Select your state</option>";
                for (key in response) {
                    html += "<option value='" + response[key].id + "'>" + response[key].name + "</option>";
                }
            } else {
                html += "<option value=''>Select country first</option> ";
            }
            $('#state').html(html);
        });
    });
//    $('#state').on('change', function () {
//        $.ajax({
//            type: "GET",
//            url: "<?php echo url('/city/get/'); ?>/" + this.value,
//            data: "",
//            async: true
//        }).success(function (val) {
//            var response = JSON.parse(val);
//            if (response.length > 0) {
//                var html1 = "<option value=''>Select your city</option>";
//                for (key in response) {
//                    html1 += "<option value='" + response[key].id + "'>" + response[key].name + "</option>";
//                }
//            } else {
//                html1 += "<option value=''>Select state first</option> ";
//            }
//            $('#city').html(html1);
//        });
//    });
</script>  