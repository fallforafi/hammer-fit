@extends('layout')
<?php
$title = 'Home';
$description = '';
$keywords = '';
?>
@include('front/common/meta')
@section('content')
<style>
    .trainer-box{
        cursor: pointer;
    }
</style>

<section class="slider-area no-ctrl"  >
    <div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">

        <ol class="carousel-indicators thumbs">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

            <div class="item active bg-cvr" style="background-image:url('{{asset('')}}/front/images/slide1.jpg');">
                <div class="caro-caps0">
                    <div class="container">
                        <div class="slide__cont col-sm-5 p0 pul-cntr">
                            <span>REVOLUTIONARY TECHNIQUE</span>
                            <h1>THE VERY BEST FITNESS COACHES</h1>
                            <h2>MAXIMIZE YOUR RESULTS</h2>
                            <p>At Hammer Fit, we believe in one thing 
                                above all, the very best in customer 
                                satisfaction and experience_ Our 
                                basic premise is simple, work harder, 
                                get SUPERIOR results. WE DO NOT 
                                SUPPORT STARVATION 
                                TECHNIQUES</p>
                            <div class="lnk-btn inline-block more-btn member-btn"><a href="signup">BECOME A MEMBER</a></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>


    </div>
</section>

<section class="trainee-area">
    <div class="container">

        <div class="hed">
            <h2>MEET OUR <span>Hammer Fit</span> TEAM</h2>
            <p>Top class Fitness Coaches of the World.</p>
        </div>

        @if(count($model) > 0)
        <div class="swiper-container s1">

            <div class="swiper-wrapper">

                @foreach($model as $user)

                <div class="swiper-slide trainer-box" onclick="changePage(<?php echo $user->id; ?>);">
                    <div class="trainer__img img-cntr">
						
                        @if($user->image == NULL)
                        <img src="{{ url('front/images/no_result.jpg')}}" alt="User Avatar">
                        @else
                        <img src="{{ url('uploads/users/'.$user->image) }}" alt="User Avatar">
                        @endif
						
						@if($user->availability == 0)
							<span class="badge trainer__tag">
								Not Available							
							</span>
						@endif
						
						
                    </div>

                    <div class="trainer__cont">
                        <h3>{{$user->firstName}} {{$user->lastName}}</h3>
                        <h4>@foreach($specialities as $speciality)
                            @if($user->speciality_id == $speciality->id)
                            {{ $speciality->title }}
                            @endif
                            @endforeach
                        </h4>

                        <ul>
                            <li>State: @foreach($states as $state)
                                @if($user->state == $state->id)  
                                {{ $state->name}}
                                @endif
                                @endforeach
                            </li>
                            <li>Experience: @if($user->experience !== Null){{$user->experience}}+ years @else Not added yet @endif</li>
                        </ul>
                    </div>					
                </div>
                @endforeach             
            </div>
        </div>

        <!-- Add Arrows -->
        <div class="swiper-button-next swiper-button-next1"><i class="fa fa-angle-right"></i></div>
        <div class="swiper-button-prev swiper-button-prev1"><i class="fa fa-angle-left"></i></div>
        @else 
        <br><br>
        <center><h3>Sorry, there is no results for your search</h3></center>
        @endif

    </div>
</section>


<section class="product-area scale--hover">
    <div class="container">
        <div class="hed">
            <h2>OUR BEST <span>PRODUCTS</span></h2>
            <p>See our products for your best health.</p>
        </div>
        @include('front.products.list')
        <div class="clearfix"></div>
        <div class="lnk-btn inline-block view-btn"><a href="products"><i class="fa fa-eye"></i> View All</a></div>

    </div>
</section>

<section class="stories-area container-split-20">

    <div class="stories__cont   text-left bg-cvr p0" style="background-image:url('{{asset('')}}/front/images/stories-bg.jpg')" >
        <div class="container">
            <div id="carousel-stories" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators ">
                    <li data-target="#carousel-stories" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-stories" data-slide-to="1"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="stories__cont col-sm-6">
                            <h2>Matt Grenoble <span>28 years old</span></h2>
                            <h4>January 1, 2017 to May 26, 2017</h4>
                            <blockquote>"...I started training January 2nd, 2017 and have not thought once about stopping. The training regimen is intense but it is what it takes to carve my body into the titan i want to be. The diet never leaves me hungry, in fact its alot of food and i stay full all the time.  Due to my transformation i am now getting questions as to whether i am taking any steroids, which i am not. Its all in the Hammerfit program! I wish i would have contacted Mark sooner!"</blockquote>

                        </div>

                        <div class="stories__img col-sm-6">

                            <div class="stories__before col-sm-6"><img src="{{asset('')}}/front/images/stories-before.jpg" alt="" />

                                <span class="btn btn-primary">Before</span>
                            </div>
                            <div class="stories__after col-sm-6"><img src="{{asset('')}}/front/images/stories-after.jpg" alt="" />

                                <span class="btn btn-primary">After</span>
                            </div>

                        </div>

                    </div> 
					<div class="item">
                        <div class="stories__cont col-sm-6">
                            <h2>Hannah Whittaker <span> </span></h2>
                            <h4>January 1, 2017 to May 26, 2017</h4>
                            <blockquote>"I started my fitness journey back in 2012,living a healthy lifestyle and going to the gym 5/6 days a week. Over the past couple of years, there came a time In my journey where I felt very complacent and not progressing as much as I had hoped for. I decided to compete in a bodybuilding show back in 2015 and was with a coach who was anything but helpful. I went down a wrong path that, and had no sense of direction after the whole thing was over. After months of mending my relationship with proper food and training. This past competition season, I knew I was ready to take my physique to the next level. Finding Mark Hummel and Team Hammerfit led me in that direction. Not only completely  transforming my physique, but having the correct knowledge to maintain a body I am happy with! At no point was I ever starving or felt deprived in anyway. It's very simple, train HARD stick to the meal plan specific for YOU and YOUR goals, and the results will follow!"</blockquote>

                        </div>

                        <div class="stories__img col-sm-6">

                            <div class="stories__before col-sm-6"><img src="{{asset('')}}/front/images/stories-before2.jpg" alt="" />

                                <span class="btn btn-primary">Before</span>
                            </div>
                            <div class="stories__after col-sm-6"><img src="{{asset('')}}/front/images/stories-after2.jpg" alt="" />

                                <span class="btn btn-primary">After</span>
                            </div>

                        </div>

                    </div> 
					
					

                    <!--div class="item">
                            <div class="stories__cont col-sm-6">
    <h2>SUCCESS STORIES AT <span>HAMMER FIT</span></h2>
    <h4>January 1, 2017 to May 26, 2017</h4>
    <blockquote>"...I started training January 2nd, 2017 and have not thought once about stopping. The training regimen is intense but it is what it takes to carve my body into the titan i want to be. The diet never leaves me hungry, in fact its alot of food and i stay full all the time.  Due to my transformation i am now getting questions as to whether i am taking any steroids, which i am not. Its all in the Hammerfit program! I wish i would have contacted Mark sooner!"</blockquote>
                            </div>
                            <div class="stories__img col-sm-6">
                                    
                                            <div class="stories__before col-sm-6"><img src="{{asset('')}}/front/images/stories-before.jpg" alt="" /></div>
                                            <div class="stories__after col-sm-6"><img src="{{asset('')}}/front/images/stories-after.jpg" alt="" /></div>
                                    
                            </div>
</div--> 

                </div>

            </div>
        </div>
    </div>
    <!--div class="stories__img col-sm-6 text-right bg-cvr p0" style="background-image:url('{{asset('')}}/front/images/stories.jpg')" >
        <div class="container">

        </div>
    </div-->


</section>


<section class="product-area apparel-area scale--hover">
    <div class="container">

        <div class="hed">
            <h2>HAMMER FIT <span>APPAREL</span></h2>
            <p>See our products for your best health.</p>
        </div>
        <?php
        $products = $apparels;
        ?>
        @include('front.products.list')
        <div class="clearfix"></div>


        <div class="lnk-btn view-btn"><a href="apparels"><i class="fa fa-eye"></i> View All</a></div>

    </div>
</section>

@include('front/common/newsletters')
<script type="text/javascript">
    function changePage(id)
    {
        window.location = '<?php echo url('user');?>'+'/'+ id;
    }
</script>
<script type="text/javascript">

    $(document).ready(function () {
        $('body').addClass('home-page');
    });

    // function changePage(id) 
    // {
    //   alert(id);
    //  //window.location.href = "<?php echo url('user'); ?>/"+id;
    // }

</script>

<script type="text/javascript">

    $(function () {
        $("#cartIcon1").attr("src", "{{asset('')}}/front/images/cart-icon.png");
        $("#cartIcon2").attr("src", "{{asset('')}}/front/images/cart-icon2.png");
    });

</script>

<script type="text/javascript">

    $(function () {
        $("#hdr-icon1").attr("src", "{{asset('')}}/front/images/hdricon1.png");
        $("#hdr-icon2").attr("src", "{{asset('')}}/front/images/hdricon2.png");
    });

</script>

<?php // echo $model->body;?>
@endsection
