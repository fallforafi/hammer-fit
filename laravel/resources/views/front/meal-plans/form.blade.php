<?php
$required = 'required';
?>


<form method="post" action="<?php echo url('meal-plans/store'); ?>" enctype="multipart/form-data">
    <div class="profile__desc">
        <h3>Title:</h3>
        <input type="text" name="title" class="form-control" placeholder="Title *" required="required">


    </div>
    <div class="profile__desc">
        <h3>Description:</h3>
        <textarea name="description" class="form-control" placeholder="Description *" required="required" maxlength="200"></textarea>


    </div>

    <div class="profile_desc col-sm-6">
        <div class="form-img"><label id="blah"></label></div> 
        <div class="fit__sub__reset clrlis">    
            <button type="button" class="fit__submit" id="upfile2" name="file" style="cursor: pointer;">Add File *</button>
            <input type="file" id="file2" name="file" style="display: none">
            <label>Pdf Only</label>

        </div>  
    </div>

    <div class="col-sm-12 fit__sub__reset clrlist">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"><br>
        <button type="submit" class="fit__submit">Save</button>
        <button type="button" class="fit__reset" onclick="back()">Back</button>
    </div>
    <br>

</form>



<script type="text/javascript">
    jQuery('#file2').bind("change", function () {

        var imgVal = $('#file2').val();
        if (imgVal !== '') {
            jQuery("#upfile2").text("File Added");
        } else {
            alert("Please add file");
        }
        return false;
    });
</script>
<script type="text/javascript">
    $("#upfile2").click(function () {
        $("#file2").trigger('click');
    });
</script>
