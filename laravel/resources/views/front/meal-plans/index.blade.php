@extends('customer')


@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>

        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="trainingDetails">
                    <div class="workout-main col-sm-12">
                        <div class="workout-inr">
                            <div class="flash-message">
                                @if (count($errors->register) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->register->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success">
                                    <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
                                </div>
                                @endif
                            </div>
                            <div class="hed">
                                <h2>UPLOADED <span>Meal Plans</span></h2>
                            </div>
                            <div class="table-responsive">    
                                <div class="row">
                                    <div class="col-sm-12">
                                        <a href="{{url('meal-plans/create')}}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add</a>
                                    </div>
                                </div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Uploaded Date</th>
                                            <th>Download File</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($mealPlans as $row)
                                        <tr>
                                            <td><?php
                                                    echo $i;
                                                    $i++;
                                                    ?></td>
                                            <td>{{ $row->title }}</td>
                                            <td>{{ date('d/m/Y', strtotime($row->created_at)) }}</td>

                                            <td><a href="{{ url('uploads/users/meal-plans/'.$row->file) }}" download="{{$row->file}}" class="btn btn-primary"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                                            <td>
                                                <a href="{{ url('meal-plans/edit/'.$row->id) }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-danger" data-toggle="modal" href="#myModal-{{ $row->id }}"><i class="fa fa-trash"></i></button></td> 
                                    <div class="modal fade" id="myModal-{{ $row->id }}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" style="color: red">Alert</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h3>Are you sure do you want to delete this?</h3>
                                                </div>
                                                <div class="modal-footer">
                                                    <a class="btn btn-danger pull-left" href="meal-plans/delete/<?php echo $row->id ?>">Yes</a>

                                                    <button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div></div></div></div>
</section>                       
@endsection                        

