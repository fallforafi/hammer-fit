@extends('customer')
<?php
$required = 'required';
$currency = Config::get('params.currency');
$per_month = Config::get('params.per_month');
?> 

@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                {{ Session::get('alert-' . $msg) }}
            </div>
            @endif
            @endforeach
        </div> <!-- end .flash-message --> 
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>

        <div class="dash__rgt col-sm-9">
            @include('front/common/errors')
            <div class="tab-content">
                @if(count($membershipCancel) > 0)
                <section class="account-area">
                    <div class="container">
                        {!! Form::open(array( 'class' => 'form','url' => url('membership/rejoin'), 'name' => 'checkout')) !!}    
                        <div class="hed">
                            <h2>Become our athlete again</h2><br>
                            <p>Below are the Membership Fees (Charged monthly on Date of Renewal until cancelled)</p>
                            <?php
                            if (count($charges) > 0) {
                                foreach ($charges as $value) {
                                    ?>
                                    <label><input type="radio" name="type" required="required" value="<?php echo $value->type; ?>"> <?php echo $value->name; ?>  : <?php echo $currency[Config::get('params.currency_default')]['symbol'] ?><?php echo $value->rate; ?></label>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="acct__login acct__reg acct__inr__title col-sm-12">

                            @include('front/common/cc_form')

                            <div class="payment_method clrlist text-center">
                                <ul>
                                    <li>
                                        Managed by: 
                                    </li>
                                    <li>
                                        <span class='paypalicon'><img src="{{ asset('front/images/paypal-logo.png') }}"></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="form-group text-center">
                                <span><p><strong>Note:</strong> This form submission could take <b>20 seconds</b> or longer, so keep patience!</p></span>
                                <button class="btn btn-lg btn-primary" type="submit">Rejoin Now</button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </section>
                @else
                <ul class="nav nav-tabs nav-justified mem__pkg">
                    <li class="active"><a data-toggle="tab" href="#cancel">Membership Cancel</a></li>
                    <?php if (isset($currentSubscriptions)) { ?>
                        <li><a data-toggle="tab" href="#change">Change Package</a></li>
                    <?php } ?>
                    <li><a data-toggle="tab" href="#update">Update Credit Card</a></li>
                </ul>
                <br>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="cancel">
                        <div class="workout-inr">
                            <div class="col-sm-12">
                                <div class="well">
                                    <blockquote>The only valid method for cancelling your Subscription is via the cancellation button provided below.
                                        Requests to cancel by e-mail or phone are not considered, and do not accomplish, membership cancellation.</blockquote>
                                </div>
                            </div>
                            <div class="col-sm-4 col-sm-offset-4">
                                <a href="#myModal" data-toggle="modal" class="btn btn-primary btn-block btn-lg"> Membership Cancel</a>
                                <br>
                            </div>
                            <div class="col-sm-12">
                                <div class="alert alert-warning">
                                    <p><span>Note: </span><strong>Once you cancel your membership, you will lose access as "Athlete" immediately.</strong></p>
                                </div>
                            </div>
                            <div class="col-sm-12 table-responsive">          
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>Rejoin</th>
                                            <th>Date Time</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($subscriptions as $row)
                                        <tr>
                                            <td><?php
                                                echo $i;
                                                $i++;
                                                ?></td>
                                            <td>{{ $row->type }}</td>
                                            @if($row->rejoin == 1)
                                            <td><span class="green">Yes</span></td>
                                            @else
                                            <td>No</td>
                                            @endif

                                            <td>{{ date('d/m/Y h:i:s',strtotime($row->created_at)) }}</td>
                                            <td><?php echo $currency[Config::get('params.currency_default')]['symbol'] ?> {{ $row->amount }}</td>
                                            @if($row->status == 1)
                                            <td>Current</td>
                                            @else
                                            <td>Previous/Old</td>
                                            @endif

                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                            <div class="modal fade" id="myModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" style="color: red">Alert</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h3>Are you sure you want to cancel your membership?</h3>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="btn btn-danger pull-left" href="{{url('membership/cancel')}}">Yes</a>

                                            <button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <?php if (isset($currentSubscriptions)) { ?>
                        <div role="tabpanel" class="tab-pane fade" id="change">
                            <section class="account-area">
                                <div class="container">
                                    <div class="hed">
                                        <h2>Current Package</h2>
                                        <p><?php echo ucwords($currentSubscriptions->name); ?> : <?php echo $currency[Config::get('params.currency_default')]['symbol'] ?><?php echo $currentSubscriptions->rate; ?></p>
                                    </div>
                                    <div class="acct__login acct__reg acct__inr__title col-sm-12">


                                        {!! Form::open(array( 'class' => 'form','url' => url('membership/update'))) !!}  
                                        <div class="hed">
                                            <h2>Update Package</h2><br>
                                            <div class="text-center update-pkg-label">
                                                <?php
                                                if (count($charges) > 0) {
                                                    foreach ($charges as $value) {
                                                        ?>
                                                        <label><input type="radio" name="type" required="required" value="<?php echo $value->type; ?>"> <?php echo $value->name; ?>  : <?php echo $currency[Config::get('params.currency_default')]['symbol'] ?><?php echo $value->rate; ?></label>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <br>

                                        <div class="payment_method clrlist text-center">
                                            <ul>
                                                <li>
                                                    Managed by: 
                                                </li>
                                                <li>
                                                    <span class='paypalicon'><img src="{{ asset('front/images/paypal-logo.png') }}"></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="form-group text-center">
                                            <span><p><strong>Note:</strong> This form submission could take <b>20 seconds</b> or longer, so keep patience!</p></span>
                                            <button class="btn btn-lg btn-primary" type="submit">Update Now</button>
                                        </div>

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </section>

                        </div>
                    <?php } ?>
                    <div role="tabpanel" class="tab-pane fade" id="update">
                        <section class="account-area pt20">
                            <div class="container">
                                <!--                                <div class="hed">
                                                                    <h2>Update your Credit Card</h2>
                                                                </div>-->
                                <div class="acct__login acct__reg acct__inr__title col-sm-12">


                                    {!! Form::open(array( 'class' => 'form','url' => url('save/credit-card'), 'name' => 'checkout')) !!}    

                                    @include('front/common/cc_form')


                                    <div class="payment_method clrlist text-center">
                                        <ul>
                                            <li>
                                                Managed by: 
                                            </li>
                                            <li>
                                                <span class='paypalicon'><img src="{{ asset('front/images/paypal-logo.png') }}"></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form-group text-center">
                                        <span><p><strong>Note:</strong> This form submission could take <b>20 seconds</b> or longer, so keep patience!</p></span>
                                        <button class="btn btn-lg btn-primary" type="submit">Update Now</button>
                                    </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </section>

                    </div>

                </div>
                @endif
            </div>

        </div>

    </div>
</section>     
<script>
    $('form').submit(function () {
        $(this).find('button[type=submit]').prop('disabled', true);
    });
</script>
@endsection                        

