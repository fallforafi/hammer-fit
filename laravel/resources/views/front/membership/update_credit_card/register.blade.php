@extends('layout')

@section('content')
<?php
$currency = Config::get('params.currency');
$required = 'required';
?>
<section class="account-area">
    <div class="container">
        <div class="hed">
            <h2>Update your Credit Card</h2>
        </div>
        <div class="acct__login acct__reg acct__inr__title col-sm-12">
            @include('front/common/errors')

            {!! Form::open(array( 'class' => 'form','url' => url('save/credit-card'), 'name' => 'checkout')) !!}    

            @include('front/common/cc_form')


            <div class="payment_method clrlist text-center">
                <ul>
                    <li>
                        Managed by: 
                    </li>
                    <li>
                        <span class='paypalicon'><img src="{{ asset('front/images/paypal-logo.png') }}"></span>
                    </li>
                </ul>
            </div>
            <div class="form-group text-center">
                <span><p><strong>Note:</strong> This form submission could take <b>20 seconds</b> or longer, so keep patience!</p></span>
                <button class="btn btn-lg btn-primary" type="submit">Update Now</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</section>
<script>
    $('form').submit(function () {
        $(this).find('button[type=submit]').prop('disabled', true);
    });
</script>
@endsection
