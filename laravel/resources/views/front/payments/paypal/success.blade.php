@extends('layout')

@section('content')

		<section class="payment-succces-area">
            <div class="container">
              <div class="title">{{ $content->title }}</div>  
              <div class="cont"> 
				{{ @strip_tags($content->body) }}
              </div>
            </div>
        </section>
				
@endsection