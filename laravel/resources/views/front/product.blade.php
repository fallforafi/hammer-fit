@extends('layout')

@section('content')
<?php
$currency = Config::get('params.currency');

if ($product->sale == 1 && $product->price > $product->salePrice) {
    $price = $product->salePrice;
} else {
    $price = $product->price;
}
?>

<div id="fh5co-programs-section" class="pt50">
    <div class="container">
        
		<div class="prod-detail-area col-sm-12 p0">
                <span class="main-head whole-sale-head prod-detail"><?php // echo $category->name;   ?></span>
                <div class="row">
                    <div class="prod-detail__img col-sm-6">
					
					
	<section class="slider-area thumb-indicat hover-ctrl fadeft" >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel">
		 

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner ">


			<div class="item active bg-cvr" style='background-image:url("{{ asset('uploads/products')}}/<?php echo $product->image; ?>"'); /> 
			</div>

			<?php
			 $i = 1;
			 foreach ($productImages as $image) {
             ?>
				<div class="item bg-cvr" style='background-image:url("{{ asset('uploads/products_images')}}/<?php echo $image->image; ?>"'); /> 
				</div>
				
            <?php
			$i++;
				}
            ?>
			
		  </div>

		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left fa fa-angle-left""></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right fa fa-angle-right"></span>
		  </a>
		  
		   <!-- Indicators -->
		  <span class="carousel-indicators indicators-arr prev">prev</span>
		  <span class="carousel-indicators indicators-arr next">Next</span>
		  
		  <ol class="carousel-indicators thumbs postatic">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="bg-cvr" style='background-image:url("{{ asset('uploads/products')}}/<?php echo $product->image; ?>'></li>
			 <?php
			 $i = 1;
			 foreach ($productImages as $image) {
             ?>
				<li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="val<?php echo $image->attribute_value_id ?>" class="bg-cvr" style='background-image:url("{{ asset('uploads/products_images/thumbnail')}}/<?php echo $image->image; ?>'> 
				</li>
				
            <?php
			$i++;
				}
            ?>
										
		  </ol>
		  
		  
		</div>
		
		
		<script>
			//jQuery.Deferred exception: jQuery(...).size is not a function TypeError: jQuery(...).size is not a function
			var pixels=0;
			
			jQuery(function() {

			var thumbmover = ".thumb-indicat .carousel-indicators li:nth-of-type(1)";

			var thumbCount = jQuery( ".thumb-indicat .carousel-indicators li" ).size(); // Counting li items
			 if (thumbCount<8){ jQuery(".indicators-arr").addClass("hidden"); } // hiden next prev with limitation

			  jQuery('.thumb-indicat .next').click(function () {
				pixels=pixels-80;      
				jQuery( thumbmover ).css('margin-left',pixels);
					//alert(pixels);
					if (pixels==-320){ pixels=0; }
				});
			  
			  jQuery('.thumb-indicat .prev').click(function () {
				pixels=pixels+80;      
				jQuery( thumbmover ).css('margin-left',pixels);
				//alert(pixels);
					if (pixels>=0){ pixels=0; }
				});
			});
		</script>

	</section>
	
	

                    </div>
                    <div class="prod-detail__cont col-sm-6">
                        <div class="prod-detail-cont">
                            <div class="title clrhm mb30">
								<h3><?php echo $product->name; ?></h3>
							</div>
							
                            <div class="prod-detail__price h4">{{$currency[Config::get('params.currency_default')]['symbol']}} @include('front/products/price')</div>
							
                            <form id="form_add_to_cart" class="prod-detail-form">
							  <div class="prod-detail__radio clrlist fnc-fom">
							  <ul>
                                <?php
                                if(!empty($availabeInColors)){
									foreach ($availabeInColors as $color) {
                                ?>
                                        <li>
											<input onclick="changeColor(), changePrice('attributes_14')" id="attributes_14_<?php echo $color->name; ?>" type="radio" name="attributes[14][]" class="color <?php echo $color->name; ?>" value="<?php echo $color->name; ?>_option_<?php echo $color->attribute_value_id; ?>_option_<?php echo $color->price; ?>" /><mark></mark>
                                            <label><?php echo $color->name; ?></label>
                                        </li>
                                        <?php
                                    }
                                }
								
                                echo "</ul></div><div class='clearfix'></div>";

                                foreach ($attributes as $attribute) {

                                    if ($attribute->type == 'dropdown') {
                                        echo "<div class='form-group col-sm-6 p0 mt20'><label>Select " . $attribute->name . "</label>";
                                        ?> 

                                        <select class="form-control" onchange="changePrice('attributes_<?php echo $attribute->attribute_id ?>');"  id="attributes_<?php echo $attribute->attribute_id ?>" name="attributes[<?php echo $attribute->attribute_id ?>][]" >
                                            <option >--</option>
                                            <?php
                                            $values = explode(',', $attribute->value_names);
                                            sort($values);
                                            $value_ids = explode(',', $attribute->value_id);
                                            $value_prices = explode(',', $attribute->value_price);
                                            while (list($key, $value) = each($values) and list($vkey, $value_id) = each($value_ids) and list($pkey, $value_price) = each($value_prices)) {
                                                ?>
                                                <option value="<?php echo $value; ?>_option_<?php echo $value_id; ?>_option_<?php echo $value_price; ?>" ><?php echo $value; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select></div>
                                        <?php
                                    } elseif ($attribute->type == 'textfield') {
                                        echo "<div class='form-group col-sm-6 p0 mt30'><label>Enter " . $attribute->name . "</label>";
                                        ?>
                                        <input value="" name="attributes[<?php echo $attribute->attribute_id ?>][]" id="attributes_<?php echo $attribute->attribute_id ?>"  />
											</div>
                                        <?php
                                    }
                                }
                                ?>
								<div class="clearfix"></div>
								
								<div class='form-group col-sm-6 p0 mt10'>
									<label>Quantity</label>
									<input value="1" name="quantity" id="quantity" class="form-control"  />
								</div>
								
								<div class="clearfix"></div>
								
								<div class='form-group col-sm-6 p0 mt10'>
									<input type="button" id="btn_add_to_cart" value="Add to Cart" class="btn btn-primary" />
								</div>
								
                                <input type="hidden" name="total_price"  id="total_price" value="<?php echo $price; ?>" />
                                <input type="hidden" name="price"  id="price" value="<?php echo $price; ?>" />
                                <input type="hidden" name="product_id"  id="product_id" value="<?php echo $id; ?>" />
                            </form>


                            <script>



                                $(document).ready(function () {
                                    $("#btn_add_to_cart").click(function () {

                                        var price = $('#total_price').val();
                                        var form = $('#form_add_to_cart').serialize();

                                        var jqxhr = $.get("../cart/add", form, function () {
                                            //alert( "Product added to cart." );
                                            window.location = "../cart/view";

                                        })
                                                .done(function () {
                                                    //alert( "second success" );
                                                })
                                                .fail(function () {
                                                    //alert( "error" );
                                                })
                                                .always(function () {
                                                    //alert( "finished" );
                                                });
                                    });


                                });

                                function changePrice(attribute_id)
                                {
                                    var value = $("#" + attribute_id).val();
                                    var price = $("#price").val();
                                    var form = $('#form_add_to_cart').serialize();
                                    //$("#description").html(total_price); 
                                    $.get("../cart/updateproductprice", form, function (response) {
                                        //alert( "Product added to cart." );
                                        //window.location="../cart/view";
                                        //alert(response);
                                        $("#total_price").val(response.total_price);
                                        $("#price").html(response.total_price);
                                        //$("#description").html(response.total_price);                  
                                    }, 'json')
                                }

                                var prev = 'slide_0';
                                function changeColor()
                                {
                                    var value = $("input:checked").val();
                                    var value = value.split("_");
                                    $("#slide_" + value[2]).trigger("click");
                                }
                            </script>
                        </div>
                    </div>
                </div>
				
				
				<ul class="nav nav-tabs mt30">
				  <li class="active"><a href="#prod_desc1" data-toggle="tab">Description</a></li>
				  <li><a href="#prod_desc2" data-toggle="tab">Profile</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content p15 ">
				  <div class="tab-pane active" id="prod_desc1">
				  
                <!--span class="descrp-head" id="description">Description</span-->
                <div class="detail-cont">
                    <!--span class="faq-top-head">Product Description</span-->
                    <span class="prod-detail-descrp"><?php echo $product->description; ?></span>
                </div>
				
				
				</div>
				  <div class="tab-pane " id="prod_desc2">...</div>
				</div>

				
                <?php
                $products = $relatedProducts;
                ?>

			<section class="similar-area">
				<div class="container">
					
					<div class="hed similar-prod">
						<h2>Similar <span>Products</span></h2>
					</div>
					
					<div class="cont">
						@include('front/products/list')
					</div>
					
				</div>
			</section>
			
            </div>
        
    </div>

</div>
@endsection


