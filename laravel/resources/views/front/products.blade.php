@extends('layout')
<?php
$title = $category->name;
$description = $category->teaser;
$keywords = $category->keywords;
?>
@include('front/common/meta')
@section('content')
@if($category!='')
<section class="inr-bnr-area">
    <div class="inr-bnr-img">
        <img src="{{asset('')}}uploads/categories/<?php echo $category->image ?>" alt="<?php echo $title ?>">
        <div class="container">
            <div class="inr-bnr-cont prod-bnr-cont col-sm-12 anime-left">
                <h2><?php echo $title ?></h2> 				
                <h4><?php echo $category->teaser; ?></h4>
            </div>
        </div>
    </div>
</section> 
@endif
<section class="product-area scale--hover">
    <div class="container">
        <div class="hed">
            <h2>OUR BEST <span><?php echo $title ?></span></h2>
            <p><?php echo $category->teaser; ?></p>
        </div>
        @include('front/products/list')
    </div>
</section>	
<div class="clearfix"></div>
<div class="lnk-btn inline-block view-btn"></div>
@include('front/common/newsletters')
@endsection