@extends('customer')
@section('content')
<?php
$required = 'required';
?>
<section class="dashboard-area">
    <div class="container">
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="dash__rgt col-sm-9">

            <div class="hed"><h2>professional</h2></div>

            <div id="profile">
                <div class="profile__dtl0 col-sm-12">
                    @include('front.common.errors')
                    <form method="post" action="<?php echo url('updateprofessional'); ?>">

                        <div class="form-group col-sm-12">
                            <label>About me:</label>
                            <textarea name="about" class="form-control">{{$user->about}}</textarea>
                        </div>

                        <div class="form-group col-sm-7">
                            <label>Goals:</label>
                            <textarea name="goals" class="form-control">{{$user->goals}}</textarea>
                        </div>
                        @if(Auth::user()->role->role == 'ambassador')
                        <div class="form-group col-sm-3">
                            <label>Specialities:</label>
                            <select class="form-control" name="specialities">
                                <option value="">Specialities *</option>
                                @foreach($speciality as $speciality1)
                                <option value="{{ $speciality1->id }}" @if(isset($speciality_id) && $speciality_id->id == $speciality1->id)selected @endif>{{ $speciality1->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <div class="form-group col-sm-2">
                            <label>Experience (Years):</label> 
                            {!! Form::selectRange('experience',1,35,$user->experience,['class' => 'form-control',$required]) !!}
                        </div>

                        <div class="form-group col-sm-12">
                            @if($user->isCompetitor == 1)
                            <input type="checkbox" name="isCompetitor" id="isCompetitor" checked="checked">Are you a competitor?
                            @else
                            <input type="checkbox" name="isCompetitor" id="isCompetitor">Are you a competitor?
                            @endif
                            <div class="form-group mt10" id="div_federation" @if($user->isCompetitor == 0) style="display:none" @endif >
                                 <label>Federation</label>
                                <input type="text" name="federation" placeholder="If so, which Federation?" class="form-control federation" value="{{ $user->federation }}">
                            </div>     
                        </div>
                        <div class="form-group col-sm-3"> 
                            {!! Form::label('height') !!}
                            <select class="form-control" name="height" id="height">
                                <option value="">Change Height *</option>
                                <option value="4'10''">4'10"</option>
                                <option value="4'11''">4'11"</option>
                                <option value="5'0''">5'0"</option>
                                <option value="5'1''">5'1"</option>
                                <option value="5'2''">5'2"</option>
                                <option value="5'3''">5'3"</option>
                                <option value="5'4''">5'4"</option>
                                <option value="5'5''">5'5"</option>
                                <option value="5'6''">5'6"</option>
                                <option value="5'7''">5'7"</option>
                                <option value="5'8''">5'8"</option>
                                <option value="5'9''">5'9"</option>
                                <option value="5'10''">5'10"</option>
                                <option value="5'11''">5'11"</option>
                                <option value="6'0''">6'0"</option>
                                <option value="6'1''">6'1"</option>
                                <option value="6'2''">6'2"</option>
                                <option value="6'3''">6'3"</option>
                                <option value="6'4''">6'4"</option>
                                <option value="6'5''">6'5"</option>
                                <option value="6'6''">6'6"</option>
                                <option value="6'7''">6'7"</option>
                                <option value="6'8''">6'8"</option>
                                <option value="6'9''">6'9"</option>
                                <option value="6'10''">6'10"</option>
                            </select>

                        </div>
                        <div class="form-group col-sm-3">  
                            {!! Form::label('weight') !!}
                            <div class="input-group">

                                {!! Form::text('weight', $user->weight  , array('placeholder'=>"Weight *",'class' => 'form-control',$required) ) !!}

                                <span class="input-group-addon">LBS</span>
                            </div>

                        </div>

                        <div class="form-group col-sm-6">  
                            {!! Form::label('medicalConcerns') !!}
                            {!! Form::text('medicalConcerns', $user->medicalConcerns , array('placeholder'=>"Medical Concerns",'class' => 'form-control') ) !!}
                        </div>
                        <div class="form-group">
                            <div class="ml15">
                                {!! Form::label('gender', 'Date of birth') !!} 
                            </div>
                            <div class="fnc-fom form-group col-sm-2">
                                <select class="form-control" name="day" id="day" required="required">
                                    <option value="">Day</option>
                                    @foreach($days as $day)
                                    <option value="{{ $day }}">{{ $day }}</option> 
                                    @endforeach
                                </select>
                            </div>
                            <div class="fnc-fom form-group col-sm-2">
                                <select class="form-control" name="month" id="month" required="required">
                                    <option value="">Month</option>
                                    @foreach($months as $month)
                                    <option value="{{ $month }}">{{ $month }}</option> 
                                    @endforeach
                                </select>
                            </div>
                            <div class="fnc-fom form-group col-sm-2">
                                <select class="form-control" name="year" id="year" required="required">
                                    <option value="">Year</option>
                                    @foreach($years as $year)
                                    <option value="{{ $year }}">{{ $year }}</option> 
                                    @endforeach
                                </select>
                            </div>
                            <!--                            <div class="input-group">
                                                            {!! Form::text('dob',date('d-m-Y',strtotime($user->dob)), 
                                                            array(
                                                            'class' => 'form-control',
                                                            'data-provide' => 'datepicker', 
                                                            'id' => 'dob',
                                                            'placeholder' => 'Select your DOB') ) !!}
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-calendar posrel" aria-hidden="true"></i>
                                                            </span>
                                                        </div>-->
                        </div>




                        <div class="col-md-12 form-group fit__sub__reset clrlist">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="fit__submit">Update</button>
                            <button type="button" class="fit__reset" onclick="back()">Back</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
</section>  


<script>
    $('#height').val("<?php echo $user->height; ?>");
    $('#day').val("<?php echo $user->date; ?>");
    $('#month').val("<?php echo $user->month; ?>");
    $('#year').val("<?php echo $user->year; ?>");
    $("#isCompetitor").change(function () {
        //  alert(this.checked);
        if (this.checked) {
            //Do stuff
            $('#div_federation').show();
            $('.federation').addClass('required');
        } else {
            $('#div_federation').hide();
            $('.federation').removeClass('required');
        }
    });

</script>
<script>
    jQuery('[data-provide="datepicker"]').datepicker({
        format: "dd-mm-yyyy"
    }).on('change', function () {
        jQuery('.datepicker').hide();
    });
</script>
@endsection