@extends('customer')

@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="dash__rgt col-sm-9">
            <div id="profile">
			
				<div class="hed">
                                <h2>professional</h2>
                </div>
				
				<div class="clearfix"></div>
							
                <div class="profile__dtl col-sm-12">
                    <div class="profile__btns clrlist">
                        <ul class="pul-rgt">
                            @if($user->status == 1)
                            <li><a href="#" class="profile__approve__btn"><i class="fa fa-check-square-o"></i>Approved</a></li>
                            @endif
                            <li><a href="{{url('professional/edit')}}" class="profile__edit__btn"><i class="fa fa-pencil"></i>Edit</a></li>
                        </ul>
                    </div>

					
				<div class="clearfix"></div>
							
							
                    <div class="label-box col-sm-6">
                        <h5>About me:</h5>
                        <h4>
                            {{$user->about}}
                        </h4>
                    </div>
                    <div class="label-box col-sm-6">
                        <h5>Goals:</h5>
                        <h4>
                            {{$user->goals}}
                        </h4>
                    </div>
@if(Auth::user()->role->role == 'ambassador')
                    <div class="label-box col-sm-6">
                        <h5>Speciality:</h5>
                        <h4>
                            @if(is_null($speciality)) Not Added @else {{$speciality->title}} @endif
                        </h4>
                    </div>
@endif
                    <div class="label-box col-sm-3">
                        <h5>Experience:</h5>
                        <h4>@if($user->experience == '')
                            Not Mentioned Yet
                            @else
                            {{$user->experience}}+ 
                            @endif
                        </h4>
                    </div>
                    <div class="label-box col-sm-3">
                        @if($user->isCompetitor == 1)
                        <h5>Federation:</h5>
                        <h4>
                            {{$user->federation}}
                        </h4>
                        @endif
                    </div>
                    <div class="label-box col-sm-2">
                        <h5>Height:</h5>
                        <h4>
                            {{$user->height}} Feet
                        </h4>
                    </div>
                    <div class="label-box col-sm-3">
                        <h5>Weight:</h5>
                        <h4>
                            {{$user->weight}} LBS
                        </h4>
                    </div>
                    <div class="label-box col-sm-4">
                        <h5>Medical Concerns:</h5>
                        <h4>
                            {{$user->medicalConcerns}} 
                        </h4>
                    </div>
                    <div class="label-box col-sm-3">
                        <h5>Date of birth:</h5>
                        <h4>
                            {{date('d-m-Y',strtotime($user->dob))}}
                        </h4>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section> 

@endsection