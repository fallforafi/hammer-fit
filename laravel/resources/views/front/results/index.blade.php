@extends('customer')

<?php
$currency = Config::get('params.currency');
$required = 'required';
$role = Auth::user()->role->role;
//d($role,1);
?>
<link rel="stylesheet" href="{{asset('front/croppie.css')}}">
<script src="{{asset('front/js/croppie.js')}}"></script>
<style>
    .form-img img {
        width: 280px;
        height: 250px;
        display: none;
    }
</style>
@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="dash__rgt col-sm-9">
            <div class="tab-content p20">
                <div class="hed">
                    @if($role == 'athlete')
                    <h2>Progress Form</h2>
                    @else
                    <h2>Albums</h2>
                    @endif
                </div>

                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                {!! Form::open(array('files' => true,'class' => 'form','url' => 'results/save', 'method' => 'post')) !!}
                <div class="row">
                    <div class="form-group col-sm-12">
                        {!! Form::text('caption',  null , 
                        array(
                        'class' => 'form-control',
                        'id' => 'caption',
                        'placeholder' => 'Caption'
                        ,$required) ) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 form-group ">
                        <div class="input-group">
                            <input type="number" name="currentWeight" class="form-control" placeholder="Current Weight" required="required">
                            <span class="input-group-addon">
                                LBS
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group form__date col-sm-12 date0">
                        <div class="input-group">
                            {!! Form::text('date',  null , 
                            array(
                            'class' => 'form-control',
                            'data-provide' => 'datepicker', 
                            'id' => 'date',
                            'placeholder' => 'Select Date'
                            ,$required) ) !!}
                            <span class="input-group-addon">
                                <i class="fa fa-calendar posrel" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="btn-actions row">

                    <div class="col-sm-4">
                        <div class="form-img">
                            <img id="front-img" src="" alt="Uploaded Image">
                        </div> 
                        <div class=" reg__photo__upload">
                            <input name="frontImage" type="file" required="required" onchange="readURL(this, 'front-img');">
                            <button>Front Picture</button>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-img">
                            <img id="side-img" src="" alt="Uploaded Image">
                        </div> 
                        <div class=" reg__photo__upload">
                            <input name="sideImage" type="file" onchange="readURL(this, 'side-img');">
                            <button>Side Picture</button>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-img">
                            <img id="back-img" src="" alt="Uploaded Image">
                        </div> 
                        <div class=" reg__photo__upload">
                            <input name="backImage" type="file" onchange="readURL(this, 'back-img');">
                            <button>Back Picture</button>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="form-group col-sm-6 col-sm-offset-3">
                        <input type="submit" class="btn-lg btn-block btn-primary" placeholder="Save Progress">
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

            <div class="update-area row">

                <div class="hed">
                    <h2>Updates </h2>(<label>Press Ctrl + F5 to refresh</label>)
                </div>
                @if(count($model)>0)

                <?php
                foreach ($model as $result) {
                    ?>
                    <div class="upd__table" id="<?php echo $result->id ?>">
                        <table class="text-center table">
                            <thead>
                                <tr>
                                    <th colspan="3">

                                        <span id="date_<?php echo $result->date ?>" class="pul-lft edit-date">
                                            <span id="t_date_<?php echo $result->id ?>"><?php echo date('d-m-Y', strtotime($result->date)) ?></span>

                                            <a class="icon" href="javascript:void(0);" onclick="edit('<?php echo $result->id ?>', 'date')" ><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                        </span>

                                        <span style="display:none;" id="edit_date_<?php echo $result->id ?>">

                                            <div class="form-group form__date col-sm-12 date0 mb0 pl0">

                                                <div class="input-group clr">
                                                    <input type="text" class="form-control" name="v_date_<?php echo $result->id ?>" id="v_date_<?php echo $result->id ?>" value="<?php echo date('d-m-Y', strtotime($result->date)) ?>" data-provide="datepicker" placeholder="Select Date">

                                                    <span class="input-group-addon">
                                                        <a class="btn btn-default" href="javascript:void(0);" onclick="save('<?php echo $result->id ?>', 'date')">Save</a>
                                                    </span>
                                                </div>

                                            </div>



                                        </span>
                                        <span class="pull-right">
                                            <button type="button" class="btn btn-danger delete" data-toggle="modal" data-target="#myModal" data-id="<?php echo url('result/delete/' . $result->id); ?>"><i class="fa fa-trash"></i></button>
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="upd__photo__side">
                                    <td><h4>Front Picture</h4></td>
                                    <td><h4>Side Picture</h4></td>
                                    <td><h4>Back Picture</h4></td>
                                </tr>
                                <tr class="upd__photos__box">
                                    <td for="Updates 1">
                                        <!--    Code for cropping tool starts -->
                                        <div id="success-front-<?php echo $result->id; ?>"></div>
                                        <div class="actions">
                                            <a href="javascript:void(0);" onclick="changeImage('<?php echo $result->id ?>', 'front_image', 'Front Picture', '<?php echo $result->frontImage ?>');" class="btn btn-info btn-change"><i class="fa fa-upload"></i></a>
                                            <i class="fa fa-rotate-left vanilla-front-rotate-<?php echo $result->id; ?> btn btn-default" data-deg="-90" aria-hidden="true"></i>
                                            <i class="fa fa-rotate-right vanilla-front-rotate-<?php echo $result->id; ?> btn btn-default" data-deg="90" aria-hidden="true"></i>

                                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                            <i class="btn btn-default upload-front-result-<?php echo $result->id; ?> fa fa-save"></i>
                                        </div>
                                        <div class="no-transition" id="front-<?php echo $result->id; ?>" style="">
                                        </div>
                                        <script type="text/javascript">
    <?php if (is_null($result->frontImage)) { ?>
                                                var srcFront<?php echo $result->id; ?> = "{{ asset('front/images/no_result.jpg')}}";
    <?php } else { ?>
                                                var srcFront<?php echo $result->id; ?> = "{{ asset('uploads/users/results/front_images/thumbnail/'. $result->frontImage)}}";
    <?php } ?>
                                            var el = document.getElementById('front-<?php echo $result->id; ?>');
                                            vanillaFront<?php echo $result->id; ?> = new Croppie(el, {
                                                viewport: {width: 200, height: 200},
                                                boundary: {width: 250, height: 250},
                                                enableResize: true,
                                                enableOrientation: true
                                            });
                                            //$('#imgInp').on('change', function () {
                                            //   var reader = new FileReader();
                                            //   reader.onload = function (e) {
                                            vanillaFront<?php echo $result->id; ?>.bind({
                                                url: srcFront<?php echo $result->id; ?>,
                                                zoom: 0
                                            }).then(function () {
                                                console.log('jQuery bind complete');
                                            });

                                            $('.vanilla-front-rotate-<?php echo $result->id; ?>').on('click', function (ev) {
                                                vanillaFront<?php echo $result->id; ?>.rotate(parseInt($(this).data('deg')));
                                            });

                                            $('.upload-front-result-<?php echo $result->id; ?>').on('click', function (ev) {
                                                vanillaFront<?php echo $result->id; ?>.result({
                                                    type: 'canvas',
                                                    size: 'viewport'
                                                }).then(function (resp) {
                                                    var _token = $('#_token').val();
                                                    $.ajax({
                                                        url: "<?php echo url('crop'); ?>",
                                                        type: "POST",
                                                        dataType: 'json',
                                                        data: {"image": resp, '_token': _token, "src": srcFront<?php echo $result->id; ?>},
                                                        success: function (data) {
                                                            console.log(data);
                                                            if (data.success === 1) {
                                                                var errorsHtml = '<div class="alert alert-success"> <i class="fa fa-check"></i> Picture Updated Successfully.</div>';
                                                                $('#success-front-<?php echo $result->id; ?>').html(errorsHtml).show();
                                                                setTimeout(function () {
                                                                    location.reload(true);
                                                                }, 300);
                                                            }
                                                        }
                                                    });
                                                });
                                                //
                                            });
                                        </script>
                                        <!--    Code for cropping tool ends -->

                                        <?php
                                        $frontImage = public_path() . '/uploads/users/results/front_images/original/' . $result->frontImage;
                                        if (file_exists($frontImage)) {
                                            ?>
                                            <a href="{{asset('uploads/users/results/front_images/original/')}}/<?php echo $result->frontImage; ?>" class="btn btn-primary" target="_blank">View Enlarge</a>
                                        <?php } else { ?>
                                            <a href="{{asset('uploads/users/results/front_images/')}}/<?php echo $result->frontImage; ?>" class="btn btn-primary" target="_blank">View Enlarge</a>
                                        <?php } ?>

                                    </td>
                                    <td>
                                        <!--    Code for cropping tool starts -->
                                        <div id="success-side-<?php echo $result->id; ?>"></div>
                                        <div class="actions">
                                            <?php
                                            if ($result->sideImage != "") {
                                                ?>
                                                <a href="javascript:void(0);" onclick="changeImage('<?php echo $result->id ?>', 'side_image', 'Side Picture', '<?php echo $result->sideImage ?>');" class="btn btn-info btn-change"><i class="fa fa-upload"></i></a>
                                                <a href="javascript:void(0);" onclick="deleteImage('<?php echo $result->id ?>', 'sideImage');" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>
                                            <?php } else { ?>
                                                <a href="javascript:void(0);" onclick="changeImage('<?php echo $result->id ?>', 'side_image', 'Side Picture', '<?php echo $result->sideImage ?>');" class="btn btn-info btn-change"><i class="fa fa-upload"></i></a>
                                            <?php } ?>
                                            <i class="fa fa-rotate-left vanilla-side-rotate-<?php echo $result->id; ?> btn btn-default" data-deg="-90" aria-hidden="true"></i>
                                            <i class="fa fa-rotate-right vanilla-side-rotate-<?php echo $result->id; ?> btn btn-default" data-deg="90" aria-hidden="true"></i>

                                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                            <i class="btn btn-default upload-side-result-<?php echo $result->id; ?> fa fa-save"></i>
                                        </div>

                                        <div class="no-transition" id="side-<?php echo $result->id; ?>" style="">
                                        </div>
                                        <script type="text/javascript">
    <?php if (is_null($result->sideImage)) { ?>
                                                var srcSide<?php echo $result->id; ?> = "{{ asset('front/images/no_result.jpg')}}";
    <?php } else { ?>
                                                var srcSide<?php echo $result->id; ?> = "{{ asset('uploads/users/results/side_images/thumbnail/'. $result->sideImage)}}";
    <?php } ?>
                                            var el = document.getElementById('side-<?php echo $result->id; ?>');
                                            vanilla<?php echo $result->id; ?> = new Croppie(el, {
                                                viewport: {width: 200, height: 200},
                                                boundary: {width: 250, height: 250},
                                                enableResize: true,
                                                enableOrientation: true
                                            });
                                            //$('#imgInp').on('change', function () {
                                            //   var reader = new FileReader();
                                            //   reader.onload = function (e) {
                                            vanilla<?php echo $result->id; ?>.bind({
                                                url: srcSide<?php echo $result->id; ?>,
                                                zoom: 0
                                            }).then(function () {
                                                console.log('jQuery bind complete');
                                            });

                                            $('.vanilla-side-rotate-<?php echo $result->id; ?>').on('click', function (ev) {
                                                vanilla<?php echo $result->id; ?>.rotate(parseInt($(this).data('deg')));
                                            });

                                            $('.upload-side-result-<?php echo $result->id; ?>').on('click', function (ev) {
                                                vanilla<?php echo $result->id; ?>.result({
                                                    type: 'canvas',
                                                    size: 'viewport'
                                                }).then(function (resp) {
                                                    var _token = $('#_token').val();
                                                    $.ajax({
                                                        url: "<?php echo url('crop'); ?>",
                                                        type: "POST",
                                                        dataType: 'json',
                                                        data: {"image": resp, '_token': _token, "src": srcSide<?php echo $result->id; ?>},
                                                        success: function (data) {
                                                            console.log(data);
                                                            if (data.success === 1) {
                                                                var errorsHtml = '<div class="alert alert-success"> <i class="fa fa-check"></i> Picture Updated Successfully.</div>';
                                                                $('#success-side-<?php echo $result->id; ?>').html(errorsHtml).show();
                                                                setTimeout(function () {
                                                                    location.reload(true);
                                                                }, 300);
                                                            }
                                                        }
                                                    });
                                                });
                                                //
                                            });
                                        </script>
                                        <!--    Code for cropping tool ends -->

                                        <?php
                                        $sideImage = public_path() . '/uploads/users/results/side_images/original/' . $result->sideImage;
                                        if (file_exists($sideImage)) {
                                            ?>
                                            <a href="{{asset('uploads/users/results/side_images/original/')}}/<?php echo $result->sideImage; ?>" class="btn btn-primary" target="_blank">View Enlarge</a>
                                        <?php } else { ?>
                                            <a href="{{asset('uploads/users/results/side_images/')}}/<?php echo $result->sideImage; ?>" class="btn btn-primary" target="_blank">View Enlarge</a>
                                        <?php } ?>

                                    </td>
                                    <td>
                                        <!--    Code for cropping tool starts -->
                                        <div id="success-back-<?php echo $result->id; ?>"></div>
                                        <div class="actions">
                                            <?php
                                            if ($result->backImage != "") {
                                                ?>
                                                <a href="javascript:void(0);" onclick="changeImage('<?php echo $result->id ?>', 'back_image', 'Back Picture', '<?php echo $result->backImage ?>');" class="btn btn-info btn-change"><i class="fa fa-upload"></i></a> 
                                                <a href="javascript:void(0);" onclick="deleteImage('<?php echo $result->id ?>', 'backImage');" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>   
                                            <?php } else { ?>
                                                <a href="javascript:void(0);" onclick="changeImage('<?php echo $result->id ?>', 'back_image', 'Back Picture', '<?php echo $result->backImage ?>');" class="btn btn-info btn-change"><i class="fa fa-upload"></i></a>
                                            <?php } ?>
                                            <i class="fa fa-rotate-left vanilla-back-rotate-<?php echo $result->id; ?> btn btn-default" data-deg="-90" aria-hidden="true"></i>
                                            <i class="fa fa-rotate-right vanilla-back-rotate-<?php echo $result->id; ?> btn btn-default" data-deg="90" aria-hidden="true"></i>

                                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                            <i class="btn btn-default upload-back-result-<?php echo $result->id; ?> fa fa-save"></i>
                                        </div>
                                        <div class="no-transition" id="back-<?php echo $result->id; ?>" style="">
                                            
                                        </div>
                                        <script type="text/javascript">
<?php if (is_null($result->backImage)) { ?>
                                        var srcBack<?php echo $result->id; ?> = "{{ asset('front/images/no_result.jpg')}}";
<?php } else { ?>
                                        var srcBack<?php echo $result->id; ?> = "{{ asset('uploads/users/results/back_images/thumbnail/'. $result->backImage)}}";
<?php } ?>
                                    var el = document.getElementById('back-<?php echo $result->id; ?>');
                                    vanillaBack<?php echo $result->id; ?> = new Croppie(el, {
                                        viewport: {width: 200, height: 200},
                                        boundary: {width: 250, height: 250},
                                        enableResize: true,
                                        enableOrientation: true
                                    });
//$('#imgInp').on('change', function () {
//   var reader = new FileReader();
//   reader.onload = function (e) {
                                    vanillaBack<?php echo $result->id; ?>.bind({
                                        url: srcBack<?php echo $result->id; ?>,
                                        zoom: 0
                                    }).then(function () {
                                        console.log('jQuery bind complete');
                                    });

                                    $('.vanilla-back-rotate-<?php echo $result->id; ?>').on('click', function (ev) {
                                        vanillaBack<?php echo $result->id; ?>.rotate(parseInt($(this).data('deg')));
                                    });

                                    $('.upload-back-result-<?php echo $result->id; ?>').on('click', function (ev) {
                                        vanillaBack<?php echo $result->id; ?>.result({
                                            type: 'canvas',
                                            size: 'viewport'
                                        }).then(function (resp) {
                                            var _token = $('#_token').val();
                                            $.ajax({
                                                url: "<?php echo url('crop'); ?>",
                                                type: "POST",
                                                dataType: 'json',
                                                data: {"image": resp, '_token': _token, "src": srcBack<?php echo $result->id; ?>},
                                                success: function (data) {
                                                    console.log(data);
                                                    if (data.success === 1) {
                                                        var errorsHtml = '<div class="alert alert-success"> <i class="fa fa-check"></i> Picture Updated Successfully.</div>';
                                                        $('#success-back-<?php echo $result->id; ?>').html(errorsHtml).show();
                                                        setTimeout(function () {
                                                            location.reload(true);
                                                        }, 300);
                                                    }
                                                }
                                            });
                                        });
//
                                    });
                                </script>
                                        <!--    Code for cropping tool ends -->

                                        <?php
                                        $backImage = public_path() . '/uploads/users/results/back_images/original/' . $result->backImage;
                                        if (file_exists($backImage)) {
                                            ?>
                                            <a href="{{asset('uploads/users/results/back_images/original/')}}/<?php echo $result->backImage; ?>" class="btn btn-primary" target="_blank">View Enlarge</a>
                                        <?php } else { ?>
                                            <a href="{{asset('uploads/users/results/back_images/')}}/<?php echo $result->backImage; ?>" class="btn btn-primary" target="_blank">View Enlarge</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <span id="caption_<?php echo $result->id ?>">
                                            <span id="t_caption_<?php echo $result->id ?>" ><?php echo $result->caption ?></span>

                                            <a class="btn-edit-xs" href="javascript:void(0);" onclick="edit('<?php echo $result->id ?>', 'caption')" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        </span>
                                        <span style="display:none;" id="edit_caption_<?php echo $result->id ?>">
                                            <div class="form-group form__date col-sm-12 date0 pul-cntr">

                                                <div class="input-group clr">
                                                    <input type="text" class="form-control" name="v_caption_<?php echo $result->id ?>" id="v_caption_<?php echo $result->id ?>" value="<?php echo $result->caption; ?>" placeholder="Caption">

                                                    <span class="input-group-addon">
                                                        <a class="btn " href="javascript:void(0);" onclick="save('<?php echo $result->id ?>', 'caption')">Save</a>
                                                    </span>

                                                </div>
                                            </div>
                                        </span>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <span id="weight_<?php echo $result->id ?>">
                                            <span id="t_weight_<?php echo $result->id ?>" ><?php
                                                if ($result->currentWeight != '') {
                                                    echo $result->currentWeight . ' ' . 'LBS';
                                                } else {
                                                    echo "Weight not added";
                                                }
                                                ?> </span>

                                            <a class="btn-edit-xs" href="javascript:void(0);" onclick="edit('<?php echo $result->id ?>', 'weight')" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        </span>
                                        <span style="display:none;" id="edit_weight_<?php echo $result->id ?>">
                                            <div class="form-group form__date col-sm-12 date0 pul-cntr">

                                                <div class="input-group clr">
                                                    <input type="number" class="form-control" name="v_weight_<?php echo $result->id ?>" id="v_weight_<?php echo $result->id ?>" value="<?php echo $result->currentWeight; ?>" placeholder="Current Weight">

                                                    <span class="input-group-addon">
                                                        <a class="btn " href="javascript:void(0);" onclick="save('<?php echo $result->id ?>', 'weight')">Save</a>
                                                    </span>

                                                </div>
                                            </div>
                                        </span>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <span> Body Fat Est: <?php
                                            if ($result->currentBodyFat != '') {
                                                echo $result->currentBodyFat . ' ' . '%';
                                            } else {
                                                echo "Not Added Yet";
                                            }
                                            ?> </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
                @include('front/common/delete_modal')
                @else
                <div class="upd__table">
                    <h3>No Data Found. . . </h3>  
                </div>
                @endif
            </div>
        </div>

    </div>
</div>
</div>
</section>  
<script>
    jQuery('.delete').click(function ()
    {
        $('#delete').attr('href', $(this).data('id'));
    });
</script>
<script>
    function edit(id, type) {
        $("#" + type + "_" + id).hide();
        $("#edit_" + type + "_" + id).show();
    }

    function save(id, type) {
        var value = $("#v_" + type + "_" + id).val();
        $.get("<?php echo url("results/save"); ?>" + type, {value: value, id: id}, function () {
            $("#" + type + "_" + id).show();
            $("#edit_" + type + "_" + id).hide();
            $("#v_" + type + "_" + id).val(value);
            $("#t_" + type + "_" + id).text(value);
        })
                .done(function () {
                    //alert( "second success" );
                    window.location.reload();
                })
                .fail(function () {
                    //alert( "error" );
                })
                .always(function () {
                    //alert( "finished" );
                });
    }

    function changeImage(id, image, title, currentImage) {
        $("#resultImage").modal('show');
        $("#model_header").html(title);
        $("#side").val(image);
        $("#id").val(id);
        $("#cImage").val(currentImage);
    }

    function deleteImage(id, side) {
        var token = '<?php echo csrf_token(); ?>';

        $.post("<?php echo url("results/deleteimage"); ?>", {side: side, id: id, _token: token}, function () {

            window.location.reload();

        });

    }

</script>


<!-- Modal -->
<div class="modal fade" id="resultImage" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        {!! Form::open(array('files' => true,'class' => 'form','url' => 'results/saveimage', 'method' => 'post','id' => 'form')) !!}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="model_header"></h4>
            </div>
            <div class="modal-body">
                <div class="reg__photo__upload">
                    <input name="image" type="file">
                    <button type="button">Please Select Picture</button>
                </div>  

                <div class="row">
                    <div class="form-group col-sm-12">
                        <input type="submit" class="form-control" placeholder="Save">
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" value="" name="side" id="side">
        <input type="hidden" value="" name="id" id="id">
        <input type="hidden" value="" name="cImage" id="cImage">
        {!! Form::close() !!}
    </div>
</div>
<script type="text/javascript">
    function readURL(input, type) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#' + type).attr('src', e.target.result);
                $('#' + type).show();
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection