@extends('customer')

@section('content')
<section class="dashboard-area">
    <div class="container">

        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="trainingDetails">
                    <div class="workout-main col-sm-12">
                        <div class="workout-inr">
                            <div class="flash-message">
                                @if (count($errors->register) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->register->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success">
                                    <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
                                </div>
                                @endif
                            </div>
                            @include('front.shows.form')
                        </div>

                    </div>
                </div></div></div>
    </div>
</section>
@endsection

