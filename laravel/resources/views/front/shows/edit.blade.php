@extends('customer')

@section('content')
<?php
$required = 'required';
?>
<section class="dashboard-area">
    <div class="container">


        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="trainingDetails">
                    <div class="workout-main col-sm-12">
                        <div class="workout-inr">
                            <div class="flash-message">
                                @if (count($errors->register) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->register->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success">
                                    <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
                                </div>
                                @endif
                            </div>
                            <form method="post" action="<?php echo url('shows/update'); ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <h3>Title:</h3>
                                    <input type="text" name="title" class="form-control" placeholder="Title *" required="required" value="{{ $shows->title}}">
                                   

                                </div>
                                <div class="form-group">
                                    <h3>Description:</h3>
                                    <textarea name="description" class="form-control" placeholder="Description *" required="required" maxlength="200">{{ $shows->description}}</textarea>
                                    
                                </div>
                                <div class="form-group">
                                    <h3>Date of Next Show:</h3>
                                    <div class="form-group form__date date0">

                                        <input type="text" class="form-control" name="date" data-provide="datepicker" placeholder="Date of Next Show:" value="{{ date('m/d/Y', strtotime($shows->date)) }}"/>
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        
                                    </div>
                                </div>  
                                <div class="col-sm-12 form-group fit__sub__reset clrlist">
                                    <input type="hidden" name="show_id" value="{{ $shows->id }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="fit__submit">Update</button>
                                    <a href="{{ url('shows') }}" class="btn btn-flat fit__reset">Back</a>
                                </div>
                                <br>

                            </form>
                        </div>

                    </div>
                </div></div></div>
    </div>
</section>

@endsection


