<?php
$required = 'required';
?>
<form method="post" action="<?php echo url('shows/store'); ?>">
    <div class="form-group ">
        <label>Title:</label>
        <input type="text" name="title" class="form-control" placeholder="Title *" <?php echo $required;?> value="{{ old('title') }}">
        @if ($errors->has('title'))
        <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group ">
        <label>Description:</label>
        <textarea name="description" class="form-control" placeholder="Description *" <?php echo $required;?> maxlength="200">{{ old('description') }}</textarea>
        @if ($errors->has('description'))
        <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group ">
        <label>Date of Next Show:</label>
        <div class="form-group form__date date0">
            <input type="text" class="form-control" name="date" data-provide="datepicker" <?php echo $required;?> placeholder="Date of Next Show:" value="{{ old('date') }}" />
            <i class="fa fa-calendar" aria-hidden="true"></i>
            @if ($errors->has('date'))
            <span class="help-block">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
            @endif
        </div>
    </div>  
    <div class="form-group  fit__sub__reset clrlist">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"><br>
        <button type="submit" class="fit__submit">Save</button>
        <button type="button" class="fit__reset" onclick="back()">Back</button>
    </div><br>


</form>



<script type="text/javascript">
    jQuery('#file2').bind("change", function () {

        var imgVal = $('#file2').val();
        if (imgVal != '') {
            jQuery("#upfile2").text("File Added");
        } else {
            alert("Please add photo");
        }
        return false;
    });
</script>
<script type="text/javascript">
    $("#upfile2").click(function () {
        $("#file2").trigger('click');
    });
</script>
