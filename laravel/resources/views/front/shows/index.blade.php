@extends('customer')


@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
            @endforeach
        </div> <!-- end .flash-message -->
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>

        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="trainingDetails">
                    <div class="workout-main col-sm-12">
                        <div class="workout-inr">
                            <div class="hed">
                                <h2>UPLOADED <span>Shows</span></h2>
                            </div>
                            <div class="table-responsive">    
                                <div class="row">
                                    <div class="col-sm-12">
                                        <a href="{{url('shows/create')}}" class="btn btn-success btn-flat pull-right"><i class="fa fa-plus"></i> Add New</a>
                                    </div>
                                </div>
                                <table class="table table-bordered mt20 ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($shows as $row)
                                        <tr>
                                            <td><a href="shows/edit/{{ $row->id }}"><?php
                                                    echo $i;
                                                    $i++;
                                                    ?></a></td>
                                            <td><a href="shows/edit/{{ $row->id }}">{{ $row->title }}</a></td>
                                            <td>{{ $row->description }}</td>

                                            <td>{{ date('d/m/Y', strtotime($row->date)) }}</td>
                                            <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal-{{ $row->id }}"><i class="fa fa-remove"></i> Delete</button>
                                                <div class="modal fade" id="myModal-{{ $row->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" style="color: red">Alert</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <h3>Are you sure do you want to delete this?</h3>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a class="btn btn-danger pull-left" href="shows/delete/<?php echo $row->id ?>">Yes</a>

                                                                <button type="button" class="btn btn-success pull-left" data-dismiss="modal">No</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div></div></div></div>
</section>                       
@endsection                        

