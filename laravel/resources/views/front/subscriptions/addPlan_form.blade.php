<?php
$required = 'required';
?>
<div class="col-sm-12">
    <div class="profile__desc">
        <h3>Title:</h3>
        <input type="text" name="title" class="form-control" placeholder="Title *" required="required">
    </div>
    <div class="profile__desc">
        <h3>Description:</h3>
        <textarea name="description" class="form-control" placeholder="Description *" required="required" maxlength="200"></textarea>
    </div>
    <div class="profile_desc col-sm-6">
        <div class="form-img"><label id="blah"></label></div> 
        <div class="fit__sub__reset clrlis">    
            <label>Add File *</label>
            <input type="file" id="file2" name="file" required="required">
            <label id="msg">Pdf Only</label>
        </div>  
    </div>
</div>
