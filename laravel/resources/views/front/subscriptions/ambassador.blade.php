<div class="hed">
    <h3>ASSIGNED <span>ATHLETES</span></h3>
</div>
<div class="workout__plans">
    @if(count($users) > 0) 
    @foreach($users as $user)
    <div class="workout__box col-sm-12">
        <div class="workout__img col-sm-3">
            @if(is_null($user->image))
            <img  alt="" src="{{ asset('front/images/no_result.jpg')}}">
            @else
            <img src="{{ asset('uploads/users')}}/<?php echo $user->image; ?>" alt="" style="height: 150px;">
            @endif
        </div>
        <div class="workout__plan__title col-sm-6">

            <table class="table table-striped">
                <tbody>
                    <tr><td><span> Name:</span></td>
                        <td>{{ $user->firstName }} {{ $user->middleName }} {{ $user->lastName }}
                        </td>
                    </tr>
                    <tr><td><span> Price:</span></td>
                        <td><?php echo $currency[Config::get('params.currency_default')]['symbol'] ?> {{ $user->subscriptionRate }} {{$per_month}}
                        </td>
                    </tr>
                    <tr><td><span> Date:</span></td>
                        <td>{{ date("d/m/Y", strtotime($user->subscriptionDate)) }}
                        </td>
                    </tr>
                    <tr><td><span> Meal Plan:</span></td>
                        <td>
                            @if(!empty($athleteMp[$user->id]))
                            {{ $athleteMp[$user->id]->mp_name }} <strong>({{ date('d/m/Y', strtotime($athleteMp[$user->id]->created_at)) }})</strong>
                            @endif
                        </td>
                    </tr>
                    <tr><td><span> Workout Plan:</span></td>
                        <td>
                            @if(!empty($athleteWp[$user->id]))
                            {{ $athleteWp[$user->id]->wp_name }} <strong>({{ date('d/m/Y', strtotime($athleteWp[$user->id]->created_at)) }})</strong>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Subscribed</button><br><br>
            <tr><button type="button" class="btn btn-info btn-sm" data-athlete-id="{{ $user->id }}" data-toggle="modal" data-target="#m-plan"><i class="fa fa-plus"></i> M-Plan</button>
            <button type="button" class="btn btn-info btn-sm" data-athlete-id="{{ $user->id }}" data-toggle="modal" data-target="#w-plan"><i class="fa fa-plus"></i> W-Plan</button>
            </tr>
            <div class="status__renew1 ">
                <a href="{{ url('user/'.$user->id) }}" class="btn btn-primary">View Profile</a>
            </div>  
            <div class="status__renew1 ">
                <a href="{{ url('updates-needed/'.$user->id) }}" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i> Updates Needed</a>
            </div>  
        </div>  
    </div>
    @endforeach

    <div class="modal fade" id="m-plan">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title"><strong>Add Meal Plan</strong></h3>
                </div>
                <div class="modal-body">
                    <form name="mplan" method="post" action="{{ url('add-mplan') }}" enctype="multipart/form-data">
                        @include('front.subscriptions.addPlan_form')
                        <div class="form-group">
                            <input type="hidden" name="athlete_id" value="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"><br>
                            <button type="submit" class="btn btn-lg btn-success btn-block pull-right">Save</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="w-plan">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title"><strong>Add Workout Plan</strong></h3>
                </div>
                <div class="modal-body">
                    <form name="wplan" method="post" action="{{ url('add-wplan') }}" enctype="multipart/form-data">
                        @include('front.subscriptions.addPlan_form')
                        <div class="form-group">
                            <input type="hidden" name="athlete_id" value="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"><br>
                            <button type="submit" class="btn btn-lg btn-success btn-block pull-right">Save</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="">No Subscribed Athlete Found...</div>
    @endif
    <hr>
    @if(count($model) > 0) 

    <div class="hed">
        <h4>PREVIOUSLY <span>ASSIGNED</span></h4>
    </div>
    @foreach($model as $user)
    <div class="workout__box col-sm-12">
        <div class="workout__img col-sm-3">
            @if(is_null($user->image))
            <img  alt="" src="{{ asset('front/images/no_result.jpg')}}">
            @else
            <img src="{{ asset('uploads/users')}}/<?php echo $user->image; ?>" alt="" style="height: 150px;">
            @endif
        </div>
        <div class="workout__plan__title col-sm-6">

            <table class="table table-striped">
                <tbody>
                    <tr><td><span> Name:</span></td>
                        <td>{{ $user->firstName }} {{ $user->middleName }} {{ $user->lastName }}
                        </td>
                    </tr>
                    <tr><td><span> Price:</span></td>
                        <td><?php echo $currency[Config::get('params.currency_default')]['symbol'] ?> {{ $user->subscriptionRate }} {{$per_month}}
                        </td>
                    </tr>
                    <tr><td><span> Date:</span></td>
                        <td>{{ date("d/m/Y", strtotime($user->subscriptionDate)) }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-warning btn-sm"><i class="fa fa-warning"></i> Unsubscribed</button>

            <!--                <div class="status__renew1 ">
            
                                <button type="button"><a href="{{ url('user/'.$user->id) }}">View Profile</a></button>
                            </div>  -->
        </div>  

    </div>

    @endforeach 
    @else
    <div class="">No Unsubscribed Athlete Found...</div>
    @endif
    <script>
        //triggered when modal is about to be shown
        $('#m-plan').on('show.bs.modal', function (e) {

            //get data-id attribute of the clicked element
            var athlete_id = $(e.relatedTarget).data('athlete-id');

            //populate the textbox
            $(e.currentTarget).find('input[name="athlete_id"]').val(athlete_id);
        });
        //triggered when modal is about to be shown
        $('#w-plan').on('show.bs.modal', function (e) {

            //get data-id attribute of the clicked element
            var athlete_id = $(e.relatedTarget).data('athlete-id');

            //populate the textbox
            $(e.currentTarget).find('input[name="athlete_id"]').val(athlete_id);
        });
    </script>
</div>
