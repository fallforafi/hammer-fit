<div class="hed">
    <h3>SUBSCRIBED <span>AMBASSADORS</span></h3>
</div>
<div class="workout__plans  ">
    @if(count($user) > 0)  
    <div class="workout__box col-sm-12">
        <div class="workout__img col-sm-3">
            @if(is_null($user->image))
            <img  alt="" src="{{ asset('front/images/no_result.jpg')}}">
            @else
            <img src="{{ asset('uploads/users')}}/<?php echo $user->image; ?>" alt="" style="height: 150px;">
            @endif
        </div>
        <div class="workout__plan__title col-sm-6">

            <table class="table table-striped">
                <tbody>
                    <tr><td><span> Name:</span></td>
                        <td>{{ $user->firstName }} {{ $user->middleName }} {{ $user->lastName }}
                        </td>
                    </tr>
                    <tr><td><span> Price:</span></td>
                        <td><?php echo $currency[Config::get('params.currency_default')]['symbol'] ?> {{ $user->subscriptionRate }} {{$per_month}}
                        </td>
                    </tr>
                    <tr><td><span> Date:</span></td>
                        <td> {{ date("d/m/Y", strtotime($user->subscriptionDate)) }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-sm-3">
            <button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Subscribed</button>
            <div class="status__renew1 ">
                    <button type="button" onclick="athlete();">View Profile</button>
            </div>  
        </div>   

    </div>
    <br>
    @else
    <div class="">No Data Found...</div>
    @endif
</div>


<div class="table-responsive"> 

    <div class="hed">
        <h4>PREVIOUSLY <span>SUBSCRIBED</span></h4>
    </div>
    <table class="table">
        @if(count($model) > 0)
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Subscription Fees</th>
                <th>Subscription Date</th>     
            </tr>
        </thead>

        <tbody>
            <?php $i = 1; ?>

            @foreach($model as $row)
            <tr>
                <td><a href="user/{{ $row->ambassador_id }}"><?php echo $i;
            $i++;
            ?></a></td>
                <td><a href="user/{{ $row->ambassador_id }}">{{ $row->firstName }} {{ $row->middleName }} {{ $row->lastName }}</a></td>
                <td><?php echo $currency[Config::get('params.currency_default')]['symbol'] ?> {{ $row->subscriptionRate }} {{$per_month}}</td>        
                <td>{{ date("d/m/Y", strtotime($row->subscriptionDate)) }}</td>    
            </tr>
            @endforeach   
        </tbody>
        @else
        <div class="">No Data Found...</div>
        @endif  
    </table>

</div>
<script>
function athlete(){
    window.location.href= "<?php if(isset($user)) { echo url('user/'.$user->id); } ?>";
}
</script>
