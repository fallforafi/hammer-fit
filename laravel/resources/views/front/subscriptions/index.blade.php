@extends('customer')
<?php
$currency = Config::get('params.currency');
$per_month = Config::get('params.per_month');
?>
<?php
$role = Auth::user()->role->role;
//d($role,1);
?>
@section('content')
<section class="dashboard-area">

<style>
    .status__renew1 {
        text-align: center;
        margin-top: 40px;
        margin-bottom: 0px;
    }
    .status__renew1 button {
        background-color:#b08f6b;
        color:#fff;
        font-size:17px;
        border:none;
        height:40px;
        padding:0 15px;

    }
    .status__renew1 button:hover{
        background-color:#434343;
    }
    .workout__box {
        margin-bottom: 10px;
        text-align: center;
        border: 1px solid #9a9a9a;
        padding: 20px 0;
    }
</style>
    <div class="container">

        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>

        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="trainingDetails">
                    <div class="workout-main col-sm-12">
                        <div class="workout-inr">
                            <div class="flash-message">
                                @if (count($errors->register) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->register->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success">
                                    <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
                                </div>
                                @endif
                            </div>
                            @include('front.subscriptions.'.$role)
                        </div>
                    </div>
                </div></div>
        </div>
    </div>
</section>                       
@endsection                        

