@extends('customer')

@section('content')
<section class="dashboard-area">
    <div class="container">
        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        
        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                    <div id="trainingDetails">
                    <div class="workout-main col-sm-12">
                        <div class="workout-inr">
                            <div class="hed">
                                <h2>UPLOADED WORKOUT FILES BY <span>AMBASSADOR</span></h2>
                            </div>
                            <div class="workout__plans col-sm-4">
                                <div class="workout__box">
                                    <div class="workout__img">
                                        <img src="{{asset('')}}/front/images/workout-plan1.png" alt="workout plan">
                                    </div>
                                    <div class="workout__plan__title">
                                        <h4>MEAL PLAN</h4>
                                    </div>
                                </div>
                                <div class="workout__dwn">
                                    <button type="button"><img src="{{asset('')}}/front/images/download-icon.png" alt="download icon">Download</button>
                                </div>
                            </div>
                            <div class="workout__plans col-sm-4">
                                <div class="workout__box">
                                    <div class="workout__img">
                                        <img src="{{asset('')}}/front/images/workout-plan2.png" alt="exercise plan">
                                    </div>
                                    <div class="workout__plan__title">
                                        <h4>EXERCISE PLAN</h4>
                                    </div>
                                </div>
                                <div class="workout__dwn">
                                    <button type="button"><img src="{{asset('')}}/front/images/download-icon.png" alt="download icon">Download</button>
                                </div>
                            </div>
                            <div class="workout__plans col-sm-4">
                                <div class="workout__box">
                                    <div class="workout__img">
                                        <img src="{{asset('')}}/front/images/workout-plan3.png" alt="diet plan">
                                    </div>
                                    <div class="workout__plan__title">
                                        <h4>DIET PLAN</h4>
                                    </div>
                                </div>
                                <div class="workout__dwn">
                                    <button type="button"><img src="{{asset('')}}/front/images/download-icon.png" alt="download icon">Download</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="trn__dtl__box col-sm-5">
                        <div class="trn__dtl__inr">
                            <div class="hed">
                                <h2>TRAINING <span>DETAILS</span></h2>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="name" placeholder="Name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="weight" placeholder="Weight">
                                    </div>
                                </div>	
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="age" placeholder="Age">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="bmr" placeholder="BMR">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="mealPlan" placeholder="Current Meal Plan">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="workPlan" placeholder="Current Workout Plan">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="height" placeholder="Height">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="pkg__dtl__box col-sm-7">
                        <div class="trn__dtl__inr pkg__dtl__inr">
                            <div class="hed">
                                <h2>PACKAGE <span>DETAILS</span></h2>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="form-group form__date col-sm-12 date0">
                                        <input type="text" class="form-control" name="startDate" data-provide="datepicker" placeholder="Package Start Date">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="status" placeholder="Membership Status">
                                    </div>
                                </div>	
                                <div class="row">
                                    <div class="status__btns clrlist">
                                        <ul>
                                            <li><button type="button" class="status__active"><img src="{{asset('')}}/front/images/active-icon.png" alt="active icon">Active</button></li>
                                            <li><button type="button" class="status__inactive"><img src="{{asset('')}}/front/images/inactive-icon.png" alt="inactive icon">Inactive</button></li>
                                            <li><button type="button" class="status__expire"><img src="{{asset('')}}/front/images/expire-icon.png" alt="expire icon">Expire</button></li>
                                        </ul>
                                    </div>
                                    <div class="status__renew">
                                        <button type="button"><img src="{{asset('')}}/front/images/renew-icon.png" alt="expire icon">Renew Now</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="age" placeholder="Age">
                                    </div>
                                </div>
                                <div class="row">


                                    <div class="form-group col-sm-12">
                                        <input type="text" class="form-control" id="bmr" placeholder="BMR">
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="update-area p0 col-sm-12">
                        <div class="upd__box">
                            <div class="hed">
                                <h2>Updates</h2>
                            </div>
                            <div class="upd__table">
                                <table class="text-center">
                                    <thead>
                                        <tr>
                                            <th colspan="3" for="Updates 1">Updates 1</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="upd__photo__side">
                                            <td for="Updates 1">Front Picture</td>
                                            <td>Side Picture</td>
                                            <td>Back Picture</td>
                                        </tr>
                                        <tr class="upd__date">
                                            <td for="Updates 1"><i class="fa fa-calendar"></i>Upload Date</td>
                                            <td><i class="fa fa-calendar"></i>Upload Date</td>
                                            <td><i class="fa fa-calendar"></i>Upload Date</td>
                                        </tr>
                                        <tr class="upd__photos__box">
                                            <td for="Updates 1">
                                                <div class="upd__photos">
                                                    <img src="{{asset('')}}/front/images/register-photo1.png">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="upd__photos">
                                                    <img src="{{asset('')}}/front/images/register-photo3.png">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="upd__photos">
                                                    <img src="{{asset('')}}/front/images/register-photo2.png">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="upd__cmt__hdr">
                                            <td colspan="2" class="text-left" for="Updates 1"><i class="fa fa-comments-o"></i>Ambassador Reviews</td>
                                            <td><i class="fa fa-calendar"></i>Date of Update</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="upd__review__box clrlist">
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="{{asset('')}}/front/images/amb-review-img.png" class="media-object">
                                        </div>
                                        <div class="review__online"><img src="{{asset('')}}/front/images/online-icon.png"></div>
                                        <div class="media-body media-body-icon">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <ul>
                                                <li class="pul-lft"><i class="fa fa-clock-o"></i>9:40 pm</li>
                                                <li class="pul-rgt"><i class="fa fa-check-circle-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="upd__table">
                                <table class="text-center">
                                    <thead>
                                        <tr>
                                            <th colspan="3" for="Updates 1">Updates 2</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="upd__photo__side">
                                            <td for="Updates 1">Front Picture</td>
                                            <td>Side Picture</td>
                                            <td>Back Picture</td>
                                        </tr>
                                        <tr class="upd__date">
                                            <td for="Updates 1"><i class="fa fa-calendar"></i>Upload Date</td>
                                            <td><i class="fa fa-calendar"></i>Upload Date</td>
                                            <td><i class="fa fa-calendar"></i>Upload Date</td>
                                        </tr>
                                        <tr class="upd__photos__box">
                                            <td for="Updates 1">
                                                <div class="upd__photos">
                                                    <img src="{{asset('')}}/front/images/register-photo1.png">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="upd__photos">
                                                    <img src="{{asset('')}}/front/images/register-photo3.png">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="upd__photos">
                                                    <img src="{{asset('')}}/front/images/register-photo2.png">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="upd__cmt__hdr">
                                            <td colspan="2" class="text-left" for="Updates 1"><i class="fa fa-comments-o"></i>Ambassador Reviews</td>
                                            <td><i class="fa fa-calendar"></i>Date of Update</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="upd__review__box clrlist">
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="{{asset('')}}/front/images/amb-review-img.png" class="media-object">
                                        </div>
                                        <div class="review__online"><img src="{{asset('')}}/front/images/online-icon.png"></div>
                                        <div class="media-body media-body-icon">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <ul>
                                                <li class="pul-lft"><i class="fa fa-clock-o"></i>9:40 pm</li>
                                                <li class="pul-rgt"><i class="fa fa-check-circle-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-body media-body-review">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                            <ul>
                                                <li class="pul-lft"><i class="fa fa-clock-o"></i>9:40 pm</li>
                                                <li class="pul-rgt"><i class="fa fa-check-circle-o"></i></li>
                                            </ul>
                                        </div>
                                        <div class="media-right">
                                            <img src="{{asset('')}}/front/images/amb-review-img-2.png" class="media-object">
                                        </div>
                                        <div class="review__online review__online-two"><img src="{{asset('')}}/front/images/online-icon.png"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</section>
@endsection
