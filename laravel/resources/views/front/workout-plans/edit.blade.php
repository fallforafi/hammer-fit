@extends('customer')

@section('content')
<?php
$required = 'required';
?>
<section class="dashboard-area">
    <div class="container">


        <div class="dash__lft col-sm-3">
            <div class="dash__tabs">
                @include('front/common/left')
            </div>
        </div>
        <div class="dash__rgt col-sm-9">
            <div class="tab-content">
                <div id="trainingDetails">
                    <div class="workout-main col-sm-12">
                        <div class="workout-inr">
                            <div class="flash-message">
                                @if (count($errors->register) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->register->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if (Session::has('success'))
                                <div class="alert alert-success">
                                    <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
                                </div>
                                @endif
                            </div>
                            <form method="post" action="<?php echo url('workout-plans/update'); ?>" enctype="multipart/form-data">
                                <div class="profile__desc">
                                    <h3>Title:</h3>
                                    <input type="text" name="title" class="form-control" placeholder="Title *" required="required" value="{{ $workoutPlans->title}}">


                                </div>
                                <div class="profile__desc">
                                    <h3>Description:</h3>
                                    <textarea name="description" class="form-control" placeholder="Description *" required="required" maxlength="200">{{ $workoutPlans->description}}</textarea>


                                </div>

                                <div class="profile_desc">


                                    <div class="fit__sub__reset clrlis profile__desc"> 
                                        <h3>File:</h3>
                                        <input type="text" name="file" value="{{$workoutPlans->file }}" class="form-control" readonly="">
                                        <button type="button" class="fit__submit btn-block" id="upfile2" name="file" style="cursor: pointer;">Update File</button>
                                        <input type="file" id="file2" name="file1" style="display: none">
                                        <label>Pdf Only</label>

                                    </div>  
                                </div>

                                <div class="col-sm-12 fit__sub__reset clrlist">
                                    <input type="hidden" name="id" value="{{ $workoutPlans->id }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="fit__submit">Update</button>
                                    <a href="{{ url('workout-plans') }}" class="btn btn-flat fit__reset">Back</a>
                                </div>
                                <br>

                            </form>
                        </div>

                    </div>
                </div></div></div>
    </div>
</section>
<script type="text/javascript">
    $("#upfile2").click(function () {
        $("#file2").trigger('click');
    });
    jQuery('#file2').bind("change", function () {

        var imgVal = $('#file2').val();
        if (imgVal != '') {
            jQuery("#upfile2").text("File Added");
        } else {
            alert("Please add photo");
        }
        return false;
    });
</script>
@endsection


